﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static InstantPayServiceLib.InstantPay_Model;

namespace InstantPayServiceLib
{
    public static class InstantPay_DataBase
    {
        #region [Connection Strings]
        public static SqlConnection MyAmdDBConnection = new SqlConnection(InstantPayConfig.MyAmdDBConnectionString);

        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }

        public static void MyAmdOpenConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Closed)
            {
                MyAmdDBConnection.Open();
            }
        }
        public static void MyAmdCloseConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Open)
            {
                MyAmdDBConnection.Close();
            }
        }
        #endregion

        #region [Log Section]
        public static bool Information(string postUrl, string requestRemark, string requestJson, string responseJson, string actionType, string agentId, string trackid)
        {
            try
            {
                string query = "insert into T_InstantPayResponseLog (PostUrl,RequestRemark,RequestJson,ResponseJson,ActionType,AgentId,TrackId)"
                    + "values ('" + postUrl + "','" + requestRemark + "','" + requestJson + "','" + responseJson + "','" + actionType + "','" + agentId + "','" + trackid + "')";

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool Error(string requestRemark, string postUrl, string requestJson, string responseRemark, string errorRemark, string actionType, string agentId, string trackid)
        {
            try
            {
                string query = "insert into T_InstantPayErrorLog (RequestRemark,PostUrl,RequestJson,ResponseRemark,ErrorRemark,ActionType,AgentId,TrackId)"
                    + "values ('" + requestRemark + "','" + postUrl + "','" + requestJson + "','" + responseRemark + "','" + errorRemark + "','" + actionType + "','" + agentId + "','" + trackid + "')";

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        #endregion

        #region [Remitter Section]
        public static DataTable GetRemitterDetailByUserId(string agentId = null, string remtmobile = null)
        {
            string query = string.Empty;
            if (!string.IsNullOrEmpty(agentId) && string.IsNullOrEmpty(remtmobile))
            {
                query = "select * from T_InstantPayRemitterDetail where AgentId='" + agentId + "'";
            }
            else if (string.IsNullOrEmpty(agentId) && !string.IsNullOrEmpty(remtmobile))
            {
                query = "select * from T_InstantPayRemitterDetail where RegMobile='" + remtmobile + "'";
            }
            else if (!string.IsNullOrEmpty(agentId) && !string.IsNullOrEmpty(remtmobile))
            {
                query = "select * from T_InstantPayRemitterDetail where AgentId='" + agentId + "' and RegMobile='" + remtmobile + "'";
            }

            return GetRecordFromTable(query);
        }
        public static DataTable GetReitterDetails(string remitterId, string mobile, string agentid)
        {
            //string query = "select * from T_InstantPayRemitterDetail where id='" + remitterId + "' and mobile='" + mobile + "' and AgentId='" + agentid + "'";
            string query = "select top 1 * from T_InstantPayRemitterDetail where RegMobile='" + mobile + "' and AgentId='" + agentid + "' and is_verified=1 order by regid desc";
            return GetRecordFromTable(query);
        }

        //public static DataTable GetReitterResponseDetails(string remitterId)
        //{
        //    string query = "select * from T_InstantPayRemitterRegResponse where RemitterId='" + remitterId + "'";

        //    return GetRecordFromTable(query);
        //}

        //public static bool CheckRemitterExist(string remitterId)
        //{
        //    try
        //    {
        //        string query = "select rreg.RemitterId as reg1, rrespo.RemitterId as reg2 from T_InstantPayRemitterReg rreg inner join T_InstantPayRemitterRegResponse rrespo on rreg.RemitterId=rrespo.RemitterId where rreg.RemitterId='" + remitterId + "'";

        //        DataTable dtRemitterDel = GetRecordFromTable(query);
        //        if (dtRemitterDel != null && dtRemitterDel.Rows.Count > 0)
        //        {
        //            return true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //    }

        //    return false;
        //}

        public static int InsertRemitterGetId(string mobile, string firstName, string lastName, string pinCode, string localadd, string agentid, string remtid = null, string status = null, string kycstatus = null, string verifystatus = null)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_InstantPay_InsertRemitterReg", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Mobile", mobile);
                sqlCommand.Parameters.AddWithValue("@FirstName", firstName);
                sqlCommand.Parameters.AddWithValue("@LastName", lastName);
                sqlCommand.Parameters.AddWithValue("@PinCode", pinCode);
                sqlCommand.Parameters.AddWithValue("@AgentId", agentid);
                sqlCommand.Parameters.AddWithValue("@LocalAddress", localadd);
                sqlCommand.Parameters.AddWithValue("@RemitterId", !string.IsNullOrEmpty(remtid) ? remtid : "");
                sqlCommand.Parameters.AddWithValue("@Status", !string.IsNullOrEmpty(status) ? status : "");
                sqlCommand.Parameters.AddWithValue("@IsKYCStatus", !string.IsNullOrEmpty(kycstatus) ? kycstatus : "");
                sqlCommand.Parameters.AddWithValue("@VerifiedStatus", !string.IsNullOrEmpty(verifystatus) ? verifystatus : "");

                sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;

                MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                string id = sqlCommand.Parameters["@Id"].Value.ToString();
                MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return Convert.ToInt32(id);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return 0;
        }

        public static bool UpdateRemitterRegistration(int remtID, string remitterId, string verifiedStatus, string agentId)
        {
            try
            {
                string query = "update T_InstantPayRemitterReg set RemitterId='" + remitterId + "', VerifiedStatus='" + verifiedStatus + "', UpdatedDate=getdate() where RegId=" + remtID;

                if (InsertUpdateDataBase(query))
                {
                    string query2 = "insert into T_InstantPayRemitterRegResponse (RemitterId,AgentUserId) values ('" + remitterId + "','" + agentId + "')";

                    return InsertUpdateDataBase(query2);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool UpdateRemitterRegistrationValidate(string remitterId, string mobile, string verifiedStatus, string agentId)
        {
            try
            {
                string query = "update T_InstantPayRemitterDetail set is_verified='" + verifiedStatus + "', UpdatedDate=getdate() where id='" + remitterId + "' and RegMobile='" + mobile + "' and AgentId='" + agentId + "'";

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool UpdateRemitterRegResponse(RemitterRegResponse respo)
        {
            try
            {
                string query = "update T_InstantPayRemitterRegResponse set "
                    + "Address='" + respo.Address + "',City='" + respo.City + "',IsVerified='" + respo.IsVerified + "',KYCDoc='" + respo.KYCDoc + "',KYCStatus='" + respo.KYCStatus + "'"
                    + ",Mobile='" + respo.Mobile + "',Name='" + respo.Name + "',PernTxnLimit='" + respo.PernTxnLimit + "',PinCode='" + respo.PinCode + "',State='" + respo.State + "'"
                    + ",CreditLimit='" + respo.CreditLimit + "',ConsumedAmount='" + respo.ConsumedAmount + "',RemainingLimit='" + respo.RemainingLimit + "',IMPSMode='" + respo.IMPSMode + "',NEFTMode='" + respo.NEFTMode + "',UpdatedDate=getdate()";

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool DeleteRemitter(int remtID)
        {
            try
            {
                string query = "delete from T_InstantPayRemitterReg where RegId=" + remtID;

                if (InsertUpdateDataBase(query))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool Delete_T_InstantPayBeneficary(int benId)
        {
            try
            {
                string query = "delete from T_InstantPayBeneficary where ID=" + benId;

                if (InsertUpdateDataBase(query))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool Insert_T_InstantPayRemitterReg(RemitterRegRequest remttRequest)
        {
            try
            {
                string query = "insert into T_InstantPayRemitterReg (Mobile,FirstName,LastName,PinCode,AgentUserId,RemitterId,Status,IsKYCStatus,VerifiedStatus)"
                    + " values('" + remttRequest.Mobile + "','" + remttRequest.FirstName + "','" + remttRequest.LastName + "','" + remttRequest.PinCode + "','" + remttRequest.AgentUserId + "'"
                    + ",'" + remttRequest.RemitterId + "','" + remttRequest.Status + "','" + remttRequest.IsKYCStatus + "','" + remttRequest.VerifiedStatus + "')";

                if (InsertUpdateDataBase(query))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool Insert_T_InstantPayRemitterRegResponse(RemitterRegResponse remtRes)
        {
            try
            {
                string query = "insert into T_InstantPayRemitterRegResponse (RemitterId,AgentUserId,Address,City,IsVerified,KYCDoc,KYCStatus,Mobile,Name,PernTxnLimit,PinCode,State,CreditLimit,ConsumedAmount,RemainingLimit,IMPSMode,NEFTMode)"
                    + " values('" + remtRes.RemitterId + "','" + remtRes.AgentUserId + "','" + remtRes.Address + "','" + remtRes.City + "','" + remtRes.IsVerified + "'"
                    + ",'" + remtRes.KYCDoc + "','" + remtRes.KYCStatus + "','" + remtRes.Mobile + "','" + remtRes.Name + "','" + remtRes.PernTxnLimit + "'"
                    + ",'" + remtRes.PinCode + "','" + remtRes.State + "','" + remtRes.CreditLimit + "','" + remtRes.ConsumedAmount + "','" + remtRes.RemainingLimit + "','" + remtRes.IMPSMode + "','" + remtRes.NEFTMode + "')";

                if (InsertUpdateDataBase(query))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static int InsertT_InstantPayBeneficary(RemitterBenDetail ben)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_InstantPay_Beneficary", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@Name", ben.Name);
                sqlCommand.Parameters.AddWithValue("@Mobile", ben.Mobile);
                sqlCommand.Parameters.AddWithValue("@Account", ben.Account);
                sqlCommand.Parameters.AddWithValue("@IfscCode", ben.IfscCode);
                sqlCommand.Parameters.AddWithValue("@RemitterId", ben.RemitterId);
                sqlCommand.Parameters.AddWithValue("@AgentId", ben.AgentId);
                sqlCommand.Parameters.AddWithValue("@ReqBank", ben.ReqBank);
                sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;

                MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                string id = sqlCommand.Parameters["@Id"].Value.ToString();
                MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return Convert.ToInt32(id);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return 0;
        }

        public static bool InsertT_InstantPayBeneficaryDetails(RemitterBenDetail ben)
        {
            try
            {
                string updateQuery = "update T_InstantPayBeneficary set Status=0 where RemitterId='" + ben.RemitterId + "' and AgentId='" + ben.AgentId + "' and Mobile='" + ben.Mobile + "' and Account='" + ben.Account + "'";
                bool isUpdate= InsertUpdateDataBase(updateQuery);

                //string query = "insert into T_InstantPayBeneficary "
                //    + "(Name,Mobile,Account,IfscCode,RemitterId,AgentId,BeneficiaryId,Status,Bank,imps,lastSuccessDate,lastSuccessImps,lastSuccessName) values "
                //    + "('" + ben.Name + "','" + ben.Mobile + "','" + ben.Account + "','" + ben.IfscCode + "','" + ben.RemitterId + "','" + ben.AgentId + "','" + ben.BeneficiaryId + "',"
                //    + "'" + ben.Status + "','" + ben.Bank + "','" + ben.imps + "','" + ben.lastSuccessDate + "','" + ben.lastSuccessImps + "','" + ben.lastSuccessName + "')";
                string query = "insert into T_InstantPayBeneficary "
                    + "(Name,Mobile,Account,IfscCode,RemitterId,AgentId,BeneficiaryId,Status,Bank,imps,lastSuccessDate,lastSuccessImps,lastSuccessName,verificationDt,ReqBank,TimeStamp,Ipay_uuid,OrderId,Environment) values "
                    + "('" + ben.Name + "','" + ben.Mobile + "','" + ben.Account + "','" + ben.IfscCode + "','" + ben.RemitterId + "','" + ben.AgentId + "','" + ben.BeneficiaryId + "',"
                    + "'" + ben.Status + "','" + ben.Bank + "','','','0','" + ben.Name + "','" + ben.verificationDt + "','" + ben.Bank + "','" + ben.TimeStamp + "','" + ben.Ipay_uuid + "','" + ben.OrderId + "','" + ben.Environment + "')";
                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool Update_T_InstantPayBeneficary(RemitterBenDetail ben,string verificationDt)
        {
            try
            {
                string query = "update T_InstantPayBeneficary set "
                    + "BeneficiaryId='" + ben.BeneficiaryId + "',Status='" + ben.Status + "',TimeStamp='" + ben.TimeStamp + "',Ipay_uuid='" + ben.Ipay_uuid + "',OrderId='" + ben.OrderId + "',Environment='" + ben.Environment + "',verificationDt='"+ verificationDt + "',lastSuccessName='"+ben.lastSuccessName + "',Bank='"+ben.Bank+"',UpdatedDate=getdate()"
                    + " where RemitterId='"+ ben.RemitterId + "' and ID=" + ben.BenID;

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool Update_T_InstantPayBeneficary(string remitterId, string bank, string imps, string lastSuccessDate, string lastSuccessImps, string lastSuccessName)
        {
            try
            {
                string query = "update T_InstantPayBeneficary set "
                    + "Bank='" + bank + "',imps='" + imps + "',lastSuccessDate='" + lastSuccessDate + "',lastSuccessImps='" + lastSuccessImps + "',lastSuccessName='" + lastSuccessName + "',UpdatedDate=getdate()"
                    + "where RemitterId=" + remitterId;

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static int InsertT_InstantPayFundTransfer(string remittermobile, string beneficiaryid, string amount, string transferMode, string agentId, string trackid, string RemitterId, string ledgerAmount, string transTrackId)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_InstantPay_InsertFundTransfer", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@AgentId", agentId);
                sqlCommand.Parameters.AddWithValue("@RemitterMobile", remittermobile);
                sqlCommand.Parameters.AddWithValue("@BenificieryId", beneficiaryid);
                sqlCommand.Parameters.AddWithValue("@Amount", amount);
                sqlCommand.Parameters.AddWithValue("@TxnMode", transferMode);
                sqlCommand.Parameters.AddWithValue("@TrackId", trackid);
                sqlCommand.Parameters.AddWithValue("@RemitterId", RemitterId);
                sqlCommand.Parameters.AddWithValue("@LedgerAmount", ledgerAmount);
                sqlCommand.Parameters.AddWithValue("@TransTrackId", transTrackId);
                sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;

                MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                string id = sqlCommand.Parameters["@Id"].Value.ToString();
                MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return Convert.ToInt32(id);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return 0;
        }

        public static bool UpdateFundTranferDetail(FundTransfer fund,string remitterId)
        {
            try
            {
                string query = "update T_InstantPayFundTransfer set "
                    + "ipay_id='" + fund.ipay_id + "',ref_no='" + fund.ref_no + "',opr_id='" + fund.opr_id + "',name='" + fund.name + "',opening_bal='" + fund.opening_bal + "'"
                    + ",charged_amt='" + fund.charged_amt + "',locked_amt='" + fund.locked_amt + "',ccf_bank='" + fund.ccf_bank + "',bank_alias='" + fund.bank_alias + "'"
                    + ",timestamp='" + fund.timestamp + "',ipay_uuid='" + fund.ipay_uuid + "',orderid='" + fund.orderid + "',environment='" + fund.environment + "',Status='" + fund.Status + "',RefundId='" + fund.RefundId + "',UpdatedDate=getdate() where RemitterId='" + remitterId + "' and FundTransId=" + fund.FundTransId;

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool UpdateRemainingRemitterLimit(string remitterId, string remittermobile, string agentId, string consumedlimit, string remaininglimit)
        {
            try
            {
                string query = "update T_InstantPayRemitterDetail set consumedlimit='" + consumedlimit + "', remaininglimit='" + remaininglimit + "' where AgentId='" + agentId + "' and id='" + remitterId + "' and mobile='" + remittermobile + "'";

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static DataTable GetTransactionHistory(string mobile, string remitterId)
        {
            string query = "SELECT t.*, b.Name as BenName FROM T_InstantPayFundTransfer t inner join T_InstantPayBeneficary b on t.BenificieryId =b.BeneficiaryId where t.RemitterMobile='" + mobile + "' and t.RemitterId='" + remitterId + "' order by t.CreatedDate desc";

            return GetRecordFromTable(query);
        }

        public static DataTable GetFilterTransactionHistory(string remitterId, string fromDate, string toDate, string trackId, string filstatus)
        {
            DataTable mytable = new DataTable();

            try
            {
                string query = "SELECT t.*, b.Name as BenName FROM T_InstantPayFundTransfer t inner join T_InstantPayBeneficary b on t.BenificieryId =b.BeneficiaryId and t.RemitterMobile=b.Mobile";
                string whrcondition = " where t.RemitterId='" + remitterId + "'";


                if (!string.IsNullOrEmpty(fromDate))
                {
                    whrcondition = whrcondition + " and t.CreatedDate>=CONVERT(datetime,'" + GetDateFormate(fromDate) + "')";
                }

                if (!string.IsNullOrEmpty(toDate))
                {
                    whrcondition = whrcondition + " and t.CreatedDate<=CONVERT(datetime,'" + GetDateFormate(toDate) + " 23:59:59')";
                }

                if (!string.IsNullOrEmpty(trackId))
                {
                    whrcondition = whrcondition + " and t.TrackId='" + trackId + "'";
                }

                if (!string.IsNullOrEmpty(filstatus))
                {
                    whrcondition = whrcondition + " and t.Status like '%" + filstatus + "%'";
                }

                query = query + whrcondition + " order by t.CreatedDate desc";

                DataTable dtTrans = GetRecordFromTable(query);


                mytable.Columns.Add("UpdatedDate", typeof(string));
                mytable.Columns.Add("ipay_id", typeof(string));
                mytable.Columns.Add("ref_no", typeof(string));
                mytable.Columns.Add("TrackId", typeof(string));
                mytable.Columns.Add("TxnMode", typeof(string));
                mytable.Columns.Add("Amount", typeof(string));
                mytable.Columns.Add("charged_amt", typeof(string));
                mytable.Columns.Add("BenificieryId", typeof(string));
                mytable.Columns.Add("BenName", typeof(string));
                mytable.Columns.Add("Refund", typeof(string));
                mytable.Columns.Add("RefundId", typeof(string));
                mytable.Columns.Add("FundTransId", typeof(string));
                mytable.Columns.Add("Status", typeof(string));
                mytable.Columns.Add("Type", typeof(string));

                if (dtTrans != null && dtTrans.Rows.Count > 0)
                {
                    for (int i = 0; i < dtTrans.Rows.Count; i++)
                    {
                        DataRow dr1 = mytable.NewRow();
                        dr1 = mytable.NewRow();
                        dr1["UpdatedDate"] = dtTrans.Rows[i]["UpdatedDate"].ToString();
                        dr1["ipay_id"] = dtTrans.Rows[i]["ipay_id"].ToString();
                        dr1["ref_no"] = dtTrans.Rows[i]["ref_no"].ToString();
                        dr1["TrackId"] = dtTrans.Rows[i]["TrackId"].ToString();
                        dr1["TxnMode"] = dtTrans.Rows[i]["TxnMode"].ToString();
                        dr1["Amount"] = dtTrans.Rows[i]["Amount"].ToString();
                        dr1["charged_amt"] = dtTrans.Rows[i]["charged_amt"].ToString();
                        dr1["BenificieryId"] = dtTrans.Rows[i]["BenificieryId"].ToString();
                        dr1["BenName"] = dtTrans.Rows[i]["BenName"].ToString();
                        dr1["Refund"] = dtTrans.Rows[i]["Refund"].ToString();
                        dr1["RefundId"] = dtTrans.Rows[i]["RefundId"].ToString();
                        dr1["FundTransId"] = dtTrans.Rows[i]["FundTransId"].ToString();
                        dr1["Status"] = dtTrans.Rows[i]["Status"].ToString();
                        dr1["Type"] = "fund";
                        mytable.Rows.Add(dr1);
                    }
                }

                string query2 = "select * from T_InstantPayInitiatePayout";
                string whrcondition2 = " where RemitterId='" + remitterId + "' and PaymentType='getname'";
                if (!string.IsNullOrEmpty(fromDate))
                {
                    whrcondition2 = whrcondition2 + " and CreatedDate>=CONVERT(datetime,'" + GetDateFormate(fromDate) + "')";
                }

                if (!string.IsNullOrEmpty(toDate))
                {
                    whrcondition2 = whrcondition2 + " and CreatedDate<=CONVERT(datetime,'" + GetDateFormate(toDate) + " 23:59:59')";
                }

                if (!string.IsNullOrEmpty(trackId))
                {
                    whrcondition2 = whrcondition2 + " and TrackId='" + trackId + "'";
                }

                if (!string.IsNullOrEmpty(filstatus))
                {
                    whrcondition2 = whrcondition2 + " and Status like '%" + filstatus + "%'";
                }

                query2 = query2 + whrcondition2 + " order by CreatedDate desc";

                DataTable dtPayTrans = GetRecordFromTable(query2);
                if (dtPayTrans != null && dtPayTrans.Rows.Count > 0)
                {
                    for (int i = 0; i < dtPayTrans.Rows.Count; i++)
                    {
                        string mode = string.Empty;
                        if (dtPayTrans.Rows[i]["sp_key"].ToString() == "DPN")
                        {
                            mode = "IMPS";
                        }
                        else if (dtPayTrans.Rows[i]["sp_key"].ToString() == "BPN")
                        {
                            mode = "NEFT";
                        }
                        else if (dtPayTrans.Rows[i]["sp_key"].ToString() == "CPN")
                        {
                            mode = "RTGS";
                        }

                        DataRow dr2 = mytable.NewRow();
                        dr2 = mytable.NewRow();
                        dr2["UpdatedDate"] = dtPayTrans.Rows[i]["CreatedDate"].ToString();
                        dr2["ipay_id"] = dtPayTrans.Rows[i]["ipay_id"].ToString();
                        dr2["ref_no"] = dtPayTrans.Rows[i]["external_ref"].ToString();
                        dr2["TrackId"] = dtPayTrans.Rows[i]["TrackId"].ToString();
                        dr2["TxnMode"] = mode;
                        dr2["Amount"] = dtPayTrans.Rows[i]["LedgerAmount"].ToString();
                        dr2["charged_amt"] = "0";
                        dr2["BenificieryId"] = dtPayTrans.Rows[i]["BenificieryId"].ToString();
                        dr2["BenName"] = dtPayTrans.Rows[i]["payout_name"].ToString();
                        dr2["Refund"] = dtPayTrans.Rows[i]["Refund"].ToString();
                        dr2["RefundId"] = dtPayTrans.Rows[i]["RefundId"].ToString();
                        dr2["FundTransId"] = dtPayTrans.Rows[i]["InitiatePayoutId"].ToString();
                        dr2["Status"] = dtPayTrans.Rows[i]["Status"].ToString();
                        dr2["Type"] = dtPayTrans.Rows[i]["PaymentType"].ToString();
                        mytable.Rows.Add(dr2);
                    }
                }

                //if (mytable != null && mytable.Rows.Count > 0)
                //{
                //    DataView view = mytable.DefaultView;
                //    view.Sort = "UpdatedDate DESC";
                //    mytable = view.ToTable();
                //}
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return mytable;
        }

        public static string GetRemitterRemingLimit(string remitterId, string agentId, string mobile)
        {
            string result = string.Empty;

            try
            {
                string query = "select * from T_InstantPayRemitterDetail where id='" + remitterId + "' and AgentId='" + agentId + "' and mobile='" + mobile + "' and is_verified='1'";
                DataTable dtLimit = GetRecordFromTable(query);
                if (dtLimit != null && dtLimit.Rows.Count > 0)
                {
                    result = dtLimit.Rows[0]["remaininglimit"].ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public static bool ProcessToReFund(string transid, string reportid)
        {
            try
            {
                string query = "update T_InstantPayFundTransfer set Refund=1, RefundId='' where RefundId='" + reportid + "' and FundTransId=" + transid;

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool DeleteBeneficiaryDetail(string benid,string mobile, string remitterId, string agentId)
        {
            try
            {
                if (!string.IsNullOrEmpty(benid))
                {
                    string query = "delete from T_InstantPayBeneficary where BeneficiaryId='" + benid + "' and RemitterId='" + remitterId + "' and Mobile='"+ mobile + "' and AgentId='" + agentId + "'";

                    return InsertUpdateDataBase(query);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool UpdateOtpReferenceForBenDelete(string beneficiaryid, string remitterid,string remitterMobile, string agentId, string otpReference)
        {
            try
            {
                string query = "update T_InstantPayBeneficary set otpReference='" + otpReference + "' where Mobile='" + remitterMobile + "' and RemitterId='" + remitterid + "' and AgentId='" + agentId + "' and BeneficiaryId='" + beneficiaryid + "'";

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool DeleteBeneficiaryByMobile(string mobile, string agentId, string remitterId = null)
        {
            try
            {
                string query = "delete from T_InstantPayBeneficary where Mobile='" + mobile + "' and AgentId='" + agentId + "' and RemitterId='" + remitterId + "'";

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        #endregion

        #region [Get Section]
        public static DataTable GetT_InstantPayRemitterRegResponse(string agentid, string remitterid, string mobile)
        {
            //string query = "select * from T_InstantPayRemitterRegResponse where RemitterId='" + remitterid + "' and AgentUserId='" + agentid + "' and Mobile='" + mobile + "'";
            string query = "SELECT rr.*, r.LocalAddress FROM T_InstantPayRemitterRegResponse rr inner JOIN T_InstantPayRemitterReg r on rr.RemitterId=r.RemitterId where rr.RemitterId='" + remitterid + "' and rr.AgentUserId='" + agentid + "' and rr.Mobile='" + mobile + "'";

            return GetRecordFromTable(query);
        }

        public static DataTable GetBenDetailsByRemitterId(string remitterId, string mobile, string agentId, string beneficaryId = null)
        {
            string query = "select * from T_InstantPayBeneficary where BeneficiaryId is not null and RemitterId='" + remitterId + "' and AgentId='" + agentId + "' and Status=1";
            //string query = "select * from T_InstantPayBeneficary where BeneficiaryId is not null and Mobile='" + mobile + "' and AgentId='" + agentId + "' and Status=1";
            if (!string.IsNullOrEmpty(beneficaryId))
            {
                query = query + " and BeneficiaryId='" + beneficaryId + "'";
            }

            return GetRecordFromTable(query);
        }

        public static DataTable GetBenificiaryByRemitterId(string remitterId, string mobile, string agentId, string beneficaryId )
        {
            string query = "select top 1 * from T_InstantPayBeneficary where BeneficiaryId is not null and RemitterId='" + remitterId + "' and AgentId='" + agentId + "' and Mobile='" + mobile + "'";           
            if (!string.IsNullOrEmpty(beneficaryId))
            {
                query = query + " and BeneficiaryId='" + beneficaryId + "'";
            }
            query = query + " order by ID desc";
            return GetRecordFromTable(query);
        }

        public static DataTable GetFundTransferVeryficationDetail(string remitterId, string beneficaryId, ref DataTable bankdetail)
        {
            string query = "select b.Name,b.Account,b.IfscCode,b.RemitterId,b.AgentId,b.BeneficiaryId,b.Bank,r.FirstName,r.LastName,r.Mobile,r.Address,r.PinCode "
            + " from T_InstantPayBeneficary b inner join T_InstantPayRemitterDetail r on r.id=b.RemitterId "
            + " where b.BeneficiaryId='" + beneficaryId + "' and r.is_verified='1' and r.id='" + remitterId + "'";

            bankdetail = GetRecordFromTable("select * from T_InstantPayBankDetail where bank_name in (select Bank from T_InstantPayBeneficary where BeneficiaryId='" + beneficaryId + "' and RemitterId='" + remitterId + "')");

            if (bankdetail.Rows.Count == 0)
            {
                bankdetail = GetRecordFromTable("select * from T_InstantPayBankDetail where bank_name in (select ReqBank from T_InstantPayBeneficary where BeneficiaryId='" + beneficaryId + "' and RemitterId='" + remitterId + "')");
            }

            return GetRecordFromTable(query);
        }
        #endregion

        #region [Common Section]       
        public static DataTable GetRecordFromTable(string query)
        {
            try
            {
                if (!string.IsNullOrEmpty(query))
                {
                    sqlCommand = new SqlCommand(query, MyAmdDBConnection);

                    sqlDataAdapter = new SqlDataAdapter();
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    dataSet = new DataSet();
                    sqlDataAdapter.Fill(dataSet, "T_Table");
                    dataTable = dataSet.Tables["T_Table"];
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        private static bool InsertUpdateDataBase(string query)
        {
            try
            {
                if (!string.IsNullOrEmpty(query))
                {
                    sqlCommand = new SqlCommand(query, MyAmdDBConnection);

                    MyAmdOpenConnection();
                    int isSuccess = sqlCommand.ExecuteNonQuery();
                    MyAmdCloseConnection();

                    if (isSuccess > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        #endregion

        public static DataTable GetCombieRemitterDetail(string userId, string remitterId, string mobile)
        {
            string query = "select * from T_InstantPayRemitterDetail where id='" + remitterId + "' and RegMobile='" + mobile + "' and mobile='" + mobile + "'";

            return GetRecordFromTable(query);
        }
        public static DataTable GetRemitterDetailDuringRegistrationWithOtp(string userId, string remitterId, string mobile)
        {
            string query = "select * from T_InstantPayRemitterDetail where id='" + remitterId + "' and RegMobile='" + mobile + "' and AgentId='" + userId + "'";

            return GetRecordFromTable(query);
        }
        public static DataTable GetLedgerDetail(string userId, string trackid)
        {
            string query = "select * from ledgerdetails where AgentID='" + userId + "' and InvoiceNo='" + trackid + "'";

            return GetRecordFromTable(query);
        }

        public static DataTable GetTransactionHistoryByTrackId(string trackid)
        {
            string query = "select * from T_InstantPayFundTransfer where TrackId='" + trackid + "'";

            return GetRecordFromTable(query);
        }

        public static DataTable GetTransactionHistoryByTransId(string transid, string orderid = null)
        {
            string query = "select * from T_InstantPayFundTransfer where FundTransId='" + transid + "' and Refund=0";
            if (!string.IsNullOrEmpty(orderid))
            {
                query = query + " and  ipay_id='" + orderid + "'";
            }

            return GetRecordFromTable(query);
        }

        public static DataTable GetPayoutTransactionHistoryByTrackId(string trackid)
        {
            string query = "select * from T_InstantPayInitiatePayout where TrackId='" + trackid + "'";

            return GetRecordFromTable(query);
        }

        private static string GetDateFormate(string date)
        {
            string result = string.Empty;

            if (!string.IsNullOrEmpty(date))
            {
                string[] strDate = date.Split('/');

                result = strDate[2] + "-" + strDate[1] + "-" + strDate[0];
            }

            return result;
        }

        public static DataTable GetAgencyDetailById(string agencyId)
        {
            try
            {
                sqlCommand = new SqlCommand("AgencyDetails", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@UserId", agencyId));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "agent_register");
                dataTable = dataSet.Tables["agent_register"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static DataTable GetRemBenDetailByAgencyId(string agencyId, string mobile, string remitterid, string benid)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_GetPayoutRemBenDetailByAgencyId", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@AgencyId", agencyId));
                sqlCommand.Parameters.Add(new SqlParameter("@Mobile", mobile));
                sqlCommand.Parameters.Add(new SqlParameter("@RemitterId", remitterid));
                sqlCommand.Parameters.Add(new SqlParameter("@BeneficiaryId", benid));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "table");
                dataTable = dataSet.Tables["table"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static DataTable GetDMTRemBenDetailByAgencyId(string agencyId, string mobile, string remitterid, string benid)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_GetDMTRemBenDetailByAgencyId", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@AgencyId", agencyId));
                sqlCommand.Parameters.Add(new SqlParameter("@Mobile", mobile));
                sqlCommand.Parameters.Add(new SqlParameter("@RemitterId", remitterid));
                sqlCommand.Parameters.Add(new SqlParameter("@BeneficiaryId", benid));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "table");
                dataTable = dataSet.Tables["table"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static DataSet GetSMBPUtilityCommission(string amount, string agentGroupType, string spkey)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_GetSMBP_UtilityCommissionDetails", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Group_Type", agentGroupType));
                sqlCommand.Parameters.Add(new SqlParameter("@SpKey", spkey));
                sqlCommand.Parameters.Add(new SqlParameter("@Amount", amount));

                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataSet;
        }

        #region [Insert Bank Details]
        public static bool BindBankDetails(List<InnerBankDetail> bank)
        {
            bool isSuccess = false;

            try
            {
                if (bank != null && bank.Count > 0)
                {
                    foreach (var item in bank)
                    {
                        string query = "insert into T_InstantPayBankDetail (bank_id,bank_name,imps_enabled,aeps_enabled,bank_sort_name,branch_ifsc,ifsc_alias,bank_iin,is_down)"
                            + "values ('" + item.id + "','" + item.bank_name + "','" + item.imps_enabled + "','" + item.aeps_enabled + "','" + item.bank_sort_name + "','" + item.branch_ifsc + "'"
                            + ",'" + item.ifsc_alias + "','" + item.bank_iin + "','" + item.is_down + "')";

                        if (InsertUpdateDataBase(query))
                        {
                            isSuccess = true;
                        }
                        else
                        {
                            isSuccess = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return isSuccess;
        }

        public static DataTable GetTopBindAllBank()
        {
            string query = "select * from T_InstantPayBankDetail where bank_sort_name in ('SBI','PNB','BOB','BOI','ICI','HDF','AXB','UOB')";

            return GetRecordFromTable(query);
        }

        public static DataTable GetFurtherBindAllBank()
        {
            string query = "select * from T_InstantPayBankDetail where bank_sort_name not in ('SBI','PNB','BOB','BOI','ICI','HDF','AXB','UOB') order by bank_name";

            return GetRecordFromTable(query);
        }
        #endregion

        public static bool IsRemitterAlreadyExist(string remitterid, string mobile, string agentId, ref string RegId)
        {
            string query = "select * from T_InstantPayRemitterDetail where RegMobile='" + mobile + "' and AgentId='" + agentId + "' and id='" + remitterid + "'";

            DataTable dtRemitter = GetRecordFromTable(query);

            if (dtRemitter != null && dtRemitter.Rows.Count > 0)
            {
                RegId = !string.IsNullOrEmpty(dtRemitter.Rows[0]["RegId"].ToString()) ? dtRemitter.Rows[0]["RegId"].ToString() : string.Empty;
                if (!string.IsNullOrEmpty(RegId))
                {
                    return true;
                }
            }

            return false;
        }

        public static bool UpdateLocalAddress(string remitterId, string agentId, string mobile, string address)
        {
            try
            {
                string query = "update T_InstantPayRemitterDetail set CurrentAddress='" + address + "', UpdatedDate=getdate() where id='" + remitterId + "' and mobile='" + mobile + "' and AgentId='" + agentId + "'";

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static int InsertInitiatePayout(InitiatePayout payout)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_InstantPay_InsertInitiatePayout", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@AgentId", payout.AgentId);
                sqlCommand.Parameters.AddWithValue("@RemitterMobile", payout.RemitterMobile);
                sqlCommand.Parameters.AddWithValue("@sp_key", payout.sp_key);
                sqlCommand.Parameters.AddWithValue("@bene_name", payout.bene_name);
                sqlCommand.Parameters.AddWithValue("@credit_amount", payout.credit_amount);
                sqlCommand.Parameters.AddWithValue("@latitude", payout.latitude);
                sqlCommand.Parameters.AddWithValue("@longitude", payout.longitude);
                sqlCommand.Parameters.AddWithValue("@endpoint_ip", payout.endpoint_ip);
                sqlCommand.Parameters.AddWithValue("@alert_mobile", payout.alert_mobile);
                sqlCommand.Parameters.AddWithValue("@alert_email", payout.alert_email);
                sqlCommand.Parameters.AddWithValue("@otp_auth", payout.otp_auth);
                sqlCommand.Parameters.AddWithValue("@otp", payout.otp);
                sqlCommand.Parameters.AddWithValue("@remarks", payout.remarks);
                sqlCommand.Parameters.AddWithValue("@TrackId", payout.TrackId);
                sqlCommand.Parameters.AddWithValue("@RemitterId", payout.RemitterId);
                sqlCommand.Parameters.AddWithValue("@BankName", !string.IsNullOrEmpty(payout.bank_name) ? payout.bank_name : "");
                sqlCommand.Parameters.AddWithValue("@PayType", payout.PayType);
                sqlCommand.Parameters.AddWithValue("@RemtRemark", payout.RemtRemark);
                sqlCommand.Parameters.AddWithValue("@LedgerAmount", payout.LedgerAmount);
                sqlCommand.Parameters.AddWithValue("@BenificieryId", payout.BenificieryId);
                sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;

                MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                string id = sqlCommand.Parameters["@Id"].Value.ToString();
                MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return Convert.ToInt32(id);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return 0;
        }

        public static bool UpdateIPFundTransferDetail(string agentId, string fundTransId, string ipay_id, string status)
        {
            string query = "update T_InstantPayFundTransfer set status='" + status + "',Refund=1 where FundTransId=" + fundTransId + " and ipay_id='" + ipay_id + "' and AgentId='" + agentId + "'";
            return InsertUpdateDataBase(query);
        }
        public static bool UpdateInitiatePayout(InitiatePayout payout, string versionType = "old")
        {
            try
            {
                if (versionType == "new")
                {
                    string query = "update T_InstantPayInitiatePayout set "
                    + "external_ref='" + payout.external_ref + "',ipay_id='" + payout.ipay_id + "',transfer_value='" + payout.transfer_value + "',type_pricing='" + payout.type_pricing + "',"
                    + "commercial_value='" + payout.commercial_value + "',value_tds='" + payout.value_tds + "',ccf='" + payout.ccf + "',vendor_ccf='" + payout.vendor_ccf + "',"
                    + "charged_amt='" + payout.charged_amt + "',payout_credit_refid='" + payout.payout_credit_refid + "',payout_account='" + payout.payout_account + "',"
                    + "payout_ifsc='" + payout.payout_ifsc + "',payout_name='" + payout.payout_name + "',timestamp='" + payout.timestamp + "',ipay_uuid='" + payout.ipay_uuid + "',orderid='" + payout.orderid + "',environment='" + payout.environment + "',Status='" + payout.Status + "',Refund=" + (!string.IsNullOrEmpty(payout.Refund) ? payout.Refund : "0") + ",RefundId='" + payout.RefundId + "',poolReferenceId='" + payout.poolReferenceId + "',txnReferenceId='" + payout.txnReferenceId + "',poolaccount='" + payout.poolaccount + "',poolopeningBal='" + payout.poolopeningBal + "',poolmode='" + payout.poolmode + "',poolclosingBal='" + payout.poolclosingBal + "',poolamount='" + payout.poolamount + "',RefundTrackId='" + payout.external_ref + "'"
                     + " where InitiatePayoutId=" + payout.InitiatePayoutId;

                    return InsertUpdateDataBase(query);
                }
                else
                {
                    string query = "update T_InstantPayInitiatePayout set "
                    + "external_ref='" + payout.external_ref + "',ipay_id='" + payout.ipay_id + "',transfer_value='" + payout.transfer_value + "',type_pricing='" + payout.type_pricing + "',"
                    + "commercial_value='" + payout.commercial_value + "',value_tds='" + payout.value_tds + "',ccf='" + payout.ccf + "',vendor_ccf='" + payout.vendor_ccf + "',"
                    + "charged_amt='" + payout.charged_amt + "',payout_credit_refid='" + payout.payout_credit_refid + "',payout_account='" + payout.payout_account + "',"
                    + "payout_ifsc='" + payout.payout_ifsc + "',payout_name='" + payout.payout_name + "',timestamp='" + payout.timestamp + "',ipay_uuid='" + payout.ipay_uuid + "',orderid='" + payout.orderid + "',environment='" + payout.environment + "',Status='" + payout.Status + "',Refund=" + (!string.IsNullOrEmpty(payout.Refund) ? payout.Refund : "0") + ",RefundId='" + payout.RefundId + "'"
                     + " where InitiatePayoutId=" + payout.InitiatePayoutId;

                    return InsertUpdateDataBase(query);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static DataTable Get_InstantPayDirectPayoutParameters(string type)
        {
            string query = "select * from T_InstantPayDirectPayoutParameters where Status=1 and Type='" + type + "'";

            return GetRecordFromTable(query);
        }

        //==================================New Section=========================================================
        #region [Insert Remitter Details During Registration]
        public static int InsertRemitterFirstDetail(string mobile, string firstName, string lastName, string pinCode, string localadd, string agentid, string remitterId)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_InstantPay_InsertRemitterDetail", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.AddWithValue("@RegMobile", mobile);
                sqlCommand.Parameters.AddWithValue("@FirstName", firstName);
                sqlCommand.Parameters.AddWithValue("@LastName", lastName);
                sqlCommand.Parameters.AddWithValue("@PinCode", pinCode);
                sqlCommand.Parameters.AddWithValue("@AgentId", agentid);
                sqlCommand.Parameters.AddWithValue("@CurrentAddress", localadd);
                sqlCommand.Parameters.AddWithValue("@RemitterId", remitterId);

                sqlCommand.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;

                MyAmdOpenConnection();
                int isSuccess = sqlCommand.ExecuteNonQuery();
                string id = sqlCommand.Parameters["@Id"].Value.ToString();
                MyAmdCloseConnection();

                if (isSuccess > 0)
                {
                    return Convert.ToInt32(id);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }

            return 0;
        }

        public static bool UpdateOtpOfExistingRecord(string mobile, string agentId, string otpReference)
        {
            string query = "select top 1 * from T_InstantPayRemitterDetail where RegMobile='" + mobile + "' and AgentId='" + agentId + "' and is_verified=0 order by RegId desc";
            DataTable dtData = GetRecordFromTable(query);
            if (dtData != null && dtData.Rows.Count > 0)
            {
                string remtID = dtData.Rows[0]["RegId"].ToString();
                string remitterId = dtData.Rows[0]["id"].ToString();
                string updateQuery = "update T_InstantPayRemitterDetail set OtpReference='" + otpReference + "' where RegId=" + remtID + " and RegMobile='" + mobile + "' and AgentId='" + agentId + "' and id='" + remitterId + "'";

                return InsertUpdateDataBase(updateQuery);
            }
            return false;
        }

        public static bool GetRemitterId(string mobile, string agentId)
        {
            string query = "select * from T_InstantPayRemitterDetail where RegMobile='"+ mobile + "' and AgentId='"+ agentId + "' and is_verified=1 order by RegId desc";
            DataTable dtData = GetRecordFromTable(query);
            if (dtData!=null&& dtData.Rows.Count>0)
            {
                return true;
            }
            return false;
        }
        public static bool UpdateRemitterOTPDetail(int remtID, string mobile, string agentId, string remitterid, string otpReference)
        {
            try
            {
                string query = "update T_InstantPayRemitterDetail set id='" + remitterid + "',OtpReference='" + otpReference + "' where RegId=" + remtID + " and RegMobile='" + mobile + "' and AgentId='" + agentId + "'";

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool Update_T_InstantPayRemitterDetailResponse(string remt_id, string name, string respo_mobile, string address, string respo_pincode, string city, string state, string kycstatus, string consumedlimit, string remaininglimit, string kycdocs, string is_verified, string perm_txn_limit, string agentId)
        {
            try
            {
                string query = "update T_InstantPayRemitterDetail set "
                    + "name='" + name + "',mobile='" + respo_mobile + "',address='" + address + "',pincode_res='" + respo_pincode + "',city='" + city + "',state='" + state + "',kycstatus='" + kycstatus + "',consumedlimit='" + consumedlimit + "',"
                    + "remaininglimit='" + remaininglimit + "',kycdocs='" + kycdocs + "',is_verified=" + is_verified + ",perm_txn_limit='" + perm_txn_limit + "',UpdatedDate=getdate()"
                    + " where RegMobile='" + respo_mobile + "' and id='" + remt_id + "' and AgentId='" + agentId + "'";

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool Insert_AlreadyRegisteredRemitterDetails(string query)
        {
            try
            {
                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static DataTable GetT_InstantPayRemitterDetail(string agentid, string remitterid, string mobile)
        {
            //string query = "select * from T_InstantPayRemitterDetail where RegMobile='" + mobile + "' and mobile='" + mobile + "' and AgentId='" + agentid + "' and id='" + remitterid + "'";
            string query = "select * from T_InstantPayRemitterDetail where mobile='" + mobile + "' and AgentId='" + agentid + "' and id='" + remitterid + "'";
            //string query = "select * from T_InstantPayRemitterDetail where mobile='" + mobile + "' and AgentId='" + agentid + "' and is_verified=1";
            return GetRecordFromTable(query);
        }

        public static bool UpdateT_InstantPayBeneficaryDetails(RemitterBenDetail ben)
        {
            try
            {
                //string query = "update T_InstantPayBeneficary set Bank='" + ben.Bank + "',imps='" + ben.imps + "',lastSuccessDate='" + ben.lastSuccessDate + "',lastSuccessImps='" + ben.lastSuccessImps + "',lastSuccessName='" + ben.lastSuccessName + "', UpdatedDate=getdate() where RemitterId='" + ben.RemitterId + "' and BeneficiaryId='" + ben.BeneficiaryId + "' and AgentId='" + ben.AgentId + "' and Mobile='" + ben.Mobile + "'";
                string query = "update T_InstantPayBeneficary set Name='" + ben.Name + "',Account='" + ben.Account + "',IfscCode='" + ben.IfscCode + "',verificationDt='" + ben.verificationDt + "' Status='" + ben.Status + "' Bank='" + ben.Bank + "',imps='',lastSuccessDate='',lastSuccessImps='',lastSuccessName='" + ben.Name + "',TimeStamp='" + ben.TimeStamp + "',Ipay_uuid='" + ben.Ipay_uuid + "',OrderId='" + ben.OrderId + "',Environment='" + ben.Environment + "', UpdatedDate=getdate() where RemitterId='" + ben.RemitterId + "' and BeneficiaryId='" + ben.BeneficiaryId + "' and AgentId='" + ben.AgentId + "' and Mobile='" + ben.Mobile + "'";
                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool UpdateRemainingRemitterLimit_Modified(string agentId, string mobile, string consumedlimit, string remaininglimit)
        {
            string query = "update T_InstantPayRemitterDetail set consumedlimit='" + consumedlimit + "',remaininglimit='" + remaininglimit + "', UpdatedDate=getdate() where AgentId='" + agentId + "' and mobile='" + mobile + "' and RegMobile='" + mobile + "' and is_verified=1";
            return InsertUpdateDataBase(query);
        }
        #endregion

        public static bool UpdateFreshRemitterDetails(string RegId, string id, string agentId, string firstName, string remtmobile, string remtaddress, string pinCode, string city, string state, string kycstatus, string is_verified, string consumedlimit, string remaininglimit, string perm_txn_limit, string totalLimit)
        {
            try
            {
                string query = "update T_InstantPayRemitterDetail set name='" + firstName + "',mobile='" + remtmobile + "',address='" + remtaddress + "',pincode_res='" + pinCode + "',city='" + city + "',state='" + state + "',kycstatus='" + kycstatus + "'"
                    + ",consumedlimit='" + consumedlimit + "',remaininglimit='" + remaininglimit + "',kycdocs='REQUIRED',is_verified='" + is_verified + "',perm_txn_limit='" + perm_txn_limit + "',CreditLimit='" + totalLimit + "'"
                    + " where RegId=" + Convert.ToInt32(RegId) + " and id='" + id + "' and AgentId='" + agentId + "' and RegMobile='" + remtmobile + "'";

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        //===================================Payout Section=============================================
        public static DataTable GetPayoutTransactionHistory(string remitterId, string agentId, string fromdate, string todate, string trackid, string filstatus)
        {
            DataTable mytable = new DataTable();

            try
            {
                string query = "select * from T_InstantPayInitiatePayout";
                string whrcondition = " where AgentId='" + agentId + "' and RemitterId='" + remitterId + "'";


                if (!string.IsNullOrEmpty(fromdate))
                {
                    whrcondition = whrcondition + " and CreatedDate>=CONVERT(datetime,'" + GetDateFormate(fromdate) + "')";
                }

                if (!string.IsNullOrEmpty(todate))
                {
                    whrcondition = whrcondition + " and CreatedDate<=CONVERT(datetime,'" + GetDateFormate(todate) + " 23:59:59')";
                }

                if (!string.IsNullOrEmpty(trackid))
                {
                    whrcondition = whrcondition + " and TrackId='" + trackid + "'";
                }

                if (!string.IsNullOrEmpty(filstatus))
                {
                    whrcondition = whrcondition + " and Status like '%" + filstatus + "%'";
                }

                query = query + whrcondition + " order by CreatedDate desc";

                mytable = GetRecordFromTable(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return mytable;
        }


        //==============================DMT DIRECT============================================================      

        public static bool UpdateRefundStatusInstantPayInitiatePayout(string trackid)
        {
            try
            {
                string query = "update T_InstantPayInitiatePayout set Refund=1,RefundTrackId='" + trackid + "',IsCheckStatus=1,status='Transaction failed' where TrackId='" + trackid + "'";

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool UpdateRefundStatusInstantPayFundTransfer(string trackid, string action)
        {
            try
            {
                string query = string.Empty;
                if (action == "single")
                {
                    query = "update T_InstantPayFundTransfer set Refund=1,RefundId='' where TrackId='" + trackid + "'";
                }
                else
                {
                    query = "update T_InstantPayFundTransfer set Refund=1,RefundId='' where TransTrackId='" + trackid + "'";
                }

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }


        //======================Get Common Trans History By Track id=====================================
        public static DataTable GetCommonFundTransHistory(string trackid, string agentId, ref DataTable RemitterDel, ref DataTable BenDetail)
        {
            DataTable dtCommonFund = new DataTable();

            try
            {
                string fund = "select * from T_InstantPayFundTransfer where TrackId='" + RemoveSpecialCharacters(trackid.Trim()) + "'";
                dtCommonFund = GetRecordFromTable(fund);
                if (dtCommonFund != null && dtCommonFund.Rows.Count > 0)
                {
                    string mobile = dtCommonFund.Rows[0]["RemitterMobile"].ToString();
                    string remitterid = dtCommonFund.Rows[0]["RemitterId"].ToString();
                    string beneficaryId = dtCommonFund.Rows[0]["BenificieryId"].ToString();

                    RemitterDel = GetReitterDetails(remitterid, mobile, agentId);
                    BenDetail = GetBenDetailsByRemitterId(remitterid, mobile, agentId, beneficaryId);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dtCommonFund;
        }

        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        //public static DataTable GetDMTCommision(string groupType)
        //{
        //    string query = "select * from T_DMTMarkupCharges where Group_Type='" + groupType + "'";

        //    return GetRecordFromTable(query);
        //}

        public static DataTable GetDMTCommision(double amount, string groupType)
        {
            DataTable dtRecord = new DataTable();
            try
            {
                string query = "select * from T_DMTMarkupCharges where MinAmount <= " + amount + " and MaxAmount >=" + amount + " and Group_Type='" + groupType + "'";

                DataTable dtMarkupWithGroupType = GetRecordFromTable(query);
                if (dtMarkupWithGroupType.Rows.Count == 0)
                {
                    string query2 = "select * from T_DMTMarkupCharges where MinAmount <= " + amount + " and MaxAmount >=" + amount;
                    dtRecord = GetRecordFromTable(query2);
                }
                else
                {
                    dtRecord = dtMarkupWithGroupType;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dtRecord;
        }

        public static DataTable SP_CheckInstantPayStatus(string userId, string trackId, string mobile, string type)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_CheckInstantPayStatus", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@TrackId", trackId));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", userId));
                sqlCommand.Parameters.Add(new SqlParameter("@RmtMobile", mobile));
                sqlCommand.Parameters.Add(new SqlParameter("@Type", type));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "CheckInstantPayStatus");
                dataTable = dataSet.Tables["CheckInstantPayStatus"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static DataSet SP_AdminCheckInstantPayStatus(string trackId, string transactionDate)
        {
            try
            {
                sqlCommand = new SqlCommand("SP_AdminCheckInstantPayStatus", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@TrackId", trackId));
                sqlCommand.Parameters.Add(new SqlParameter("@TransDate", transactionDate));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "CheckInstantPayStatus");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataSet;
        }

        //public static bool Update_SP_CheckInstantPayStatus(string userId, string trackId, string mobile, string tablename = "")
        //{
        //    try
        //    {
        //        string query = string.Empty;
        //        if (tablename == "T_InstantPayInitiatePayout")
        //        {
        //            query = "update T_InstantPayInitiatePayout set IsCheckStatus=1, Refund='1',RefundTrackId='" + trackId + "' where TrackId='" + trackId + "' and AgentId='" + userId + "' and RemitterMobile='" + mobile + "'";
        //        }
        //        else if (tablename == "T_InstantPayFundTransfer")
        //        {
        //            query = "update T_InstantPayInitiatePayout set IsCheckStatus=1, Refund='1',RefundTrackId='" + trackId + "' where TrackId='" + trackId + "' and AgentId='" + userId + "' and RemitterMobile='" + mobile + "'";
        //        }

        //        return InsertUpdateDataBase(query);
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //    }

        //    return false;
        //}

        public static bool Insert_T_InstantPayFailedRefund(string AgentId, string TrackId, string LedTrackId, string RefundAmount, string IpAddress)
        {
            string query = "insert into T_InstantPayFailedRefund (AgentId,TrackId,LedTrackId,RefundAmount,IpAddress) values ('" + AgentId + "','" + TrackId + "','" + LedTrackId + "','" + RefundAmount + "','" + IpAddress + "')";
            return InsertUpdateDataBase(query);
        }

        public static int Get_DMTAmountRefundedOrNot(string userId, string trackId)
        {
            try
            {
                string query = "select * from T_InstantPayFailedRefund where TrackId='" + trackId + "' and AgentId='" + userId + "'";
                DataTable dtRefund = GetRecordFromTable(query);
                if (dtRefund.Rows.Count > 0)
                {
                    string LedTrackId = dtRefund.Rows[0]["LedTrackId"].ToString();
                    string queryLedger = "select count(*) as TotalCount from LedgerDetails where InvoiceNo='" + LedTrackId + "' and credit>0 and AgentID='" + userId + "'";

                    DataTable dtchkLedger = GetRecordFromTable(queryLedger);
                    if (dtRefund.Rows.Count > 0)
                    {
                        return Convert.ToInt32(dtchkLedger.Rows[0]["TotalCount"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return 0;
        }
    }
}
