﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstantPayServiceLib
{
    public static class LedgerService
    {
        public static List<string> DMTLedgerDebitCredit(double Amount, string AgentId, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType, string mobile, string remitterId, string benid)
        {
            return LedgerDataBase.DMTLedgerDebitCredit(Amount, AgentId, AgencyName, AccountID, IPAddress, Debit, Credit, BookingType, Remark, Uploadtype, shortRemark, Narration, TransType, mobile, remitterId, benid);
        }

        public static List<string> PayoutLedgerDebitCredit(double Amount, string AgentId, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType, string user_Id, string mobile, string remitterId, string benid)
        {
            return LedgerDataBase.PayoutLedgerDebitCredit(Amount, AgentId, AgencyName, AccountID, IPAddress, Debit, Credit, BookingType, Remark, Uploadtype, shortRemark, Narration, TransType, user_Id, mobile, remitterId, benid);
        }

        public static List<string> LedgerDebitCreditForCheckBen_DetailOnline(double Amount, string AgentId, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType, string mobile, string remitterId)
        {
            return LedgerDataBase.LedgerDebitCreditForCheckBen_DetailOnline(Amount, AgentId, AgencyName, AccountID, IPAddress, Debit, Credit, BookingType, Remark, Uploadtype, shortRemark, Narration, TransType, mobile, remitterId);
        }

        public static List<string> LedgerDMTDebitCreditForCheckBen_DetailOnline(double Amount, string AgentId, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType, string mobile, string remitterId)
        {
            return LedgerDataBase.LedgerDMTDebitCreditForCheckBen_DetailOnline(Amount, AgentId, AgencyName, AccountID, IPAddress, Debit, Credit, BookingType, Remark, Uploadtype, shortRemark, Narration, TransType, mobile, remitterId);
        }

        public static List<string> LedgerDebitCreditUtility(double Amount, string AgentId, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType)
        {
            return LedgerDataBase.LedgerDebitCreditUtility(Amount, AgentId, AgencyName, AccountID, IPAddress, Debit, Credit, BookingType, Remark, Uploadtype, shortRemark, Narration, TransType);
        }

        public static List<string> SimpleLedgerDebitCreditUtility(double Amount, string AgentId, string AgencyName, string TrackId, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType)
        {
            return LedgerDataBase.SimpleLedgerDebitCreditUtility(Amount, AgentId, AgencyName, TrackId, AccountID, IPAddress, Debit, Credit, BookingType, Remark, Uploadtype, shortRemark, Narration, TransType);
        }

        public static DataTable GetRemBenDetailByAgencyId(string agencyId, string mobile, string remitterid, string benid)
        {
            return InstantPay_DataBase.GetRemBenDetailByAgencyId(agencyId, mobile, remitterid, benid);
        }

        public static DataTable GetDMTRemBenDetailByAgencyId(string agencyId, string mobile, string remitterid, string benid)
        {
            return InstantPay_DataBase.GetDMTRemBenDetailByAgencyId(agencyId, mobile, remitterid, benid);
        }

        public static DataSet GetSMBPUtilityCommission(string amount, string agentGroupType, string spkey)
        {
            return InstantPay_DataBase.GetSMBPUtilityCommission(amount, agentGroupType, spkey);
        }
    }
}