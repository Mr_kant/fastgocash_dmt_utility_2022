﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using static InstantPayServiceLib.InstantPay_Model;

namespace InstantPayServiceLib
{
    public static class InstantPay_ApiService
    {
        private static string TokenID = "ffecc13de31e6be9fe916654e0bcf61f";//"ffecc13de31e6be9fe916654e0bcf61f"
        private static string PostUrl { get; set; }
        private static int OutletId = 1;

        private static string GetPostUrl()
        {
            //return "https://www.instantpay.in/ws/dmi/";
            return "https://api.instantpay.in/fi/remit/out/domestic/";
        }

        private static string GetInitiatePayoutPostUrl()
        {
            return "https://www.instantpay.in/ws/payouts/";
        }

        private static string GetTrackId()
        {
            return "DMT" + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 15).ToUpper();
        }

        #region [API SECTION]

        public static string GetRemitterDetailByUserId(string agentId, string remtmobile)
        {
            string result = string.Empty;
            DataTable dtRemtDetail = InstantPay_DataBase.GetRemitterDetailByUserId(null, remtmobile);
            if (dtRemtDetail != null && dtRemtDetail.Rows.Count > 0)
            {
                DataTable dtUserRemtDetail = InstantPay_DataBase.GetRemitterDetailByUserId(agentId, remtmobile);
                if (dtUserRemtDetail != null && dtUserRemtDetail.Rows.Count > 0)
                {
                    result = "rmtexistwithuser";
                }
                else
                {
                    result = "rmtnotexistwithuser";
                }
            }
            else
            {
                result = "rmtnotexist";
            }
            return result;
        }
        public static string GetReSendRemitterDetail(string mobile, string agentId)
        {
            string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"mobile\": \"" + mobile + "\",\"outletid\":" + OutletId + "}}";
            PostUrl = GetPostUrl() + "remitter_details";

            return Post("POST", PostUrl, reqJson, "GetRemitterDetail", agentId, GetTrackId());
        }
        public static DataTable GetLedgerDetail(string agentId, string trackid)
        {
            return InstantPay_DataBase.GetLedgerDetail(agentId, trackid);
        }
        public static bool UpdateIPFundTransferDetail(string agentId, string fundTransId, string ipay_id, string status)
        {
            return InstantPay_DataBase.UpdateIPFundTransferDetail(agentId, fundTransId, ipay_id, status);
        }
        public static string GetTransStatusDetail(string orderid, string agentId, string trackid)
        {
            string result = string.Empty;
            try
            {
                string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"order_id\":\"" + orderid + "\"}}";
                PostUrl = "https://www.instantpay.in/ws/status/checkbyorderid";

                result = Post("POST", PostUrl, reqJson, "GetStatusDetail", agentId, GetTrackId());
                //if (!string.IsNullOrEmpty(result))
                //{
                //    CheckTransStatus checkTransStatus = JsonConvert.DeserializeObject<CheckTransStatus>(result);
                //    if (checkTransStatus != null)
                //    {
                //        if (checkTransStatus.data != null)
                //        {
                //            double debitAmt = 0;
                //            DataTable dtLedgerDel = InstantPay_DataBase.GetLedgerDetail(agentId, trackid);
                //            if (dtLedgerDel != null && dtLedgerDel.Rows.Count > 0)
                //            {
                //                debitAmt = dtLedgerDel.Rows[0]
                //            }


                //            if (!string.IsNullOrEmpty(checkTransStatus.data.transaction_status))
                //            {
                //                if (checkTransStatus.data.transaction_status.ToUpper().Contains("SUCCESS"))
                //                {
                //                    result = checkTransStatus.data.transaction_description;
                //                }
                //                else if (checkTransStatus.data.transaction_status.ToUpper().Contains("REFUND"))
                //                {
                //                    result = checkTransStatus.data.transaction_description;
                //                }
                //                else
                //                {
                //                    result = checkTransStatus.data.transaction_description;
                //                }
                //            }
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }
        public static string GetRemitterDetail(string mobile, string agentId, ref string remitterId)
        {
            string result = string.Empty;

            try
            {
                if (InstantPay_DataBase.GetRemitterId(mobile, agentId))
                {
                    //string reqJson = "";
                    PostUrl = GetPostUrl() + "remitterProfile?mobile=" + mobile;
                    result = InstantPay_NewAPI_Modified.Post("GET", PostUrl, "", "GetRemitterDetail", agentId, GetTrackId());

                    //result = "{\"statuscode\":\"TXN\",\"actcode\":null,\"status\":\"Success\",\"data\":{\"mobile\":\"9555266308\",\"firstName\":\"KRISHNA\",\"lastName\":\"KANT\",\"address\":\"NEW DELHI WEST DELHI DELHI\",\"city\":\"NEW DELHI\",\"state\":\"DELHI\",\"pincode\":\"110059\",\"limitConsumed\":\"1.00\",\"limitAvailable\":\"24999.00\",\"limitIncreaseOffer\":false,\"allowProfileUpdate\":true,\"beneficiaries\":[{\"id\":\"028e7d4b25df589ab705b9948b534c10\",\"name\":\"SHIVANI ROY\",\"account\":\"1450100100005960\",\"ifsc\":\"PUNB0244200\",\"bank\":\"PUNJAB NATIONAL BANK\",\"verificationDt\":\"2022-06-03 01:06:25\"},{\"id\":\"1d337a3b2ece746f5dd4c7279fa16d97\",\"name\":\"KRISHNA KANT\",\"account\":\"008591800075364\",\"ifsc\":\"YESB0000001\",\"bank\":\"YES BANK\",\"verificationDt\":\"2022-11-15 23:50:51\"}]},\"timestamp\":\"2022-11-16 22:49:54\",\"ipay_uuid\":\"h06897c2916f-30f2-4753-b49a-47d65fb4bc57\",\"orderid\":null,\"environment\":\"SANDBOX\",\"internalCode\":null}";

                    //result = "{\"statuscode\":\"RNF\",\"actcode\":null,\"status\":\"Remitter Not Found\",\"data\":null,\"timestamp\":\"2022-11-15 23:41:12\",\"ipay_uuid\":\"h00597c0a0cc-8394-41e5-969f-c24d2bf99f34\",\"orderid\":null,\"environment\":\"SANDBOX\",\"internalCode\":null}";

                    //string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"mobile\": \"" + mobile + "\",\"outletid\":" + OutletId + "}}";
                    //PostUrl = GetPostUrl() + "remitter_details";
                    //result = Post("POST", PostUrl, reqJson, "GetRemitterDetail", agentId, GetTrackId());
                    if (!string.IsNullOrEmpty(result))
                    {
                        dynamic dyResult = JObject.Parse(result);
                        string statusCode = dyResult.statuscode;
                        string statusMessage = dyResult.status;

                        if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "success")
                        {
                            dynamic dyData = dyResult.data;
                            //dynamic remitter = dyData.remitter;
                            dynamic beneficiary = dyData.beneficiaries;

                            //string remitterId = ""; //remitter.id;
                            string remittermobile = dyData.mobile;

                            string limitConsumed = dyData.limitConsumed;
                            string limitAvailable = dyData.limitAvailable;

                            if (!IsRemitterExist(remitterId, remittermobile, agentId))
                            {
                                DateTime dtTodayDate = DateTime.Now;
                                remitterId = dtTodayDate.Year.ToString() + dtTodayDate.Month.ToString() + dtTodayDate.Day.ToString() + dtTodayDate.Minute.ToString() + dtTodayDate.Millisecond.ToString() + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10).ToUpper();
                                //remitterId = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10).ToUpper();
                                InsertAlreadyRegRemitterDetails(result, agentId, remitterId);
                            }
                            else
                            {
                                remitterId = IsRemitterExistRemitterId(remitterId, remittermobile, agentId);
                            }

                            //if (!IsRemitterExist(remitterId, remittermobile))
                            //{
                            //    InsertRemitterDetails(result, agentId);
                            //}
                            //else
                            //{
                            //    UpdateInstantPayRemitterRegResponse(result, agentId);
                            //}

                            InsertBeneficiaryDetails(remitterId, agentId, remittermobile, beneficiary);
                            bool isUpdate = InstantPay_DataBase.UpdateRemainingRemitterLimit(remitterId, remittermobile, agentId, limitConsumed, limitAvailable);
                        }
                        //else if (statusCode.ToLower() == "OTP".ToLower() && statusMessage.ToLower().Contains("OTP sent to remitter mobile number".ToLower()))
                        //{
                        //    dynamic dyData = dyResult.data;
                        //    string otpReference = dyData.otpReference;
                        //    bool issuccess = InstantPay_DataBase.UpdateOtpOfExistingRecord(mobile, agentId, otpReference);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        public static string RemitterRegistration(string mobile, string firstName, string lastName, string pinCode, string localadd, string agentId, ref string remitterId)
        {
            string result = string.Empty;

            //string reqJson = "{\"token\":\"" + TokenID + "\",\"request\": {\"mobile\": \"" + mobile + "\",\"name\":\"" + firstName + "\",\"surname\":\"" + lastName + "\",\"pincode\":\"" + pinCode + "\",\"outletid\":" + OutletId + "}}";
            //PostUrl = GetPostUrl() + "remitter";

            string reqJson = "{\"mobile\":\"" + mobile + "\",\"firstName\":\"" + firstName + "\",\"lastName\":\"" + lastName + "\",\"pinCode\":\"" + pinCode + "\"}";
            PostUrl = GetPostUrl() + "remitterRegistration";

            //int RemtID = InstantPay_DataBase.InsertRemitterGetId(mobile, firstName, lastName, pinCode, localadd, agentId);            
            //remitterId = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10).ToUpper(); //remitter.id;
            DateTime dtTodayDate = DateTime.Now;
            remitterId = dtTodayDate.Year.ToString() + dtTodayDate.Month.ToString() + dtTodayDate.Day.ToString() + dtTodayDate.Minute.ToString() + dtTodayDate.Millisecond.ToString() + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10).ToUpper();
            int RemtID = InstantPay_DataBase.InsertRemitterFirstDetail(mobile, firstName, lastName, pinCode, localadd, agentId, remitterId);
            if (RemtID > 0)
            {
                result = InstantPay_NewAPI_Modified.Post("POST", PostUrl, reqJson, "Remitter_Registration", agentId, GetTrackId());
                //result = "{\"statuscode\":\"OTP\",\"actcode\":null,\"status\":\"OTP sent to remitter mobile number XXXXXXX750\",\"data\":{\"otpReference\":\"397279593052314f65724a614a55626a457668337a394a49557a427331322b624979304659767a564e753861666b2b73746575353458576278466b4c6d314f546538494d344a4668777470644a3855347245705a66564f4463684f4a4b75534b763733545a33665762436b5469387a5a695553782f45594e754f52305966535a555a636e5137396b42723847673073364e537031306a357444416b6c703766446e4f73682b32756343695934347a52315854704c436c4f4e4666306a336465512f6844754f79754e576855344e5735656d71332b37435673763949555431693943693435676443455156303d\"},\"timestamp\":\"2022-11-17 00:22:44\",\"ipay_uuid\":\"h00597c2b2a1-2793-45fd-ba4a-2ecca6243254\",\"orderid\":null,\"environment\":\"SANDBOX\",\"internalCode\":null}";
                //UpdateRemitterRegistration(RemtID, result, agentId);

                UpdateRemitterRegDetail(RemtID, mobile, agentId, result, remitterId);
            }

            return result;
        }
        public static string RemitterRegistrationValidate(string remitterid, string mobile, string otp, string agentId)
        {
            string response = string.Empty;
            DataTable dtRemitterDetail = InstantPay_DataBase.GetRemitterDetailDuringRegistrationWithOtp(agentId, remitterid, mobile);
            if (dtRemitterDetail.Rows.Count > 0)
            {
                string otpReference = dtRemitterDetail.Rows[0]["OtpReference"].ToString();
                if (!string.IsNullOrEmpty(otpReference.Trim()))
                {
                    string reqJson = "{\"otpReference\": \"" + otpReference.Trim() + "\",\"otp\": \"" + otp + "\"}";
                    PostUrl = GetPostUrl() + "otpVerification";

                    response = InstantPay_NewAPI_Modified.Post("POST", PostUrl, reqJson, "Remitter_Validate", agentId, GetTrackId());

                    if (!string.IsNullOrEmpty(response))
                    {
                        dynamic dyOTPResult = JObject.Parse(response);
                        string otpstatusCode = dyOTPResult.statuscode;
                        string otpstatusMessage = dyOTPResult.status;

                        if (otpstatusCode.ToLower() == "txn" && otpstatusMessage.ToLower() == "transaction successful")
                        {
                            string RegId = string.Empty;
                            if (IsRemitterAlreadyExist(remitterid, mobile, agentId, ref RegId))
                            {
                                PostUrl = GetPostUrl() + "remitterProfile?mobile=" + mobile;

                                string remittResult = InstantPay_NewAPI_Modified.Post("GET", PostUrl, "", "GetRemitterDetail", agentId, GetTrackId());
                                if (!string.IsNullOrEmpty(remittResult))
                                {
                                    dynamic dyRemittResult = JObject.Parse(remittResult);
                                    string remittstatusCode = dyRemittResult.statuscode;
                                    string remittstatusMessage = dyRemittResult.status;

                                    if (remittstatusCode.ToLower() == "txn" && remittstatusMessage.ToLower() == "Success".ToLower())
                                    {
                                        dynamic dyRemittData = dyRemittResult.data;
                                        //dynamic remitter = dyRemittData.remitter;
                                        //dynamic beneficiary = dyRemittData.beneficiary;
                                        dynamic remitter_limit = "25000";

                                        string id = remitterid;
                                        string remtmobile = dyRemittData.mobile;
                                        string firstName = dyRemittData.firstName;
                                        string remtaddress = dyRemittData.address;
                                        string pinCode = dyRemittData.pincode;
                                        string city = dyRemittData.city;
                                        string state = dyRemittData.state;
                                        string kycstatus = "0";
                                        string is_verified = "1";
                                        string consumedlimit = dyRemittData.limitConsumed;
                                        string remaininglimit = dyRemittData.limitAvailable;
                                        string perm_txn_limit = "5000";
                                        string totalLimit = "25000";

                                        bool isUpdated = InstantPay_DataBase.UpdateFreshRemitterDetails(RegId, id, agentId, firstName, remtmobile, remtaddress, pinCode, city, state, kycstatus, is_verified, consumedlimit, remaininglimit, perm_txn_limit, totalLimit);
                                    }
                                }
                            }
                        }
                    }
                }
            }



            //string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"remitterid\": \"" + remitterid + "\",\"mobile\": \"" + mobile + "\",\"otp\": \"" + otp + "\",\"outletid\":" + OutletId + "}}";
            //PostUrl = GetPostUrl() + "remitter_validate";

            //result = Post("POST", PostUrl, reqJson, "Remitter_Registration_Validate", agentId, GetTrackId());


            return response;
        }
        private static bool IsRemitterAlreadyExist(string remitterid, string mobile, string agentId, ref string RegId)
        {
            return InstantPay_DataBase.IsRemitterAlreadyExist(remitterid, mobile, agentId, ref RegId);
        }
        public static string BeneficiaryRegistration(string remitterid, string name, string mobile, string account, string bankname, string ifsc, string agentId, string bankId)
        {
            string response = string.Empty;

            try
            {
                DataTable dtBenDel = GetRecordFromTable("select * from T_InstantPayBeneficary where BeneficiaryId is not null and Account='" + account + "' and RemitterId='" + remitterid + "' and Mobile='" + mobile + "' and AgentId='" + agentId + "'");
                if (dtBenDel.Rows.Count == 0)
                {
                    //string reqJson = "{\"token\": \"" + TokenID + "\", \"request\": {\"remitterid\": \"" + remitterid + "\",\"name\": \"" + name + "\",\"mobile\": \"" + mobile + "\",\"ifsc\": \"" + ifsc + "\",\"account\": \"" + account + "\"}}";
                    //PostUrl = GetPostUrl() + "beneficiary_register";
                    //int benId = InsertT_InstantPayBeneficary(remitterid, name, mobile, account, bankname, ifsc, agentId);
                    //response = Post("POST", PostUrl, reqJson, "Beneficiary_Registration", agentId, GetTrackId());

                    string reqJson = "{\"remitterMobile\": \"" + mobile + "\",\"firstName\": \"" + name + "\",\"ifsc\": \"" + ifsc + "\",\"accountNumber\": \"" + account + "\",\"bankId\": " + bankId + "}";
                    PostUrl = GetPostUrl() + "beneficiaryRegistration";

                    int benId = InsertT_InstantPayBeneficary(remitterid, name, mobile, account, bankname, ifsc, agentId);
                    response = InstantPay_NewAPI_Modified.Post("POST", PostUrl, reqJson, "Beneficiary_Registration", agentId, GetTrackId());

                    if (!string.IsNullOrEmpty(response))
                    {
                        if (!string.IsNullOrEmpty(response))
                        {
                            dynamic dyResult = JObject.Parse(response);
                            string statusCode = dyResult.statuscode;
                            string statusMessage = dyResult.status;

                            if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "beneficiary details")
                            {
                                if (benId > 0)
                                {
                                    Update_T_InstantPayBeneficary(benId, remitterid, response);
                                }
                            }
                            else
                            {
                                InstantPay_DataBase.Delete_T_InstantPayBeneficary(benId);
                            }
                        }
                    }
                }
                else
                {
                    response = "exist";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }


            return response;
        }
        public static string BeneficiaryRemove(string beneficiaryid, string remitterid, string agentId, string remitterMobile)
        {
            //string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"beneficiaryid\": \"" + beneficiaryid + "\",\"remitterid\": \"" + remitterid + "\",\"outletid\":" + OutletId + "}}";
            string reqJson = "{\"remitterMobile\": \"" + remitterMobile + "\",\"beneficiaryId\": \"" + beneficiaryid + "\"}";
            PostUrl = GetPostUrl() + "beneficiaryDelete";

            string response = InstantPay_NewAPI_Modified.Post("DELETE", PostUrl, reqJson, "Beneficiary_Remove", agentId, GetTrackId());

            if (!string.IsNullOrEmpty(response))
            {
                dynamic dyResult = JObject.Parse(response);
                string statusCode = dyResult.statuscode;
                string statusMessage = dyResult.status;

                //if (statusCode.ToLower() == "err" && statusMessage.ToLower() == "invalid beneficiary id")
                //{
                //    bool isdeleted = InstantPay_DataBase.DeleteBeneficiaryDetail(beneficiaryid, remitterid, agentId);
                //}

                if (statusCode.ToLower() == "OTP".ToLower() && statusMessage.ToLower().Contains("OTP sent to remitter mobile number".ToLower()))
                {
                    dynamic dyData = dyResult.data;
                    string otpReference = dyData.otpReference;
                    bool isdeleted = InstantPay_DataBase.UpdateOtpReferenceForBenDelete(beneficiaryid, remitterid, remitterMobile, agentId, otpReference);
                }
            }

            return response;
        }
        public static string BeneficiaryRemoveValidate(string beneficiaryid, string mobile, string remitterid, string otp, string agentId)
        {
            //string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"beneficiaryid\": \"" + beneficiaryid + "\",\"remitterid\": \"" + remitterid + "\",\"otp\": \"" + otp + "\",\"outletid\":" + OutletId + "}}";
            //PostUrl = GetPostUrl() + "beneficiary_remove_validate";

            //string response = Post("POST", PostUrl, reqJson, "Beneficiary_Remove_Validate", agentId, GetTrackId());

            //if (!string.IsNullOrEmpty(response))
            //{
            //    DeleteBeneficiaryDetail(beneficiaryid, remitterid, agentId, response);
            //}
            string response = string.Empty;
            DataTable dtBenDetail = InstantPay_DataBase.GetBenificiaryByRemitterId(remitterid, mobile, agentId, beneficiaryid);
            if (dtBenDetail.Rows.Count > 0)
            {
                string otpReference = dtBenDetail.Rows[0]["otpReference"].ToString();
                if (!string.IsNullOrEmpty(otpReference.Trim()))
                {
                    string reqJson = "{\"otpReference\": \"" + otpReference.Trim() + "\",\"otp\": \"" + otp + "\"}";
                    PostUrl = GetPostUrl() + "otpVerification";

                    response = InstantPay_NewAPI_Modified.Post("POST", PostUrl, reqJson, "Beneficiary_Remove_Validate", agentId, GetTrackId());

                    if (!string.IsNullOrEmpty(response))
                    {
                        dynamic dyBenDeleteResult = JObject.Parse(response);
                        string statusCode = dyBenDeleteResult.statuscode;
                        string statusMessage = dyBenDeleteResult.status;
                        if (statusCode.ToString().Trim().ToLower() == "TXN".ToLower() && statusMessage.ToString().Trim().ToLower() == "Beneficiary removed successfully".ToLower())
                        {
                            DeleteBeneficiaryDetail(beneficiaryid, mobile, remitterid, agentId, response);
                        }
                    }
                }
            }

            return response;
        }
        public static string FundTransfer(string remittermobile, string beneficiaryid, string amount, string transferMode, string uniqueAgentId, string agentId, string trackid, string RemitterId, string ledgerAmount)
        {
            //string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"remittermobile\": \"" + remittermobile + "\",\"beneficiaryid\": \"" + beneficiaryid + "\",\"agentid\": \"" + uniqueAgentId + "\",\"amount\": \"" + amount + "\",\"mode\": \"" + transferMode + "\"}}";
            //PostUrl = GetPostUrl() + "transfer";

            string latitude = InstantPayConfig.Latitude;
            string longitude = InstantPayConfig.Longitude;
            string response = string.Empty;
            if (!string.IsNullOrEmpty(latitude) && !string.IsNullOrEmpty(longitude))
            {
                string reqJson = "{\"remitterMobile\": \"" + remittermobile + "\",\"beneficiaryId\": \"" + beneficiaryid + "\",\"externalRef\": \"" + uniqueAgentId + "\",\"transferAmount\": \"" + amount + "\",\"transferMode\": \"" + transferMode + "\",\"latitude\": \"" + latitude + "\",\"longitude\": \"" + longitude + "\"}";
                PostUrl = GetPostUrl() + "fundTransfer";

                int fundTrandId = InsertFundTranferDetail(remittermobile, beneficiaryid, amount, transferMode, agentId, trackid, RemitterId, ledgerAmount, uniqueAgentId);
                response = InstantPay_NewAPI_Modified.Post("POST", PostUrl, reqJson, "Fund_Transfer", agentId, trackid);
                //string response = "{'statuscode':'TXN','status':'Transaction Successful','data':{'ipay_id':'1200908232229HLGZB','ref_no':'025223627059','opr_id':'025223627059','name':'KRISHNA KANT SO JINI','opening_bal':'24053.30','amount':1,'charged_amt':'7.66','locked_amt':0,'ccf_bank':10,'bank_alias':'PPBL'},'timestamp':'2020-09-08 23:22:31','ipay_uuid':'1F01952A1C47C5654BCE','orderid':'1200908232229HLGZB','environment':'PRODUCTION'}";
                //string response="{'statuscode':'ERR','status':'Transaction failed','data':{'ipay_id':'1200912163657DKNNY','ref_no':'00','opr_id':'00','name':'','opening_bal':'15987.71','amount':10,'charged_amt':'0.00','locked_amt':0,'ccf_bank':10,'bank_alias':'SANDBOX'},'timestamp':'2020-09-12 16:36:59','ipay_uuid':'A91B82783F664FDED2C5','orderid':'1200912163657DKNNY','environment':'SANDBOX'}"
                bool isUpdate = UpdateFundTranferDetail(fundTrandId, remittermobile, agentId, response, RemitterId, beneficiaryid);
                if (isUpdate)
                {
                    bool isRemitterLimitUpdate = UpdateRemainingRemitterLimit(remittermobile, agentId, RemitterId);
                }
            }
            return response;
        }
        public static string GetRemitterRemingLimit(string remitterId, string agentId, string mobile)
        {
            return InstantPay_DataBase.GetRemitterRemingLimit(remitterId, agentId, mobile);
        }
        public static DataTable GetTransactionHistory(string mobile, string remitterId)
        {
            return InstantPay_DataBase.GetTransactionHistory(mobile, remitterId);
        }
        public static DataTable GetFilterTransactionHistory(string remitterId, string fromDate, string toDate, string trackId, string filstatus)
        {
            return InstantPay_DataBase.GetFilterTransactionHistory(remitterId, fromDate, toDate, trackId, filstatus);
        }
        public static bool ProcessToReFund(string transid, string reportid)
        {
            return InstantPay_DataBase.ProcessToReFund(transid, reportid);
        }

        //public static string GetBankDetails(string account, string agentId)
        //{
        //    string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"account\": \"" + account + "\",\"outletid\":" + OutletId + "}}";
        //    PostUrl = GetPostUrl() + "bank_details";

        //    return Post("POST", PostUrl, reqJson, "Get_Bank_Details", agentId, GetTrackId());
        //}
        public static bool BindBankDetails(string agentId)
        {
            try
            {
                string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"outletid\":" + OutletId + "}}";
                PostUrl = GetPostUrl() + "bank_details";

                string JsonResponse = Post("POST", PostUrl, reqJson, "GetBankDetail", agentId, GetTrackId());
                if (!string.IsNullOrEmpty(JsonResponse))
                {
                    dynamic dyResult = JObject.Parse(JsonResponse);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                    {
                        BankDetail myDeserializedClass = JsonConvert.DeserializeObject<BankDetail>(JsonResponse);

                        return InstantPay_DataBase.BindBankDetails(myDeserializedClass.data);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static string Post(string postType, string Url, string ReqJson, string actionType, string agentId, string trackid)
        {
            string ReturnValue = string.Empty;
            InstantPay_DataBase.Information(Url, "Request captured in post Method : " + Url + "", ReqJson, "", actionType, agentId, trackid);

            StringBuilder sbResult = new StringBuilder();

            //ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(Url);

            try
            {
                Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");

                Http.Method = postType;
                byte[] lbPostBuffer = Encoding.UTF8.GetBytes(ReqJson);
                Http.ContentLength = lbPostBuffer.Length;
                Http.Timeout = 130000; //5000 milliseconds == 5 seconds// 900000 milliseconds == 900 seconds- 15 mints
                Http.ContentType = "application/json";
                Http.Accept = "application/json";

                using (Stream PostStream = Http.GetRequestStream())
                {
                    PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        ReturnValue = sbResult.ToString();
                        responseStream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                InstantPay_DataBase.Error("Error : Request in Post Method : " + Url + "", Url, ReqJson, "Response in Post Method : " + ReturnValue + "", "Exception in Post Method:" + ex.Message.Replace("'", "''") + "", actionType, agentId, trackid);
                ReturnValue = null;
            }
            finally
            {
                Http.Abort();
                Http = null;
            }
            InstantPay_DataBase.Information(Url, "Request captured in post Method : " + Url + "", ReqJson, ReturnValue, actionType, agentId, trackid);
            return ReturnValue;
        }
        #endregion

        #region [Data Base Section]
        private static bool DeleteBeneficiaryDetail(string benid, string mobile, string remitterId, string agentId, string response)
        {
            try
            {
                if (!string.IsNullOrEmpty(response))
                {
                    dynamic dyResult = JObject.Parse(response);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                    {
                        return InstantPay_DataBase.DeleteBeneficiaryDetail(benid, mobile, remitterId, agentId);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        //private static bool UpdateRemitterRegistration(int remtID, string resultRespo, string agentId)
        //{
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(resultRespo))
        //        {
        //            dynamic dyResult = JObject.Parse(resultRespo);
        //            string statusCode = dyResult.statuscode;
        //            string statusMessage = dyResult.status;

        //            if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "otp sent successfully")
        //            {
        //                dynamic dyData = dyResult.data;
        //                dynamic remitter = dyData.remitter;

        //                string remitterId = remitter.id;
        //                string verifiedStatus = remitter.is_verified;

        //                if (!InstantPay_DataBase.CheckRemitterExist(remitterId))
        //                {
        //                    return InstantPay_DataBase.UpdateRemitterRegistration(remtID, remitterId, verifiedStatus, agentId);
        //                }
        //                else
        //                {
        //                    //return InstantPay_DataBase.DeleteRemitter(remtID);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //    }

        //    return false;
        //}
        private static bool UpdateRemitterRegistrationValidate(string response, string remitterid, string mobile, string agentId)
        {
            try
            {
                if (!string.IsNullOrEmpty(response))
                {
                    dynamic dyResult = JObject.Parse(response);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                    {
                        dynamic dyData = dyResult.data;
                        dynamic remitter = dyData.remitter;

                        string remitterId = remitter.id;
                        string verifiedStatus = remitter.is_verified;

                        return InstantPay_DataBase.UpdateRemitterRegistrationValidate(remitterId, mobile, verifiedStatus, agentId);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        private static bool UpdateInstantPayRemitterRegResponse(string remtRespo, string agentId)
        {
            try
            {
                if (!string.IsNullOrEmpty(remtRespo))
                {
                    RemitterRegResponse respo = new RemitterRegResponse();
                    bool executeUpdate = false;

                    dynamic dyResult = JObject.Parse(remtRespo);
                    dynamic dyData = dyResult.data;
                    dynamic remitter = dyData.remitter;

                    dynamic remitter_limit = dyData.remitter_limit[0];
                    dynamic remitter_mode = remitter_limit.mode;
                    dynamic remitter_limit_limit = remitter_limit.limit;

                    //string imps = ""; string neft = ""; decimal creditLtm = 0; decimal consumedAmount = 0; decimal remainningAmount = 0;
                    //foreach (var item in remitter_limit.mode[0])
                    //{
                    //    imps = item.imps;
                    //    neft = item.neft;
                    //}
                    //foreach (var item in remitter_limit.limit)
                    //{
                    //    creditLtm = creditLtm + Convert.ToDecimal(item.totalAmount);
                    //    consumedAmount = consumedAmount + Convert.ToDecimal(item.consumedAmount);
                    //    remainningAmount = remainningAmount + Convert.ToDecimal(item.remainningAmount);
                    //}

                    respo.RemitterId = remitter.id;
                    respo.Address = remitter.address;
                    respo.City = remitter.city;
                    respo.IsVerified = remitter.is_verified;
                    respo.KYCDoc = remitter.kycdocs;
                    respo.KYCStatus = remitter.kycstatus;
                    respo.Mobile = remitter.mobile;
                    respo.Name = remitter.name;
                    respo.PernTxnLimit = remitter.perm_txn_limit;
                    respo.PinCode = remitter.pincode;
                    respo.State = remitter.state;
                    respo.CreditLimit = remitter_limit_limit.total;
                    respo.ConsumedAmount = remitter_limit_limit.consumed;
                    respo.RemainingLimit = remitter_limit_limit.remaining;
                    respo.IMPSMode = remitter_mode.imps;
                    respo.NEFTMode = remitter_mode.neft;
                    //respo.CreditLimit = creditLtm.ToString();
                    //respo.ConsumedAmount = consumedAmount.ToString();
                    //respo.RemainingLimit = remainningAmount.ToString();
                    //respo.IMPSMode = imps;
                    //respo.NEFTMode = neft;

                    if (InstantPay_DataBase.UpdateRemitterRegResponse(respo))
                    {
                        return true;
                    }
                    //DataTable dtRemttDetail = InstantPay_DataBase.GetReitterResponseDetails(respo.RemitterId);
                    //if (dtRemttDetail != null && dtRemttDetail.Rows.Count > 0)
                    //{
                    //    string address = dtRemttDetail.Rows[0]["Address"].ToString();
                    //    string pincode = dtRemttDetail.Rows[0]["PinCode"].ToString();

                    //    if (string.IsNullOrEmpty(address) && string.IsNullOrEmpty(pincode))
                    //    {
                    //        executeUpdate = true;
                    //    }
                    //}

                    //if (executeUpdate)
                    //{
                    //    if (InstantPay_DataBase.UpdateRemitterRegResponse(respo))
                    //    {
                    //        return true;
                    //    }
                    //}
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static int InsertFundTranferDetail(string remittermobile, string beneficiaryid, string amount, string transferMode, string agentId, string trackid, string RemitterId, string ledgerAmount, string transTrackId)
        {
            try
            {
                return InstantPay_DataBase.InsertT_InstantPayFundTransfer(remittermobile, beneficiaryid, amount, transferMode, agentId, trackid, RemitterId, ledgerAmount, transTrackId);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return 0;
        }
        public static bool UpdateFundTranferDetail(int fundTrandId, string mobileno, string agentId, string response, string remitterId, string beneficiaryid)
        {
            try
            {
                if (!string.IsNullOrEmpty(response))
                {
                    dynamic dyResult = JObject.Parse(response);
                    dynamic dyData = dyResult.data;
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    FundTransfer fund = new FundTransfer();

                    fund.FundTransId = fundTrandId;
                    if (statusCode.ToLower() == "TXN".ToLower() && statusMessage.ToLower() == "Transaction Successful".ToLower())
                    {
                        string externalRef = dyData.externalRef;
                        string poolReferenceId = dyData.poolReferenceId;
                        string txnValue = dyData.txnValue;
                        string txnReferenceId = dyData.txnReferenceId;
                        string remitterMobile = dyData.remitterMobile;
                        string beneficiaryAccount = dyData.beneficiaryAccount;
                        string beneficiaryIfsc = dyData.beneficiaryIfsc;

                        dynamic dyPool = dyData.pool;
                        string poolaccount = dyPool.account;
                        string poolopeningBal = dyPool.openingBal;
                        string poolmode = dyPool.mode;
                        string poolamount = dyPool.amount;
                        string poolclosingBal = dyPool.closingBal;

                        bool isupdated = UpdateRemainingRemitterLimit(mobileno, agentId, remitterId);
                        fund.ipay_id = poolReferenceId;
                        fund.ref_no = txnReferenceId;
                        fund.opr_id = txnReferenceId;
                        fund.name = "";
                        fund.opening_bal = poolopeningBal;
                        fund.charged_amt = poolamount;
                        //fund.locked_amt = dyData.locked_amt;
                        //fund.ccf_bank = dyData.ccf_bank;
                        //fund.bank_alias = dyData.bank_alias;
                        DataTable dtBenDetail = InstantPay_DataBase.GetBenificiaryByRemitterId(remitterId, mobileno, agentId, beneficiaryid);
                        if (dtBenDetail.Rows.Count > 0)
                        {
                            fund.name = dtBenDetail.Rows[0]["Name"].ToString();
                        }
                    }
                    //if (statusCode.ToLower() != "dtx" && statusMessage.ToLower() != "duplicate transaction")
                    //{
                    //    if (statusCode.ToLower() == "err" && statusMessage.ToLower() == "transaction failed")
                    //    {
                    //        fund.RefundId = "RUF" + fundTrandId + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10).ToUpper();
                    //    }
                    //    else
                    //    {
                    //        fund.RefundId = string.Empty;
                    //        bool isupdated = UpdateRemainingRemitterLimit(mobileno, agentId);
                    //    }
                    //    fund.ipay_id = dyData.ipay_id;
                    //    fund.ref_no = dyData.ref_no;
                    //    fund.opr_id = dyData.opr_id;
                    //    fund.name = dyData.name;
                    //    fund.opening_bal = dyData.opening_bal;
                    //    fund.charged_amt = dyData.charged_amt;
                    //    fund.locked_amt = dyData.locked_amt;
                    //    fund.ccf_bank = dyData.ccf_bank;
                    //    fund.bank_alias = dyData.bank_alias;
                    //}
                    else
                    {
                        fund.RefundId = "RUF" + fundTrandId + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10).ToUpper();
                    }
                    fund.timestamp = dyResult.timestamp;
                    fund.ipay_uuid = dyResult.ipay_uuid;
                    fund.orderid = dyResult.orderid;
                    fund.environment = dyResult.environment;
                    fund.Status = statusMessage;

                    return InstantPay_DataBase.UpdateFundTranferDetail(fund, remitterId);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool UpdateRemainingRemitterLimit(string mobile, string agentId, string remitterId)
        {
            try
            {
                //string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"mobile\": \"" + mobile + "\",\"outletid\":" + OutletId + "}}";
                //PostUrl = GetPostUrl() + "remitter_details";

                PostUrl = GetPostUrl() + "remitterTransferLimit?mobile" + mobile;

                string result = InstantPay_NewAPI_Modified.Post("GET", PostUrl, "", "GetRemitterDetail", agentId, GetTrackId());
                if (!string.IsNullOrEmpty(result))
                {
                    dynamic dyResult = JObject.Parse(result);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "Successfully fetched remitter limit".ToLower())
                    {
                        dynamic dyData = dyResult.data;
                        //dynamic remitter = dyData.remitter;
                        //dynamic beneficiary = dyData.beneficiary;

                        //string remitterId = remitter.id;
                        //string remittermobile = remitter.mobile;

                        string consumedlimit = dyData.limitConsumed;
                        string remaininglimit = dyData.limitAvailable;

                        if (!string.IsNullOrEmpty(consumedlimit) && !string.IsNullOrEmpty(remaininglimit))
                        {
                            return InstantPay_DataBase.UpdateRemainingRemitterLimit(remitterId, mobile, agentId, consumedlimit, remaininglimit);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool InsertBeneficiaryDetails(string remitterId, string agentId, string remittermobile, dynamic beneficiary)
        {
            bool isSuccess = false;

            try
            {
                List<string> benids = new List<string>();

                if (beneficiary != null)
                {
                    DataTable dtBenDetails = GetBenDetailsByRemitterId(remitterId, remittermobile, agentId);
                    if (dtBenDetails != null && dtBenDetails.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtBenDetails.Rows.Count; i++)
                        {
                            benids.Add(dtBenDetails.Rows[i]["BeneficiaryId"].ToString().Trim());
                        }
                    }

                    //bool isDeleted = InstantPay_DataBase.DeleteBeneficiaryByMobile(remittermobile, agentId, remitterId);

                    foreach (var item in beneficiary)
                    {
                        RemitterBenDetail model = new RemitterBenDetail();

                        model.BeneficiaryId = item.id;
                        if (!benids.Contains(model.BeneficiaryId))
                        {
                            model.Name = item.name;
                            model.Mobile = remittermobile;
                            model.Account = item.account;
                            model.IfscCode = item.ifsc;
                            model.RemitterId = remitterId;
                            model.AgentId = agentId;
                            model.verificationDt = item.verificationDt;
                            if (!string.IsNullOrEmpty(model.verificationDt))
                            {
                                model.Status = "1";
                            }
                            else
                            {
                                model.Status = "0";
                            }
                            model.Bank = item.bank;
                            //model.imps = item.imps;
                            //model.lastSuccessDate = item.last_success_date;
                            //model.lastSuccessImps = item.last_success_imps;
                            //model.lastSuccessName = item.last_success_name;
                            //model.TimeStamp = item.timestamp;
                            //model.Ipay_uuid = item.ipay_uuid;
                            //model.OrderId = item.orderid;
                            //model.Environment = item.environment;

                            if (InstantPay_DataBase.InsertT_InstantPayBeneficaryDetails(model))
                            {
                                isSuccess = true;
                            }
                            else
                            {
                                isSuccess = false;
                            }
                        }
                        else
                        {
                            DataTable dtBen = InstantPay_DataBase.GetBenDetailsByRemitterId(remitterId, remittermobile, agentId, model.BeneficiaryId);
                            if (dtBen != null && dtBen.Rows.Count > 0)
                            {
                                string bankrespo = !string.IsNullOrEmpty(dtBen.Rows[0]["bank"].ToString()) ? dtBen.Rows[0]["bank"].ToString() : string.Empty;
                                if (string.IsNullOrEmpty(bankrespo))
                                {
                                    model.Name = item.name;
                                    model.Mobile = remittermobile;
                                    model.Account = item.account;
                                    model.IfscCode = item.ifsc;
                                    model.RemitterId = remitterId;
                                    model.AgentId = agentId;
                                    model.verificationDt = item.verificationDt;
                                    if (!string.IsNullOrEmpty(model.verificationDt))
                                    {
                                        model.Status = "1";
                                    }
                                    else
                                    {
                                        model.Status = "0";
                                    }
                                    model.Bank = item.bank;
                                    //model.imps = item.imps;
                                    //model.lastSuccessDate = item.last_success_date;
                                    //model.lastSuccessImps = item.last_success_imps;
                                    //model.lastSuccessName = item.last_success_name;
                                    model.TimeStamp = item.timestamp;
                                    model.Ipay_uuid = item.ipay_uuid;
                                    model.OrderId = item.orderid;
                                    model.Environment = item.environment;
                                    if (InstantPay_DataBase.UpdateT_InstantPayBeneficaryDetails(model))
                                    {
                                        isSuccess = true;
                                    }
                                    else
                                    {
                                        isSuccess = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return isSuccess;
        }
        public static string GetFundTranferHistory(string remitterid, string agentId)
        {
            string result = string.Empty;

            try
            {

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        public static bool IsRemitterExist(string remitterid, string mobile, string agentid)
        {
            try
            {
                DataTable dtRemitter = InstantPay_DataBase.GetReitterDetails(remitterid, mobile, agentid);

                if (dtRemitter != null && dtRemitter.Rows.Count > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static string IsRemitterExistRemitterId(string remitterid, string mobile, string agentid)
        {
            try
            {
                DataTable dtRemitter = InstantPay_DataBase.GetReitterDetails(remitterid, mobile, agentid);

                if (dtRemitter != null && dtRemitter.Rows.Count > 0)
                {
                    return dtRemitter.Rows[0]["id"].ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return "";
        }
        public static bool InsertRemitterDetails(string response, string agentId)
        {
            try
            {
                if (!string.IsNullOrEmpty(response))
                {
                    dynamic dyResult = JObject.Parse(response);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                    {
                        dynamic dyData = dyResult.data;
                        dynamic remitter = dyData.remitter;

                        RemitterRegRequest remttRequest = new RemitterRegRequest();
                        remttRequest.Mobile = remitter.mobile;
                        remttRequest.FirstName = remitter.name;
                        remttRequest.LastName = string.Empty;
                        remttRequest.PinCode = remitter.pincode;
                        remttRequest.AgentUserId = agentId;
                        remttRequest.RemitterId = remitter.id;
                        remttRequest.Status = "already registered";
                        remttRequest.IsKYCStatus = remitter.kycstatus;
                        remttRequest.VerifiedStatus = remitter.is_verified;

                        if (InstantPay_DataBase.Insert_T_InstantPayRemitterReg(remttRequest))
                        {
                            dynamic remitter_limit = dyData.remitter_limit[0];
                            dynamic remitter_mode = remitter_limit.mode;
                            dynamic remitter_limit_limit = remitter_limit.limit;

                            //string imps = ""; string neft = ""; decimal creditLtm = 0; decimal consumedAmount = 0; decimal remainningAmount = 0;
                            //foreach (var item in remitter_mode)
                            //{
                            //    imps = item.imps;
                            //    neft = item.neft;
                            //}
                            //foreach (var item in remitter_limit.limit)
                            //{
                            //    creditLtm = creditLtm + (item["total"] != null ? Convert.ToDecimal(item.total) : (item["totalAmount"] != null ? Convert.ToDecimal(item.totalAmount) : 0));
                            //    consumedAmount = consumedAmount + Convert.ToDecimal(item.consumedAmount);
                            //    remainningAmount = remainningAmount + Convert.ToDecimal(item.remainningAmount);
                            //}

                            RemitterRegResponse remttResponse = new RemitterRegResponse();
                            remttResponse.RemitterId = remitter.id;
                            remttResponse.AgentUserId = agentId;
                            remttResponse.Address = remitter.address;
                            remttResponse.City = remitter.city;
                            remttResponse.IsVerified = remitter.is_verified;
                            remttResponse.KYCDoc = remitter.kycdocs;
                            remttResponse.KYCStatus = remitter.kycstatus;
                            remttResponse.Mobile = remitter.mobile;
                            remttResponse.Name = remitter.name;
                            remttResponse.PernTxnLimit = remitter.perm_txn_limit;
                            remttResponse.PinCode = remitter.pincode;
                            remttResponse.State = remitter.state;
                            remttResponse.CreditLimit = remitter_limit_limit.total;
                            remttResponse.ConsumedAmount = remitter_limit_limit.consumed;
                            remttResponse.RemainingLimit = remitter_limit_limit.remaining;
                            remttResponse.IMPSMode = remitter_mode.imps;
                            remttResponse.NEFTMode = remitter_mode.neft;
                            //remttResponse.CreditLimit = creditLtm.ToString();
                            //remttResponse.ConsumedAmount = consumedAmount.ToString();
                            //remttResponse.RemainingLimit = remainningAmount.ToString();
                            //remttResponse.IMPSMode = imps;
                            //remttResponse.NEFTMode = neft;

                            return InstantPay_DataBase.Insert_T_InstantPayRemitterRegResponse(remttResponse);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static DataTable GetT_InstantPayRemitterRegResponse(string agentid, string remitterid, string mobile)
        {
            DataTable dtRemitter = new DataTable();

            try
            {
                dtRemitter = InstantPay_DataBase.GetT_InstantPayRemitterDetail(agentid, remitterid, mobile);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dtRemitter;
        }
        private static int InsertT_InstantPayBeneficary(string remitterid, string name, string mobile, string account, string bankname, string ifsc, string agentId)
        {
            RemitterBenDetail ben = new RemitterBenDetail();
            ben.Name = name;
            ben.Mobile = mobile;
            ben.Account = account;
            ben.IfscCode = ifsc;
            ben.RemitterId = remitterid;
            ben.AgentId = agentId;
            ben.ReqBank = bankname;

            return InstantPay_DataBase.InsertT_InstantPayBeneficary(ben);
        }
        private static bool Update_T_InstantPayBeneficary(int benId, string remitterid, string response)
        {
            if (!string.IsNullOrEmpty(response))
            {
                dynamic dyResult = JObject.Parse(response);
                string statusCode = dyResult.statuscode;
                string statusMessage = dyResult.status;

                if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "beneficiary details")
                {
                    dynamic dyData = dyResult.data;
                    //dynamic remitter = dyData.remitter;
                    //dynamic beneficiary = dyData.beneficiary;

                    RemitterBenDetail ben = new RemitterBenDetail();
                    ben.BenID = benId;
                    ben.RemitterId = remitterid;
                    ben.BeneficiaryId = dyData.id;
                    ben.Status = "1";

                    ben.TimeStamp = dyResult.timestamp;
                    ben.Ipay_uuid = dyResult.ipay_uuid;
                    ben.OrderId = dyResult.orderid;
                    ben.Environment = dyResult.environment;
                    ben.lastSuccessName = dyData.name;
                    ben.Bank = dyData.bank;
                    string verificationDt = dyData.verificationDt;

                    return InstantPay_DataBase.Update_T_InstantPayBeneficary(ben, verificationDt);
                }
            }

            return false;
        }
        public static DataTable GetBenDetailsByRemitterId(string remitterId, string remittermobile, string agentId, string beneficaryId = null)
        {
            return InstantPay_DataBase.GetBenDetailsByRemitterId(remitterId, remittermobile, agentId, beneficaryId);
        }
        public static DataTable GetFundTransferVeryficationDetail(string remitterId, string beneficaryId, ref DataTable bankdetail)
        {
            return InstantPay_DataBase.GetFundTransferVeryficationDetail(remitterId, beneficaryId, ref bankdetail);
        }
        #endregion

        #region [Common]
        public static string Encrypt(string value)
        {
            //byte[] keyArray;
            //byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(value);
            //string key = "dmt";
            //MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            //keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            //hashmd5.Clear();
            //TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //tdes.Key = keyArray;
            //tdes.Mode = CipherMode.ECB;
            //tdes.Padding = PaddingMode.PKCS7;
            //ICryptoTransform ctransform = tdes.CreateEncryptor();
            //byte[] resultArray = ctransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            //tdes.Clear();
            //return Convert.ToBase64String(resultArray, 0, resultArray.Length);

            string encData = null;
            byte[][] keys = GetHashKeys("dmt");

            try
            {
                encData = EncryptStringToBytes_Aes(value, keys[0], keys[1]);
            }
            catch (CryptographicException)
            {
            }
            catch (ArgumentNullException)
            {

            }

            return encData;
        }
        private static byte[][] GetHashKeys(string key)
        {
            byte[][] result = new byte[2][];
            Encoding enc = Encoding.UTF8;

            SHA256 sha2 = new SHA256CryptoServiceProvider();
            int a = 5, b = 6, c = -7, d = -8, f = 4, e = 12;
            int Formula_one = ((a * b) + (c + f) / (d + e));
            string Result = Formula_one.ToString();
            string Raw_key = Result + key;
            byte[] rawKey = enc.GetBytes(Raw_key);



            string Row_IV = Result + key;
            byte[] rawIV = enc.GetBytes(Row_IV);


            string result_byte = Result;
            byte[] bytes = Encoding.ASCII.GetBytes(result_byte);
            byte[] Hash_Key = Combine(bytes, rawKey);
            byte[] Combine(byte[] a1, byte[] a2)
            {
                byte[] ret = new byte[a1.Length + a2.Length];
                Array.Copy(a1, 0, ret, 0, a1.Length);
                Array.Copy(a2, 0, ret, a1.Length, a2.Length);
                return ret;
            }

            byte[] hashKey = sha2.ComputeHash(Hash_Key);


            byte[] bytess = Encoding.ASCII.GetBytes(result_byte);
            byte[] Hash_IV = CombineTime(bytess, rawIV);
            byte[] CombineTime(byte[] a1, byte[] a2)
            {
                byte[] ret = new byte[a1.Length + a2.Length];
                Array.Copy(a1, 0, ret, 0, a1.Length);
                Array.Copy(a2, 0, ret, a1.Length, a2.Length);
                return ret;
            }
            byte[] hashIV = sha2.ComputeHash(Hash_IV);

            Array.Resize(ref hashIV, 16);

            result[0] = hashKey;
            result[1] = hashIV;

            return result;
        }
        private static string EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            byte[] encrypted;

            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt =
                            new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            return Convert.ToBase64String(encrypted);
        }
        public static string Decrypt(string value)
        {
            //byte[] keyArray;
            //byte[] toEncryptArray = Convert.FromBase64String(value);
            //string key = "dmt";
            //MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            //keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            //hashmd5.Clear();
            //TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //tdes.Key = keyArray;
            //tdes.Mode = CipherMode.ECB;
            //tdes.Padding = PaddingMode.PKCS7;
            //ICryptoTransform ctransform = tdes.CreateDecryptor();
            //byte[] resultArray = ctransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            //tdes.Clear();
            //return UTF8Encoding.UTF8.GetString(resultArray);
            string decData = null;
            byte[][] keys = GetHashKeys("dmt");

            try
            {
                decData = DecryptStringFromBytes_Aes(value, keys[0], keys[1]);
            }
            catch (CryptographicException) { }
            catch (ArgumentNullException) { }

            return decData;
        }
        public static string DecryptStringFromBytes_Aes(string cipherTextString, byte[] Key, byte[] IV)
        {

            byte[] cipherText = Convert.FromBase64String(cipherTextString);

            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            string plaintext = null;

            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt =
                            new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }
        #endregion
        public static DataTable GetCombieRemitterDetail(string userId, string remitterId, string mobile)
        {
            return InstantPay_DataBase.GetCombieRemitterDetail(userId, remitterId, mobile);
        }
        public static DataTable GetTransactionHistoryByTrackId(string trackid)
        {
            return InstantPay_DataBase.GetTransactionHistoryByTrackId(trackid);
        }
        public static DataTable GetTransactionHistoryByTransId(string transid, string orderid = null)
        {
            return InstantPay_DataBase.GetTransactionHistoryByTransId(transid, orderid);
        }
        public static DataTable GetPayoutTransactionHistoryByTrackId(string trackid)
        {
            return InstantPay_DataBase.GetPayoutTransactionHistoryByTrackId(trackid);
        }
        public static DataTable GetAgencyDetailById(string agencyId)
        {
            return InstantPay_DataBase.GetAgencyDetailById(agencyId);
        }
        public static DataTable GetTopBindAllBank()
        {
            return InstantPay_DataBase.GetTopBindAllBank();
        }
        public static DataTable GetFurtherBindAllBank()
        {
            return InstantPay_DataBase.GetFurtherBindAllBank();
        }
        public static bool UpdateLocalAddress(string remitterId, string agentId, string mobile, string address)
        {
            return InstantPay_DataBase.UpdateLocalAddress(remitterId, agentId, mobile, address);
        }

        #region [Get Latitude and Longnitude]
        //public static List<string> GetLatitudeLongnitude(string address)
        //{
        //    List<string> result = new List<string>();

        //    try
        //    {
        //        if (!string.IsNullOrEmpty(address))
        //        {
        //            var locationService = new GoogleLocationService();
        //            var point = locationService.GetLatLongFromAddress(address);

        //            var latitude = point.Latitude;
        //            var longitude = point.Longitude;

        //            result.Add(latitude.ToString());
        //            result.Add(longitude.ToString());
        //        }

        //        string url = "http://maps.google.com/maps/api/geocode/xml?address=" + address + "&sensor=false&key=AIzaSyAvwnzh2TvYbh_cTZgKm6XzX2CDDeQJW9A";
        //        WebRequest request = WebRequest.Create(url);

        //        using (WebResponse response = (HttpWebResponse)request.GetResponse())
        //        {
        //            using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
        //            {
        //                DataSet dsResult = new DataSet();
        //                dsResult.ReadXml(reader);
        //                DataTable dtCoordinates = new DataTable();
        //                dtCoordinates.Columns.AddRange(new DataColumn[4] { new DataColumn("Id", typeof(int)),
        //            new DataColumn("Address", typeof(string)),
        //            new DataColumn("Latitude",typeof(string)),
        //            new DataColumn("Longitude",typeof(string)) });
        //                foreach (DataRow row in dsResult.Tables["result"].Rows)
        //                {
        //                    string geometry_id = dsResult.Tables["geometry"].Select("result_id = " + row["result_id"].ToString())[0]["geometry_id"].ToString();
        //                    DataRow location = dsResult.Tables["location"].Select("geometry_id = " + geometry_id)[0];
        //                    dtCoordinates.Rows.Add(row["result_id"], row["formatted_address"], location["lat"], location["lng"]);
        //                }
        //            }
        //            // return dtCoordinates;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //    }

        //    return result;
        //}
        #endregion

        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    string ipAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
        //    if (string.IsNullOrEmpty(ipAddress))
        //    {
        //        ipAddress = Request.ServerVariables["REMOTE_ADDR"];
        //    }

        //    string APIKey = "<Your API Key>";
        //    string url = string.Format("http://api.ipinfodb.com/v3/ip-city/?key={0}&ip={1}&format=json", APIKey, ipAddress);
        //    using (WebClient client = new WebClient())
        //    {
        //        string json = client.DownloadString(url);
        //        Location location = new JavaScriptSerializer().Deserialize<Location>(json);
        //        List<Location> locations = new List<Location>();
        //        locations.Add(location);
        //        gvLocation.DataSource = locations;
        //        gvLocation.DataBind();
        //    }
        //}
        public static string InitiatePayoutSendOTP(string sp_key, string amount, string agentId)
        {
            string clientRefId = GetTrackId();
            string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": { \"sp_key\": \"" + sp_key + "\",\"credit_amount\": \"" + amount + "\"}}";

            PostUrl = GetInitiatePayoutPostUrl() + "direct/create_otp";

            return Post("POST", PostUrl, reqJson, "Generate_OTP", agentId, clientRefId);
        }
        public static int InsertInitiatePayout(InitiatePayout payout)
        {
            try
            {
                return InstantPay_DataBase.InsertInitiatePayout(payout);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return 0;
        }
        public static string ProcessToDirectFundTransfer(string agentId, string mobile, string RemitterId, string accountno, string bankname, string ifsccode, string clientRefId, string sp_key, string benname, string alert_mobile, string alert_email, string remark, string amount, string otp, string latitude, string longitude, string ipaddress)
        {
            string response = string.Empty;

            try
            {
                string remarks = " Initiate_Payout";

                string reqJson = "{\"token\": \"" + TokenID + "\",\"request\":{\"sp_key\":\"" + sp_key + "\",\"external_ref\":\"" + clientRefId + "\",\"credit_account\":\"" + accountno + "\",\"ifs_code\":\"" + ifsccode + "\","
                    + "\"bene_name\":\"" + benname + "\",\"credit_amount\":\"" + amount + "\",\"latitude\":\"" + latitude + "\",\"longitude\":\"" + longitude + "\",\"endpoint_ip\":\"" + ipaddress + "\",\"alert_mobile\":\"" + alert_mobile + "\",\"alert_email\":\"" + alert_email + "\","
                    + "\"remarks\":\"" + remarks + "\",\"otp_auth\":\"1\",\"otp\":\"" + otp + "\"}}";

                PostUrl = GetInitiatePayoutPostUrl() + "direct";

                InitiatePayout payout = new InitiatePayout();
                payout.AgentId = agentId;
                payout.RemitterMobile = mobile;
                payout.sp_key = sp_key;
                payout.bene_name = benname;
                payout.credit_amount = amount;
                payout.latitude = latitude;
                payout.longitude = longitude;
                payout.endpoint_ip = ipaddress;
                payout.alert_mobile = mobile;
                payout.alert_email = "";
                payout.otp_auth = "1";
                payout.otp = otp;
                payout.remarks = remarks;
                payout.TrackId = clientRefId;
                payout.RemitterId = RemitterId;
                payout.bank_name = bankname;

                int payoutId = InsertInitiatePayout(payout);

                response = Post("POST", PostUrl, reqJson, "direct", agentId, clientRefId);
                //response = "{'statuscode': 'TXN','status': 'Transaction Successful','data': {'external_ref': 'RTFRT5465HFGHG','ipay_id': '1200825120941JNLSB','transfer_value': '1.00','type_pricing': 'CHARGE','commercial_value': '1.1800','value_tds': '0.0000',        'ccf': '0.00','vendor_ccf': '0.00','charged_amt': '2.18','payout': {'credit_refid': '023812469633','account': '04362191032205','ifsc': 'ORBC0100436','name': 'KRISHNA KANT SO JINI'}},'timestamp': '2020-08-25 12:09:42','ipay_uuid': 'F76E0CF70C5563A54A7A','orderid': '1200825120941JNLSB','environment': 'PRODUCTION'}";

                if (!string.IsNullOrEmpty(response))
                {
                    dynamic dyResult = JObject.Parse(response);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                    {
                        dynamic dyData = dyResult.data;
                        dynamic dypayout = dyData.payout;

                        InitiatePayout uppayout = new InitiatePayout();

                        uppayout.InitiatePayoutId = payoutId;
                        uppayout.external_ref = dyData.external_ref;
                        uppayout.ipay_id = dyData.ipay_id;
                        uppayout.transfer_value = dyData.transfer_value;
                        uppayout.type_pricing = dyData.type_pricing;
                        uppayout.commercial_value = dyData.commercial_value;
                        uppayout.value_tds = dyData.value_tds;
                        uppayout.ccf = dyData.ccf;
                        uppayout.vendor_ccf = dyData.vendor_ccf;
                        uppayout.charged_amt = dyData.charged_amt;
                        uppayout.payout_credit_refid = dypayout.credit_refid;
                        uppayout.payout_account = dypayout.account;
                        uppayout.payout_ifsc = dypayout.ifsc;
                        uppayout.payout_name = dypayout.name;
                        uppayout.timestamp = dyResult.timestamp;
                        uppayout.ipay_uuid = dyResult.ipay_uuid;
                        uppayout.orderid = dyResult.orderid;
                        uppayout.environment = dyResult.environment;
                        uppayout.Status = statusMessage;
                        payout.bene_name = dypayout.name;
                        uppayout.Refund = "1";
                        uppayout.RefundId = "";

                        bool isUpdated = InstantPay_DataBase.UpdateInitiatePayout(uppayout);
                    }
                    else
                    {
                        InitiatePayout uppayout = new InitiatePayout();
                        uppayout.Refund = "1";
                        uppayout.RefundId = "RUF" + payoutId + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10).ToUpper(); ;

                        bool isUpdated = InstantPay_DataBase.UpdateInitiatePayout(uppayout);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return response;
        }
        public static string InitiatePayout(string mobile, string clientRefId, string accountno, string transtype, string ifsccode, string latitude, string longitude, string ipaddress, string alert_email, string alert_mobile, string agentId, string RemitterId, string bankName, string paytype)
        {
            string response = string.Empty;

            try
            {
                DataTable dtAgencyRemtBen = InstantPay_DataBase.GetRemBenDetailByAgencyId(agentId, mobile, RemitterId, "");
                if (dtAgencyRemtBen != null && dtAgencyRemtBen.Rows.Count > 0)
                {
                    //string clientRefId = GetTrackId();
                    string transfermode = transtype;

                    if (transtype.ToLower() == "imps") { transtype = "DPN"; }
                    else if (transtype.ToLower() == "neft") { transtype = "BPN"; }
                    else { transtype = "CPN"; }

                    string remarks = "getname";

                    //string reqJson = "{\"token\": \"" + TokenID + "\",\"request\":{\"sp_key\":\"" + transtype + "\",\"external_ref\":\"" + clientRefId + "\",\"credit_account\":\"" + accountno + "\",\"ifs_code\":\"" + ifsccode + "\","
                    //    + "\"bene_name\":\"none\",\"credit_amount\":\"1\",\"latitude\":\"" + latitude + "\",\"longitude\":\"" + longitude + "\",\"endpoint_ip\":\"" + ipaddress + "\",\"alert_mobile\":\"" + alert_mobile + "\",\"alert_email\":\"" + alert_email + "\","
                    //    + "\"remarks\":\"" + remarks + "\"}}";

                    //PostUrl = GetInitiatePayoutPostUrl() + "direct";

                    InitiatePayout payout = new InitiatePayout();
                    payout.AgentId = agentId;
                    payout.RemitterMobile = mobile;
                    payout.sp_key = transtype;
                    payout.bene_name = "";
                    payout.credit_amount = "1";
                    payout.latitude = latitude;
                    payout.longitude = longitude;
                    payout.endpoint_ip = ipaddress;
                    payout.alert_mobile = alert_mobile;
                    payout.alert_email = "";
                    payout.otp_auth = "";
                    payout.otp = "";
                    payout.remarks = remarks;
                    payout.TrackId = clientRefId;
                    payout.RemitterId = RemitterId;
                    payout.bank_name = bankName;
                    payout.PayType = paytype;
                    payout.RemtRemark = "Check Beneficiary Name";
                    payout.LedgerAmount = "3";
                    payout.BenificieryId = "";

                    int payoutId = InsertInitiatePayout(payout);

                    //response = Post("POST", PostUrl, reqJson, "direct", agentId, clientRefId);
                    //response = "{'statuscode': 'TXN','status': 'Transaction Successful','data': {'external_ref': 'RTFRT5465HFGHG','ipay_id': '1200825120941JNLSB','transfer_value': '1.00','type_pricing': 'CHARGE','commercial_value': '1.1800','value_tds': '0.0000',        'ccf': '0.00','vendor_ccf': '0.00','charged_amt': '2.18','payout': {'credit_refid': '023812469633','account': '04362191032205','ifsc': 'ORBC0100436','name': 'KRISHNA KANT SO JINI'}},'timestamp': '2020-08-25 12:09:42','ipay_uuid': 'F76E0CF70C5563A54A7A','orderid': '1200825120941JNLSB','environment': 'PRODUCTION'}";
                    //response = "{'statuscode':'ERR','status':'Failed case for UAT mode','data':{'external_ref':'0E63CA2E49','ipay_id':'1200912152750HECXC','transfer_value':'1.00','type_pricing':'CHARGE','commercial_value':'1.1800','value_tds':'0.0000','ccf':'0.00','vendor_ccf':'0.00','charged_amt':'2.18','payout':{'credit_refid':'00','account':'04362191032205','ifsc':'ORBC0100000','name':'none'}},'timestamp':'2020-09-12 15:27:51','ipay_uuid':'A61677D1E3DBC750A587','orderid':'1200912152750HECXC','environment':'SANDBOX'}";

                    string new_reqJson = "{\"payee\":{\"accountNumber\":\"" + accountno + "\",\"bankIfsc\":\"" + ifsccode + "\"},\"externalRef\":\"" + clientRefId + "\",\"consent\":\"Y\",\"isCached\":\"0\",\"latitude\":\"" + latitude + "\",\"longitude\":\"" + longitude + "\"}";

                    response = InstantPay_NewAPI_Modified.Post("POST", "https://api.instantpay.in/identity/verifyBankAccount", new_reqJson, "direct", agentId, clientRefId);

                    //if (!string.IsNullOrEmpty(response))
                    //{
                    //    dynamic dyResult = JObject.Parse(response);
                    //    string statusCode = dyResult.statuscode;
                    //    string statusMessage = dyResult.status;

                    //    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                    //    {
                    //        dynamic dyData = dyResult.data;
                    //        dynamic dypayout = dyData.payout;

                    //        InitiatePayout uppayout = new InitiatePayout();

                    //        uppayout.InitiatePayoutId = payoutId;
                    //        uppayout.external_ref = dyData.external_ref;
                    //        uppayout.ipay_id = dyData.ipay_id;
                    //        uppayout.transfer_value = dyData.transfer_value;
                    //        uppayout.type_pricing = dyData.type_pricing;
                    //        uppayout.commercial_value = dyData.commercial_value;
                    //        uppayout.value_tds = dyData.value_tds;
                    //        uppayout.ccf = dyData.ccf;
                    //        uppayout.vendor_ccf = dyData.vendor_ccf;
                    //        uppayout.charged_amt = dyData.charged_amt;
                    //        uppayout.payout_credit_refid = dypayout.credit_refid;
                    //        uppayout.payout_account = dypayout.account;
                    //        uppayout.payout_ifsc = dypayout.ifsc;
                    //        uppayout.payout_name = dypayout.name;
                    //        uppayout.timestamp = dyResult.timestamp;
                    //        uppayout.ipay_uuid = dyResult.ipay_uuid;
                    //        uppayout.orderid = dyResult.orderid;
                    //        uppayout.environment = dyResult.environment;
                    //        uppayout.Status = statusMessage;
                    //        uppayout.bene_name = dypayout.name;
                    //        uppayout.Refund = "1";

                    //        bool isUpdated = InstantPay_DataBase.UpdateInitiatePayout(uppayout);
                    //    }
                    //}
                    if (!string.IsNullOrEmpty(response))
                    {
                        dynamic dyResult = JObject.Parse(response);
                        string statusCode = dyResult.statuscode;
                        string statusMessage = dyResult.status;

                        if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                        {
                            string timestamp = dyResult.timestamp;
                            string ipay_uuid = dyResult.ipay_uuid;
                            string orderid = dyResult.orderid;
                            string environment = dyResult.environment;

                            dynamic dyData = dyResult.data;
                            string externalRef = dyData.externalRef;
                            string poolReferenceId = dyData.poolReferenceId;
                            string txnValue = dyData.txnValue;
                            string txnReferenceId = dyData.txnReferenceId;

                            dynamic dypool = dyData.pool;
                            string pool_account = dypool.account;
                            string pool_openingBal = dypool.openingBal;
                            string pool_mode = dypool.mode;
                            string pool_amount = dypool.amount;
                            string pool_closingBal = dypool.closingBal;

                            dynamic dypayer = dyData.payer;
                            string payer_account = dypayer.account;
                            string payer_name = dypayer.name;

                            dynamic dypayee = dyData.payee;
                            string payee_account = dypayee.account;
                            string payee_name = dypayee.name;

                            InitiatePayout uppayout = new InitiatePayout();

                            uppayout.InitiatePayoutId = payoutId;
                            uppayout.external_ref = clientRefId;
                            uppayout.ipay_id = orderid;
                            uppayout.transfer_value = txnValue;
                            uppayout.type_pricing = "CHARGE";
                            uppayout.commercial_value = "0";
                            uppayout.value_tds = "0";
                            uppayout.ccf = "0";
                            uppayout.vendor_ccf = "0";
                            uppayout.charged_amt = pool_amount;
                            uppayout.payout_credit_refid = txnReferenceId;
                            uppayout.payout_account = payee_account;
                            uppayout.payout_ifsc = ifsccode;
                            uppayout.payout_name = payee_name;
                            uppayout.timestamp = timestamp;
                            uppayout.ipay_uuid = ipay_uuid;
                            uppayout.orderid = orderid;
                            uppayout.environment = environment;
                            uppayout.Status = statusMessage;
                            uppayout.bene_name = payee_name;
                            uppayout.Refund = "1";

                            bool isUpdated = InstantPay_DataBase.UpdateInitiatePayout(uppayout);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return response;
        }
        public static string DMTInitiatePayout(string mobile, string clientRefId, string accountno, string transtype, string ifsccode, string latitude, string longitude, string ipaddress, string alert_email, string alert_mobile, string agentId, string RemitterId, string bankName, string paytype)
        {
            string response = string.Empty;

            try
            {
                DataTable dtAgencyRemtBen = InstantPay_DataBase.GetDMTRemBenDetailByAgencyId(agentId, mobile, RemitterId, "");
                if (dtAgencyRemtBen != null && dtAgencyRemtBen.Rows.Count > 0)
                {
                    //string clientRefId = GetTrackId();
                    string transfermode = transtype;

                    if (transtype.ToLower() == "imps") { transtype = "DPN"; }
                    else if (transtype.ToLower() == "neft") { transtype = "BPN"; }
                    else { transtype = "CPN"; }

                    string remarks = "getname";

                    //string reqJson = "{\"token\": \"" + TokenID + "\",\"request\":{\"sp_key\":\"" + transtype + "\",\"external_ref\":\"" + clientRefId + "\",\"credit_account\":\"" + accountno + "\",\"ifs_code\":\"" + ifsccode + "\","
                    //    + "\"bene_name\":\"none\",\"credit_amount\":\"1\",\"latitude\":\"" + latitude + "\",\"longitude\":\"" + longitude + "\",\"endpoint_ip\":\"" + ipaddress + "\",\"alert_mobile\":\"" + alert_mobile + "\",\"alert_email\":\"" + alert_email + "\","
                    //    + "\"remarks\":\"" + remarks + "\"}}";

                    //PostUrl = GetInitiatePayoutPostUrl() + "direct";

                    InitiatePayout payout = new InitiatePayout();
                    payout.AgentId = agentId;
                    payout.RemitterMobile = mobile;
                    payout.sp_key = transtype;
                    payout.bene_name = "";
                    payout.credit_amount = "1";
                    payout.latitude = latitude;
                    payout.longitude = longitude;
                    payout.endpoint_ip = ipaddress;
                    payout.alert_mobile = alert_mobile;
                    payout.alert_email = "";
                    payout.otp_auth = "";
                    payout.otp = "";
                    payout.remarks = remarks;
                    payout.TrackId = clientRefId;
                    payout.RemitterId = RemitterId;
                    payout.bank_name = bankName;
                    payout.PayType = paytype;
                    payout.RemtRemark = "Check Beneficiary Name";
                    payout.LedgerAmount = "3";
                    payout.BenificieryId = "";

                    int payoutId = InsertInitiatePayout(payout);

                    //string new_reqJson = "{\"payee\":{\"accountNumber\":\"" + accountno + "\",\"bankIfsc\":\"" + ifsccode + "\"},\"externalRef\":\"" + clientRefId + "\",\"consent\":\"Y\",\"isCached\":\"0\",\"latitude\":\"" + latitude + "\",\"longitude\":\"" + longitude + "\"}";
                    string new_reqJson = "{\"payee\":{\"accountNumber\":\"" + accountno + "\",\"bankIfsc\":\"" + ifsccode + "\"},\"externalRef\":\"" + clientRefId + "\",\"consent\":\"Y\",\"isCached\":\"0\",\"latitude\":\"" + latitude + "\",\"longitude\":\"" + longitude + "\"}";
                    response = InstantPay_NewAPI_Modified.Post("POST", "https://api.instantpay.in/identity/verifyBankAccount", new_reqJson, "direct", agentId, clientRefId);
                    //response = "{'statuscode': 'TXN','status': 'Transaction Successful','data': {'external_ref': 'RTFRT5465HFGHG','ipay_id': '1200825120941JNLSB','transfer_value': '1.00','type_pricing': 'CHARGE','commercial_value': '1.1800','value_tds': '0.0000',        'ccf': '0.00','vendor_ccf': '0.00','charged_amt': '2.18','payout': {'credit_refid': '023812469633','account': '04362191032205','ifsc': 'ORBC0100436','name': 'KRISHNA KANT SO JINI'}},'timestamp': '2020-08-25 12:09:42','ipay_uuid': 'F76E0CF70C5563A54A7A','orderid': '1200825120941JNLSB','environment': 'PRODUCTION'}";
                    //response = "{'statuscode':'ERR','status':'Failed case for UAT mode','data':{'external_ref':'0E63CA2E49','ipay_id':'1200912152750HECXC','transfer_value':'1.00','type_pricing':'CHARGE','commercial_value':'1.1800','value_tds':'0.0000','ccf':'0.00','vendor_ccf':'0.00','charged_amt':'2.18','payout':{'credit_refid':'00','account':'04362191032205','ifsc':'ORBC0100000','name':'none'}},'timestamp':'2020-09-12 15:27:51','ipay_uuid':'A61677D1E3DBC750A587','orderid':'1200912152750HECXC','environment':'SANDBOX'}";

                    //response = "{'statuscode':'TXN','actcode':null,'status':'Transaction Successful','data':{'externalRef':'1697E40784','poolReferenceId':'1220602232139UWAPU','txnValue':'1.00','txnReferenceId':'215323620066','pool':{'account':'9760443317','openingBal':'56829.82','mode':'DR','amount':'2.18','closingBal':'56827.64'},'payer':{'account':'9760443317','name':'GANDHI SERVICES'},'payee':{'account':'1450100100005960','name':'SHIVANI ROY'},'isCached':false},'timestamp':'2022-06-02 23:21:40','ipay_uuid':'h0689672aa6f-b5bd-479d-afad-96628cbde733','orderid':'1220602232139UWAPU','environment':'LIVE'}";

                    if (!string.IsNullOrEmpty(response))
                    {
                        dynamic dyResult = JObject.Parse(response);
                        string statusCode = dyResult.statuscode;
                        string statusMessage = dyResult.status;

                        if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                        {
                            string timestamp = dyResult.timestamp;
                            string ipay_uuid = dyResult.ipay_uuid;
                            string orderid = dyResult.orderid;
                            string environment = dyResult.environment;

                            dynamic dyData = dyResult.data;
                            string externalRef = dyData.externalRef;
                            string poolReferenceId = dyData.poolReferenceId;
                            string txnValue = dyData.txnValue;
                            string txnReferenceId = dyData.txnReferenceId;

                            dynamic dypool = dyData.pool;
                            string pool_account = dypool.account;
                            string pool_openingBal = dypool.openingBal;
                            string pool_mode = dypool.mode;
                            string pool_amount = dypool.amount;
                            string pool_closingBal = dypool.closingBal;

                            dynamic dypayer = dyData.payer;
                            string payer_account = dypayer.account;
                            string payer_name = dypayer.name;

                            dynamic dypayee = dyData.payee;
                            string payee_account = dypayee.account;
                            string payee_name = dypayee.name;

                            InitiatePayout uppayout = new InitiatePayout();

                            uppayout.InitiatePayoutId = payoutId;
                            uppayout.external_ref = clientRefId;
                            uppayout.ipay_id = orderid;
                            uppayout.transfer_value = txnValue;
                            uppayout.type_pricing = "CHARGE";
                            uppayout.commercial_value = "0";
                            uppayout.value_tds = "0";
                            uppayout.ccf = "0";
                            uppayout.vendor_ccf = "0";
                            uppayout.charged_amt = pool_amount;
                            uppayout.payout_credit_refid = txnReferenceId;
                            uppayout.payout_account = payee_account;
                            uppayout.payout_ifsc = ifsccode;
                            uppayout.payout_name = payee_name;
                            uppayout.timestamp = timestamp;
                            uppayout.ipay_uuid = ipay_uuid;
                            uppayout.orderid = orderid;
                            uppayout.environment = environment;
                            uppayout.Status = statusMessage;
                            uppayout.bene_name = payee_name;
                            uppayout.Refund = "1";

                            bool isUpdated = InstantPay_DataBase.UpdateInitiatePayout(uppayout);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return response;
        }
        public static string InitiatePayoutDirectTransfer(string transtype, string account, string ifsccode, string benname, string amount, string remark, string alertmobile, string alertemail, string latitude, string longitude, string ipaddress, string agentId, string mobile, string remitterId, string bankName)
        {
            string response = string.Empty;

            try
            {
                string clientRefId = GetTrackId();

                string remarks = "Payout_Transfer";

                string reqJson = "{\"token\": \"" + TokenID + "\",\"request\":{\"sp_key\":\"" + transtype + "\",\"external_ref\":\"" + clientRefId + "\",\"credit_account\":\"" + account + "\",\"ifs_code\":\"" + ifsccode + "\","
                    + "\"bene_name\":\"" + benname + "\",\"credit_amount\":\"" + amount + "\",\"latitude\":\"" + latitude + "\",\"longitude\":\"" + longitude + "\",\"endpoint_ip\":\"" + ipaddress + "\",\"alert_mobile\":\"" + alertmobile + "\",\"alert_email\":\"" + alertemail + "\","
                    + "\"remarks\":\"" + remarks + "\"}}";

                PostUrl = GetInitiatePayoutPostUrl() + "direct";

                InitiatePayout payout = new InitiatePayout();
                payout.AgentId = agentId;
                payout.RemitterMobile = mobile;
                payout.sp_key = transtype;
                payout.bene_name = "none";
                payout.credit_amount = "1";
                payout.latitude = latitude;
                payout.longitude = longitude;
                payout.endpoint_ip = ipaddress;
                payout.alert_mobile = mobile;
                payout.alert_email = "";
                payout.otp_auth = "";
                payout.otp = "";
                payout.remarks = remarks;
                payout.TrackId = clientRefId;
                payout.RemitterId = remitterId;
                payout.bank_name = bankName;

                int payoutId = InsertInitiatePayout(payout);

                response = Post("POST", PostUrl, reqJson, "direct", agentId, clientRefId);
                //response = "{'statuscode': 'TXN','status': 'Transaction Successful','data': {'external_ref': 'RTFRT5465HFGHG','ipay_id': '1200825120941JNLSB','transfer_value': '1.00','type_pricing': 'CHARGE','commercial_value': '1.1800','value_tds': '0.0000',        'ccf': '0.00','vendor_ccf': '0.00','charged_amt': '2.18','payout': {'credit_refid': '023812469633','account': '04362191032205','ifsc': 'ORBC0100436','name': 'KRISHNA KANT SO JINI'}},'timestamp': '2020-08-25 12:09:42','ipay_uuid': 'F76E0CF70C5563A54A7A','orderid': '1200825120941JNLSB','environment': 'PRODUCTION'}";

                if (!string.IsNullOrEmpty(response))
                {
                    dynamic dyResult = JObject.Parse(response);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                    {
                        dynamic dyData = dyResult.data;
                        dynamic dypayout = dyData.payout;

                        InitiatePayout uppayout = new InitiatePayout();

                        uppayout.InitiatePayoutId = payoutId;
                        uppayout.external_ref = dyData.external_ref;
                        uppayout.ipay_id = dyData.ipay_id;
                        uppayout.transfer_value = dyData.transfer_value;
                        uppayout.type_pricing = dyData.type_pricing;
                        uppayout.commercial_value = dyData.commercial_value;
                        uppayout.value_tds = dyData.value_tds;
                        uppayout.ccf = dyData.ccf;
                        uppayout.vendor_ccf = dyData.vendor_ccf;
                        uppayout.charged_amt = dyData.charged_amt;
                        uppayout.payout_credit_refid = dypayout.credit_refid;
                        uppayout.payout_account = dypayout.account;
                        uppayout.payout_ifsc = dypayout.ifsc;
                        uppayout.payout_name = dypayout.name;
                        uppayout.timestamp = dyResult.timestamp;
                        uppayout.ipay_uuid = dyResult.ipay_uuid;
                        uppayout.orderid = dyResult.orderid;
                        uppayout.environment = dyResult.environment;
                        uppayout.Status = statusMessage;
                        uppayout.Refund = "1";

                        bool isUpdated = InstantPay_DataBase.UpdateInitiatePayout(uppayout);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return response;
        }

        //==================================New Section=========================================================       
        private static bool UpdateRemitterRegDetail(int remtID, string mobile, string agentId, string resultRespo, string remitterId)
        {
            try
            {
                if (!string.IsNullOrEmpty(resultRespo))
                {
                    dynamic dyResult = JObject.Parse(resultRespo);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "OTP".ToLower() && statusMessage.ToLower().Contains("OTP sent to remitter mobile number XXXXXXX".ToLower())) //&& statusMessage.ToLower() == "otp sent successfully")
                    {
                        dynamic dyData = dyResult.data;
                        //dynamic remitter = dyData.remitter;
                        string otpReference = dyData.otpReference;
                        //string remitterId = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10).ToUpper(); //remitter.id;
                        //string verifiedStatus = remitter.is_verified;

                        return InstantPay_DataBase.UpdateRemitterOTPDetail(remtID, mobile, agentId, remitterId, otpReference);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static string CheckRemitterValidate(string remitterid, string mobile, string otp, string agentId)
        {
            string result = string.Empty;

            string reqJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"remitterid\": \"" + remitterid + "\",\"mobile\": \"" + mobile + "\",\"otp\": \"" + otp + "\",\"outletid\":" + OutletId + "}}";
            PostUrl = GetPostUrl() + "remitter_validate";

            result = Post("POST", PostUrl, reqJson, "Remitter_Registration_Validate", agentId, GetTrackId());
            if (!string.IsNullOrEmpty(result))
            {
                dynamic dyOtpResult = JObject.Parse(result);
                string otpstatusCode = dyOtpResult.statuscode;
                string otpstatusMessage = dyOtpResult.status;

                if (otpstatusCode.ToLower().Trim() == "txn" && otpstatusMessage.ToLower().Trim() == "otp sent successfully")
                {
                    string remitterJson = "{\"token\": \"" + TokenID + "\",\"request\": {\"mobile\": \"" + mobile + "\",\"outletid\":" + OutletId + "}}";
                    PostUrl = GetPostUrl() + "remitter_details";

                    string remitterResult = Post("POST", PostUrl, remitterJson, "GetRemitterDetail", agentId, GetTrackId());
                    if (!string.IsNullOrEmpty(remitterResult))
                    {
                        dynamic dyResult = JObject.Parse(remitterResult);
                        string statusCode = dyResult.statuscode;
                        string statusMessage = dyResult.status;

                        if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                        {
                            dynamic dyData = dyResult.data;
                            dynamic remitter = dyData.remitter;
                            dynamic beneficiary = dyData.beneficiary;

                            string remt_id = remitter.id;
                            string name = remitter.name;
                            string respo_mobile = remitter.mobile;
                            string address = remitter.address;
                            string respo_pincode = remitter.pincode;
                            string city = remitter.city;
                            string state = remitter.state;
                            string kycstatus = remitter.kycstatus;
                            string consumedlimit = remitter.consumedlimit;
                            string remaininglimit = remitter.remaininglimit;
                            string kycdocs = remitter.kycdocs;
                            string is_verified = remitter.is_verified;
                            string perm_txn_limit = remitter.perm_txn_limit;

                            bool isSuccess = InstantPay_DataBase.Update_T_InstantPayRemitterDetailResponse(remt_id, name, respo_mobile, address, respo_pincode, city, state, kycstatus, consumedlimit, remaininglimit, kycdocs, is_verified, perm_txn_limit, agentId);
                        }
                    }
                }
            }

            return result;
        }
        public static bool InsertAlreadyRegRemitterDetails(string response, string agentId, string remitterId)
        {
            try
            {
                if (!string.IsNullOrEmpty(response))
                {
                    dynamic dyResult = JObject.Parse(response);
                    string statusCode = dyResult.statuscode;
                    string statusMessage = dyResult.status;

                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "success")
                    {
                        dynamic dyData = dyResult.data;
                        //dynamic remitter = dyData.remitter;

                        //dynamic remitter_limit = dyData.remitter_limit[0];
                        //dynamic remitter_limit_limit = remitter_limit.limit;

                        string RegMobile = dyData.mobile;
                        string FirstName = dyData.firstName;
                        string LastName = dyData.lastName;
                        string PinCode = dyData.pincode;
                        string CurrentAddress = dyData.address;
                        string AgentId = agentId;
                        string id = remitterId; //remitter.id;//Guid.NewGuid().ToString().Replace("-", "").Substring(0, 15).ToUpper()
                        string name = FirstName + " " + LastName;
                        string mobile = RegMobile;
                        string address = CurrentAddress;
                        string pincode_res = PinCode;
                        string city = dyData.city;
                        string state = dyData.state;
                        string kycstatus = "0";
                        string consumedlimit = dyData.limitConsumed;
                        string remaininglimit = dyData.limitAvailable;
                        string kycdocs = "REQUIRED";
                        string is_verified = "1";
                        string perm_txn_limit = "5000";
                        string CreditLimit = "25000";

                        string query = "insert into T_InstantPayRemitterDetail "
                            + "(RegMobile,FirstName,LastName,PinCode,CurrentAddress,AgentId,id,name,mobile,address,pincode_res,city,state,kycstatus,consumedlimit,remaininglimit,kycdocs,is_verified,perm_txn_limit,CreditLimit) values "
                            + "('" + RegMobile + "','" + FirstName + "','" + LastName + "','" + PinCode + "','" + CurrentAddress + "','" + AgentId + "','" + id + "','" + name + "',"
                            + "'" + mobile + "','" + address + "','" + pincode_res + "','" + city + "','" + state + "','" + kycstatus + "','" + consumedlimit + "','" + remaininglimit + "',"
                            + "'" + kycdocs + "','" + is_verified + "','" + perm_txn_limit + "','" + CreditLimit + "')";

                        return InstantPay_DataBase.Insert_AlreadyRegisteredRemitterDetails(query);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        //===================================Payout Section=============================================
        public static DataTable GetPayoutTransactionHistory(string remitterId, string agentId, string fromdate, string todate, string trackid, string filstatus)
        {
            return InstantPay_DataBase.GetPayoutTransactionHistory(remitterId, agentId, fromdate, todate, trackid, filstatus);
        }

        #region [Ledger Section Credit and Debit]
        public static List<string> DMTLedgerDebitCredit(double Amount, string AgentId, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType, string mobile, string remitterId, string benid)
        {
            return LedgerService.DMTLedgerDebitCredit(Amount, AgentId, AgencyName, AccountID, IPAddress, Debit, Credit, BookingType, Remark, Uploadtype, shortRemark, Narration, TransType, mobile, remitterId, benid);
        }
        #endregion

        //==============================DMT DIRECT============================================================   
        public static bool UpdateRefundStatusInstantPayInitiatePayout(string trackid)
        {
            return InstantPay_DataBase.UpdateRefundStatusInstantPayInitiatePayout(trackid);
        }
        public static bool UpdateRefundStatusInstantPayFundTransfer(string trackid, string action)
        {
            return InstantPay_DataBase.UpdateRefundStatusInstantPayFundTransfer(trackid, action);
        }
        public static bool Insert_T_InstantPayFailedRefund(string AgentId, string TrackId, string LedTrackId, string RefundAmount, string IpAddress)
        {
            return InstantPay_DataBase.Insert_T_InstantPayFailedRefund(AgentId, TrackId, LedTrackId, RefundAmount, IpAddress);
        }

        //======================Get Common Trans History By Track id=====================================
        public static DataTable GetCommonFundTransHistory(string trackid, string agentId, ref DataTable RemitterDel, ref DataTable BenDetail)
        {
            return InstantPay_DataBase.GetCommonFundTransHistory(trackid, agentId, ref RemitterDel, ref BenDetail);
        }
        public static DataTable GetRecordFromTable(string query)
        {
            return InstantPay_DataBase.GetRecordFromTable(query);
        }
    }
}
