﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static InstantPayServiceLib.InstantPay_Model;

namespace InstantPayServiceLib
{
    public static class InstantPay_PayoutService
    {
        private static string TokenID = "ffecc13de31e6be9fe916654e0bcf61f";//"  3470f9330a3cc01d71f31a7050ad1834"
        private static string PostUrl { get; set; }
        // private static int OutletId = 1;

        #region [DMT-DIRECT]
        public static DataTable GetDMTDirectRemitter(string mobile, string agentid, string remitterid = null)
        {
            return InstantPay_PayoutDatabase.GetDMTDirectRemitter(mobile, agentid, remitterid);
        }

        public static bool InsertDMTDirectRemitter(string mobile, string fistname, string lastname, string pincode, string address, string agentid, string dynremitterid, ref string otp)
        {
            return InstantPay_PayoutDatabase.InsertDMTDirectRemitter(mobile, fistname, lastname, pincode, address, agentid, dynremitterid, ref otp);
        }

        public static bool ResendDMTDOtp(string agentid, string mobile, string remitterid)
        {
            return InstantPay_PayoutDatabase.ResendDMTDOtp(agentid, mobile, remitterid);
        }

        public static bool VerifyingDMTDRemitter(string agentid, string mobile, string remitterid)
        {
            return InstantPay_PayoutDatabase.VerifyingDMTDRemitter(agentid, mobile, remitterid);
        }
        #endregion

        private static string GetInitiatePayoutPostUrl()
        {
            return "https://www.instantpay.in/ws/payouts/";
        }

        public static bool CheckPayoutKycComplete(string agentid, string mobile, string remitterid, ref string regId, ref bool showkyc, ref string kycStatus)
        {
            return InstantPay_PayoutDatabase.CheckPayoutKycComplete(agentid, mobile, remitterid, ref regId, ref showkyc, ref kycStatus);
        }

        public static bool GenrateOTPForTransfer(string regId, string agentid, string mobile, string remitterid, ref string otp)
        {
            return InstantPay_PayoutDatabase.GenrateOTPForTransfer(regId, agentid, mobile, remitterid, ref otp);
        }
        public static bool RemoveOTPForTransfer(string regId, string agentid, string mobile, string remitterid)
        {
            return InstantPay_PayoutDatabase.RemoveOTPForTransfer(regId, agentid, mobile, remitterid);
        }

        public static bool GenrateOTPForBenDelete(string agentid, string mobile, string remitterid, string benid, ref string otp)
        {
            return InstantPay_PayoutDatabase.GenrateOTPForBenDelete(agentid, mobile, remitterid, benid, ref otp);
        }

        public static DataTable GetPayoutRemitterDetails(string regId, string agentid, string mobile, string remitterid, ref DataTable BenDetail, string benid = null)
        {
            return InstantPay_PayoutDatabase.GetPayoutRemitterDetails(regId, agentid, mobile, remitterid, ref BenDetail, benid);
        }

        public static DataTable GetPayoutRemitterBenDetails(string regId, string agentid, string mobile, string remitterid, string benid = null)
        {
            return InstantPay_PayoutDatabase.GetPayoutRemitterBenDetails(regId, agentid, mobile, remitterid, benid);
        }

        public static bool UpdateRemitterKYCDetails(string regId, string agentId, string mobile, string remitterId, string fn, string ln, string add, string gen, string dob, string proof, string pno, string frontImg, string backImg)
        {
            return InstantPay_PayoutDatabase.UpdateRemitterKYCDetails(regId, agentId, mobile, remitterId, fn, ln, add, gen, dob, proof, pno, frontImg, backImg);
        }

        public static bool PayoutUpdateLocalAddress(string regid, string remitterId, string agentId, string mobile, string address)
        {
            return InstantPay_PayoutDatabase.PayoutUpdateLocalAddress(regid, remitterId, agentId, mobile, address);
        }

        public static bool PayoutBeneficiaryRegistration(string regid, string remitterid, string name, string mobile, string account, string bankname, string ifsc, string agentId)
        {
            return InstantPay_PayoutDatabase.PayoutBeneficiaryRegistration(regid, remitterid, name, mobile, account, bankname, ifsc, agentId);
        }

        public static string PaayoutDirectMoneyTransfer(string RegId, string agentId, string mobile, string remitterId, string benid, string transtype, string clientRefId, string amount, string led_Amt, string ipadd, string remtRemark, string alert_email, string alert_mobile, string remark)
        {
            string result = string.Empty;

            try
            {
                //DataTable BenDetail = InstantPay_PayoutDatabase.GetPayoutRemitterBenDetails(RegId, agentId, mobile, remitterId, benid);
                DataTable BenDetail = LedgerService.GetRemBenDetailByAgencyId(agentId, mobile, remitterId, benid);

                if (BenDetail != null && BenDetail.Rows.Count > 0)
                {
                    string accountno = BenDetail.Rows[0]["AccountNo"].ToString().Trim();
                    string ifsccode = BenDetail.Rows[0]["IFSCCode"].ToString().Trim();
                    string bankname = BenDetail.Rows[0]["BankName"].ToString().Trim();
                    string bene_name = BenDetail.Rows[0]["BenName"].ToString().Replace("S/O", "").Replace("s/o", "").Trim();

                    string latitude = "28.690430";
                    string longitude = "77.101662";
                    string ipaddress = ipadd;


                    InitiatePayout payout = new InitiatePayout();
                    payout.AgentId = agentId;
                    payout.RemitterMobile = mobile;
                    payout.sp_key = transtype;
                    payout.bene_name = bene_name;
                    payout.credit_amount = amount;
                    payout.latitude = latitude;
                    payout.longitude = longitude;
                    payout.endpoint_ip = ipaddress;
                    payout.alert_mobile = alert_mobile;
                    payout.alert_email = alert_email;
                    payout.otp_auth = "";
                    payout.otp = "";
                    payout.remarks = "FundTransfer";
                    payout.TrackId = clientRefId;
                    payout.RemitterId = remitterId;
                    payout.bank_name = bankname;
                    payout.PayType = "payout_direct";
                    payout.RemtRemark = remtRemark;
                    payout.LedgerAmount = led_Amt;
                    payout.BenificieryId = benid;

                    int payoutId = InsertInitiatePayout(payout);

                    string reqJson = "";
                    DataTable DirectPayoutParameters = InstantPay_DataBase.Get_InstantPayDirectPayoutParameters("direct");
                    if (DirectPayoutParameters != null && DirectPayoutParameters.Rows.Count > 0)
                    {
                        string IsApply = DirectPayoutParameters.Rows[0]["IsApply"].ToString();
                        if (Convert.ToBoolean(IsApply) == true)
                        {
                            string AuthCode = DirectPayoutParameters.Rows[0]["AuthCode"].ToString();
                            string ClientId = DirectPayoutParameters.Rows[0]["ClientId"].ToString();
                            string ClientSecret = DirectPayoutParameters.Rows[0]["ClientSecret"].ToString();
                            string EndpointIp = DirectPayoutParameters.Rows[0]["EndpointIp"].ToString();

                            string bankId = DirectPayoutParameters.Rows[0]["PayerBankId"].ToString();
                            string bankProfileId = DirectPayoutParameters.Rows[0]["PayerBankProfileId"].ToString();
                            string accountNumber = DirectPayoutParameters.Rows[0]["PayerAccountNumber"].ToString();
                            string new_latitude = DirectPayoutParameters.Rows[0]["Latitude"].ToString();
                            string new_longitude = DirectPayoutParameters.Rows[0]["Longitude"].ToString();
                            string purpose = DirectPayoutParameters.Rows[0]["TransferPurpose"].ToString();
                            PostUrl = DirectPayoutParameters.Rows[0]["PostUrl"].ToString();

                            //string alertEmail= BenDetail.Rows[0]["Agent_Email"].ToString();
                            string alertEmail = DirectPayoutParameters.Rows[0]["AlertEmail"].ToString();

                            string transferMode = (transtype == "DPN" ? "IMPS" : (transtype == "BPN" ? "NEFT" : "RTGS"));

                            reqJson = "{\"payer\":{\"bankId\":\"" + bankId + "\",\"bankProfileId\":\"" + bankProfileId + "\",\"accountNumber\":\"" + accountNumber + "\"},\"payee\":{\"name\":\"" + bene_name + "\",\"accountNumber\":\"" + accountno + "\",\"bankIfsc\":\"" + ifsccode + "\"},\"transferMode\":\"" + transferMode + "\",\"transferAmount\":\"" + amount + "\",\"externalRef\":\"" + clientRefId + "\",\"latitude\":\"" + new_latitude + "\",\"longitude\":\"" + new_longitude + "\",\"remarks\":\"" + remark + "\",\"alertEmail\":\"" + alertEmail + "\",\"purpose\":\"" + purpose + "\"}";

                            if (!string.IsNullOrEmpty(reqJson))
                            {
                                result = Post("POST", PostUrl, reqJson, "direct", agentId, clientRefId, AuthCode, ClientId, ClientSecret, EndpointIp);
                            }

                            if (!string.IsNullOrEmpty(result))
                            {
                                dynamic dyResult = JObject.Parse(result);
                                string statusCode = dyResult.statuscode;
                                string actcode = dyResult.actcode;
                                string statusMessage = dyResult.status;

                                if ((statusCode.ToLower() == "txn" && statusMessage.ToLower().Contains("transaction successful")) || (statusCode.ToLower() == "err" && statusMessage.ToLower().Contains("transaction failed")))
                                {
                                    //dynamic dypayout = dyData.payout;
                                    string timestamp = dyResult.timestamp;
                                    string ipay_uuid = dyResult.ipay_uuid;
                                    string orderid = dyResult.orderid;
                                    string environment = dyResult.environment;

                                    dynamic dyData = dyResult.data;
                                    string externalRef = dyData.externalRef;
                                    string poolReferenceId = dyData.poolReferenceId;
                                    string txnValue = dyData.txnValue;
                                    string txnReferenceId = dyData.txnReferenceId;

                                    dynamic dyDataPool = dyResult.data.pool;
                                    string pool_account = dyDataPool.account;
                                    string pool_openingBal = dyDataPool.openingBal;
                                    string pool_mode = dyDataPool.mode;
                                    string pool_amount = dyDataPool.amount;
                                    string pool_closingBal = dyDataPool.closingBal;

                                    dynamic dyDataPayer = dyResult.data.payer;
                                    string payer_account = dyDataPayer.account;
                                    string payer_name = dyDataPayer.name;

                                    dynamic dyDataPayee = dyResult.data.payee;
                                    string payee_account = dyDataPayee.account;
                                    string payee_name = dyDataPayee.name;

                                    InitiatePayout uppayout = new InitiatePayout();
                                    uppayout.InitiatePayoutId = payoutId;
                                    uppayout.external_ref = externalRef;
                                    uppayout.ipay_id = orderid;
                                    uppayout.transfer_value = txnValue;
                                    uppayout.type_pricing = "";
                                    uppayout.commercial_value = "";
                                    uppayout.value_tds = "";
                                    uppayout.ccf = "";
                                    uppayout.vendor_ccf = "";
                                    uppayout.charged_amt = pool_amount;
                                    uppayout.payout_credit_refid = "";
                                    uppayout.payout_account = payee_account;
                                    uppayout.payout_ifsc = ifsccode;
                                    uppayout.payout_name = payee_name;
                                    uppayout.timestamp = timestamp;
                                    uppayout.ipay_uuid = ipay_uuid;
                                    uppayout.orderid = orderid;
                                    uppayout.environment = environment;
                                    uppayout.Status = statusMessage;

                                    uppayout.poolReferenceId = poolReferenceId;
                                    uppayout.txnReferenceId = txnReferenceId;
                                    uppayout.poolaccount = pool_account;
                                    uppayout.poolopeningBal = pool_openingBal;
                                    uppayout.poolmode = pool_mode;
                                    uppayout.poolclosingBal = pool_closingBal;
                                    uppayout.poolamount = pool_amount;

                                    if (statusCode.ToLower() == "txn" && statusMessage.ToLower().Contains("transaction successful"))
                                    {
                                        uppayout.Refund = "1";
                                    }

                                    if (InstantPay_DataBase.UpdateInitiatePayout(uppayout, "new"))
                                    {
                                        return result;
                                    }
                                }
                            }
                        }
                        else
                        {
                            reqJson = "{\"token\": \"" + TokenID + "\",\"request\":{\"sp_key\":\"" + transtype + "\",\"external_ref\":\"" + clientRefId + "\",\"credit_account\":\"" + accountno + "\",\"ifs_code\":\"" + ifsccode + "\"," + "\"bene_name\":\"" + bene_name + "\",\"credit_amount\":\"" + amount + "\",\"latitude\":\"" + latitude + "\",\"longitude\":\"" + longitude + "\",\"endpoint_ip\":\"" + ipaddress + "\",\"alert_mobile\":\"" + alert_mobile + "\",\"alert_email\":\"" + alert_email + "\"," + "\"remarks\":\"FundTransfer\"}}";
                            PostUrl = GetInitiatePayoutPostUrl() + "direct";
                            if (!string.IsNullOrEmpty(reqJson))
                            {
                                result = Post("POST", PostUrl, reqJson, "direct", agentId, clientRefId, "", "", "", "");
                            }

                            if (!string.IsNullOrEmpty(result))
                            {
                                dynamic dyResult = JObject.Parse(result);
                                string statusCode = dyResult.statuscode;
                                string statusMessage = dyResult.status;

                                if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                                {
                                    dynamic dyData = dyResult.data;
                                    dynamic dypayout = dyData.payout;

                                    InitiatePayout uppayout = new InitiatePayout();

                                    uppayout.InitiatePayoutId = payoutId;
                                    uppayout.external_ref = dyData.external_ref;
                                    uppayout.ipay_id = dyData.ipay_id;
                                    uppayout.transfer_value = dyData.transfer_value;
                                    uppayout.type_pricing = dyData.type_pricing;
                                    uppayout.commercial_value = dyData.commercial_value;
                                    uppayout.value_tds = dyData.value_tds;
                                    uppayout.ccf = dyData.ccf;
                                    uppayout.vendor_ccf = dyData.vendor_ccf;
                                    uppayout.charged_amt = dyData.charged_amt;
                                    uppayout.payout_credit_refid = dypayout.credit_refid;
                                    uppayout.payout_account = dypayout.account;
                                    uppayout.payout_ifsc = dypayout.ifsc;
                                    uppayout.payout_name = dypayout.name;
                                    uppayout.timestamp = dyResult.timestamp;
                                    uppayout.ipay_uuid = dyResult.ipay_uuid;
                                    uppayout.orderid = dyResult.orderid;
                                    uppayout.environment = dyResult.environment;
                                    uppayout.Status = statusMessage;
                                    uppayout.Refund = "1";

                                    if (InstantPay_DataBase.UpdateInitiatePayout(uppayout, "old"))
                                    {
                                        return result;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }

        public static string CheckTransStatus(string transactionDate, string trackId, string agentId, string clientRefId, string rmtMObile, string RemitterId, string IpAddress)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(transactionDate.Trim()) && !string.IsNullOrEmpty(trackId.Trim()) && !string.IsNullOrEmpty(agentId.Trim()))
                {
                    DataTable DirectPayoutParameters = InstantPay_DataBase.Get_InstantPayDirectPayoutParameters("checkstatus");
                    if (DirectPayoutParameters != null && DirectPayoutParameters.Rows.Count > 0)
                    {
                        string IsApply = DirectPayoutParameters.Rows[0]["IsApply"].ToString();
                        if (Convert.ToBoolean(IsApply) == true)
                        {
                            string AuthCode = DirectPayoutParameters.Rows[0]["AuthCode"].ToString();
                            string ClientId = DirectPayoutParameters.Rows[0]["ClientId"].ToString();
                            string ClientSecret = DirectPayoutParameters.Rows[0]["ClientSecret"].ToString();
                            string EndpointIp = DirectPayoutParameters.Rows[0]["EndpointIp"].ToString();
                            string PostUrl = DirectPayoutParameters.Rows[0]["PostUrl"].ToString();
                            bool UpdateRecord = Convert.ToBoolean(DirectPayoutParameters.Rows[0]["UpdateRecord"].ToString());

                            string reqJson = "{\"transactionDate\" : \"" + transactionDate + "\",\"externalRef\" : \"" + trackId + "\"}";
                            if (!string.IsNullOrEmpty(reqJson))
                            {
                                result = Post("POST", PostUrl, reqJson, "checkstatus", agentId, clientRefId, AuthCode, ClientId, ClientSecret, EndpointIp);

                                if (UpdateRecord)
                                {
                                    if (!string.IsNullOrEmpty(result))
                                    {
                                        dynamic dyResult = JObject.Parse(result);
                                        dynamic dyData = dyResult.data;
                                        string transactionStatusCode = dyData.transactionStatusCode;
                                        string transactionStatus = dyData.transactionStatus;

                                        dynamic dyorder = dyData.order;
                                        dynamic dypool = dyData.pool;

                                        dynamic dypool_transaction = dypool.transaction;
                                        dynamic dypool_reversal = dypool.reversal;

                                        string reversal_refereceId = dypool_reversal.refereceId;
                                        string reversal_openingBalance = dypool_reversal.openingBalance;
                                        string reversal_paymentAmount = dypool_reversal.paymentAmount;
                                        string reversal_mode = dypool_reversal.mode;
                                        string reversal_closingBalance = dypool_reversal.closingBalance;

                                        if (!string.IsNullOrEmpty(reversal_refereceId) && reversal_mode == "CR")
                                        {
                                            DataTable dtChecckStatus = InstantPay_DataBase.SP_CheckInstantPayStatus(agentId, trackId, rmtMObile, "checkstatus");
                                            if (dtChecckStatus != null && dtChecckStatus.Rows.Count > 0)
                                            {
                                                decimal debitAMt = Convert.ToDecimal(dtChecckStatus.Rows[0]["Debit"].ToString());
                                                if (debitAMt > 0)
                                                {
                                                    DataTable dtAgency = InstantPay_DataBase.SP_CheckInstantPayStatus(agentId, trackId, rmtMObile, "getagency");
                                                    if (dtAgency != null && dtAgency.Rows.Count > 0)
                                                    {
                                                        string agencyName = dtAgency.Rows[0]["Agency_Name"].ToString();

                                                        string Narration = "Dmt Direct Reund " + debitAMt + " To " + agentId;
                                                        //List<string> LdRefund = LedgerService.SimpleLedgerDebitCreditUtility(Convert.ToDouble(debitAMt), agentId, agencyName, trackId, agentId, IpAddress, 0, Convert.ToDouble(debitAMt), "Credit", "Refund", "CR", "Payout Direct Money Transfer", Narration, "CREDIT NOTE");
                                                        //if (LdRefund.Count > 0)
                                                        //{
                                                        //    InstantPay_DataBase.Update_SP_CheckInstantPayStatus(agentId, LdRefund[0], rmtMObile);
                                                        //}
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public static string Admin_CheckTransStatus(string transactionDate, string trackId)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(transactionDate.Trim()) && !string.IsNullOrEmpty(trackId))
                {
                    DataSet dsDetail = InstantPay_DataBase.SP_AdminCheckInstantPayStatus(trackId, transactionDate);
                    if (dsDetail.Tables.Count > 0)
                    {
                        string agentId = string.Empty; string agencyName = string.Empty; string remitterMobNo = string.Empty; string ledDebitAmt = string.Empty; string apiDebitAmt = string.Empty; string mode = string.Empty;
                        string AccountNo = string.Empty; string BankName = string.Empty; string IFSCCode = string.Empty; string BenName = string.Empty; string RemtName = string.Empty;
                        if (dsDetail.Tables[0].Rows.Count > 0)
                        {
                            DataTable dtTransDetail = dsDetail.Tables[0];
                            agentId = dtTransDetail.Rows[0]["AgentId"].ToString();
                            agencyName = dtTransDetail.Rows[0]["Agency_Name"].ToString();
                            remitterMobNo = dtTransDetail.Rows[0]["RemitterMobile"].ToString();
                            ledDebitAmt = dtTransDetail.Rows[0]["LedgerAmount"].ToString();
                            apiDebitAmt = dtTransDetail.Rows[0]["InstDebitAmt"].ToString();
                            mode = dtTransDetail.Rows[0]["Mode"].ToString();
                        }
                        if (dsDetail.Tables[1].Rows.Count > 0)
                        {
                            DataTable dtBenDetail = dsDetail.Tables[1];
                            AccountNo = dtBenDetail.Rows[0]["AccountNo"].ToString();
                            BankName = dtBenDetail.Rows[0]["BankName"].ToString();
                            IFSCCode = dtBenDetail.Rows[0]["IFSCCode"].ToString();
                            BenName = dtBenDetail.Rows[0]["BenName"].ToString();
                            RemtName = dtBenDetail.Rows[0]["RemtName"].ToString();
                        }

                        if (dsDetail.Tables[0].Rows.Count > 0)
                        {
                            DataTable DirectPayoutParameters = InstantPay_DataBase.Get_InstantPayDirectPayoutParameters("checkstatus");
                            if (DirectPayoutParameters != null && DirectPayoutParameters.Rows.Count > 0)
                            {
                                result = BindCheckStatusHtml(DirectPayoutParameters, trackId, transactionDate, agencyName, agentId, remitterMobNo, ledDebitAmt, apiDebitAmt, AccountNo, BankName, IFSCCode, BenName, RemtName, mode, dsDetail.Tables[0]);

                                //string IsApply = DirectPayoutParameters.Rows[0]["IsApply"].ToString();
                                //if (Convert.ToBoolean(IsApply) == true)
                                //{
                                //    string AuthCode = DirectPayoutParameters.Rows[0]["AuthCode"].ToString();
                                //    string ClientId = DirectPayoutParameters.Rows[0]["ClientId"].ToString();
                                //    string ClientSecret = DirectPayoutParameters.Rows[0]["ClientSecret"].ToString();
                                //    string EndpointIp = DirectPayoutParameters.Rows[0]["EndpointIp"].ToString();
                                //    string PostUrl = DirectPayoutParameters.Rows[0]["PostUrl"].ToString();
                                //    bool UpdateRecord = Convert.ToBoolean(DirectPayoutParameters.Rows[0]["UpdateRecord"].ToString());

                                //    string reqJson = "{\"transactionDate\" : \"" + transactionDate + "\",\"externalRef\" : \"" + trackId + "\"}";
                                //    if (!string.IsNullOrEmpty(reqJson))
                                //    {
                                //        string clientRefId = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 7).ToUpper();
                                //        //string response = Post("POST", PostUrl, reqJson, "checkstatus", agentId, clientRefId, AuthCode, ClientId, ClientSecret, EndpointIp);

                                //        string response = "{\"statuscode\":\"TXN\",\"actcode\":null,\"status\":\"-\",\"data\":{\"transactionStatusCode\":\"TXN\",\"transactionStatus\":\"Transaction Successful\",\"transactionAmount\":\"1500.00\",\"transactionReferenceId\":\"211120995508\",\"order\":{\"refereceId\":\"1220421205157QLGDY\",\"externalRef\":\"007FCFCB2A\",\"spKey\":\"DPN\",\"sspKey\":\"API\",\"account\":\"35557197811\",\"optional1\":\"SBIN0004343\",\"optional2\":\"khobir abbas pur\",\"optional3\":\"transfer\",\"optional4\":\"IMPS\",\"optional5\":\"\",\"optional6\":\"fastgocash@gmail.com\",\"optional7\":\"OTHERS\",\"optional8\":\"\",\"optional9\":\"\"},\"pool\":{\"transaction\":{\"refereceId\":\"1220421205157QLGDY\",\"openingBalance\":\"134107.61\",\"paymentAmount\":\"1503.54\",\"mode\":\"DR\",\"closingBalance\":\"132604.07\"},\"reversal\":{\"refereceId\":null,\"openingBalance\":null,\"paymentAmount\":null,\"mode\":null,\"closingBalance\":null}},\"tag\":{\"1\":{\"desc\":null,\"value\":null},\"2\":{\"desc\":null,\"value\":null},\"3\":{\"desc\":null,\"value\":null},\"4\":{\"desc\":null,\"value\":null},\"5\":{\"desc\":null,\"value\":null}}},\"timestamp\":\"2022-04-21 22:38:29\",\"ipay_uuid\":\"h068961e1daa-7bbe-491b-b7c1-ee4297382e19\",\"orderid\":null,\"environment\":\"LIVE\"}";

                                //        //string response = "{\"statuscode\":\"TXN\",\"actcode\":null,\"status\":\"-\",\"data\":{\"transactionStatusCode\":\"ERR\",\"transactionStatus\":\"Transaction not found in given transaction date\",\"transactionAmount\":null,\"transactionReferenceId\":null,\"order\":{\"refereceId\":null,\"externalRef\":null,\"spKey\":null,\"sspKey\":null,\"account\":null,\"optional1\":null,\"optional2\":null,\"optional3\":null,\"optional4\":null,\"optional5\":null,\"optional6\":null,\"optional7\":null,\"optional8\":null,\"optional9\":null},\"pool\":{\"transaction\":{\"refereceId\":null,\"openingBalance\":null,\"paymentAmount\":null,\"mode\":null,\"closingBalance\":null},\"reversal\":{\"refereceId\":null,\"openingBalance\":null,\"paymentAmount\":null,\"mode\":null,\"closingBalance\":null}},\"tag\":{\"1\":{\"desc\":null,\"value\":null},\"2\":{\"desc\":null,\"value\":null},\"3\":{\"desc\":null,\"value\":null},\"4\":{\"desc\":null,\"value\":null},\"5\":{\"desc\":null,\"value\":null}}},\"timestamp\":\"2022-04-21 22:41:37\",\"ipay_uuid\":\"h006961e1eca-4f81-4f14-a20f-fecb4a0eb9ba\",\"orderid\":null,\"environment\":\"LIVE\"}";

                                //        //result = BindCheckStatusHtml(response, agencyName, agentId, remitterMobNo, ledDebitAmt, apiDebitAmt, AccountNo, BankName, IFSCCode, BenName, RemtName, mode, dsDetail.Tables[0]);
                                //    }
                                //}
                            }
                        }
                        else
                        {
                            result = "<div class='col-md-12' style='text-align: center;'><div class='row form-group'><h3 style='color: red;'>Record not available !</h3></div></div>";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        public static string Admin_CheckStatusRefundAmount(string transactionDate, string trackId, string Amount, string IpAddress)
        {
            string rutStr = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(transactionDate.Trim()) && !string.IsNullOrEmpty(trackId))
                {
                    DataSet dsDetail = InstantPay_DataBase.SP_AdminCheckInstantPayStatus(trackId, transactionDate);
                    if (dsDetail.Tables.Count > 0)
                    {
                        if (dsDetail.Tables[0].Rows.Count > 0)
                        {
                            DataTable dtTransDetail = dsDetail.Tables[0];
                            string agentId = dtTransDetail.Rows[0]["AgentId"].ToString();
                            string agencyName = dtTransDetail.Rows[0]["Agency_Name"].ToString();
                            string remitterMobNo = dtTransDetail.Rows[0]["RemitterMobile"].ToString();
                            string ledDebitAmt = dtTransDetail.Rows[0]["LedgerAmount"].ToString();
                            string apiDebitAmt = dtTransDetail.Rows[0]["InstDebitAmt"].ToString();
                            string RefundTrackId = dtTransDetail.Rows[0]["RefundTrackId"].ToString();

                            if (InstantPay_DataBase.Get_DMTAmountRefundedOrNot(agentId, trackId) == 0)
                            {
                                string Narration = "Dmt Direct Reund " + Amount + " To " + agentId;
                                List<string> LdRefund = LedgerService.SimpleLedgerDebitCreditUtility(Convert.ToDouble(Amount), agentId, agencyName, trackId, agentId, IpAddress, 0, Convert.ToDouble(Amount), "Credit", "Refund", "CR", "Payout Direct Money Transfer", Narration, "CREDIT NOTE");
                                if (LdRefund.Count > 0)
                                {
                                    //InstantPay_DataBase.Update_SP_CheckInstantPayStatus(agentId, LdRefund[0], remitterMobNo);
                                    InstantPay_DataBase.Insert_T_InstantPayFailedRefund(agentId, trackId, LdRefund[0].ToString(), Amount, IpAddress);
                                }
                                rutStr = "Success : Amount refunded successfully for trackId : " + trackId;
                            }
                            else
                            {
                                rutStr = "Warning : Amount already refunded for trackId : " + trackId;
                            }
                        }
                    }
                    else
                    {
                        rutStr = "Warning : Record not available !";
                    }
                }
            }
            catch (Exception ex)
            {
                rutStr = ex.Message;
            }

            return rutStr;
        }

        private static string BindCheckStatusHtml(DataTable DirectPayoutParameters, string trackId, string transactionDate, string agencyName, string agentId, string remitterMobNo, string ledDebitAmt, string apiDebitAmt, string AccountNo, string BankName, string IFSCCode, string BenName, string RemtName, string mode, DataTable dsDetail)
        {
            StringBuilder sbHtml = new StringBuilder();
            if (dsDetail.Rows.Count > 0)
            {
                //if (!string.IsNullOrEmpty(response))
                //{
                //    dynamic dyResult = JObject.Parse(response);
                //    dynamic dyData = dyResult.data;
                //    string transactionStatusCode = dyData.transactionStatusCode;
                //    string transactionStatus = dyData.transactionStatus;

                //    dynamic dyorder = dyData.order;
                //    dynamic dypool = dyData.pool;

                //    dynamic dypool_transaction = dypool.transaction;
                //    dynamic dypool_reversal = dypool.reversal;

                //    string reversal_refereceId = dypool_reversal.refereceId;
                //    string reversal_openingBalance = dypool_reversal.openingBalance;
                //    string reversal_paymentAmount = dypool_reversal.paymentAmount;
                //    string reversal_mode = dypool_reversal.mode;
                //    string reversal_closingBalance = dypool_reversal.closingBalance;


                sbHtml.Append("<div class='col-md-12' style='border-bottom: 1px solid #ccc;'>");
                sbHtml.Append("<div class='row form-group' style='border-bottom: 1px solid #ccc;'>");
                sbHtml.Append("<div class='col-md-3'>");
                sbHtml.Append("<h3>Agency Name</h3>");
                sbHtml.Append("<label>" + agencyName + " [" + agentId + "]</label>");
                sbHtml.Append("<input type='hidden' name='AgencyUserId' id='AgencyUserId' value='" + agentId + "' />");
                sbHtml.Append("</div>");
                sbHtml.Append("<div class='col-md-3'>");
                sbHtml.Append("<h3>Remitter Mobile Number</h3>");
                sbHtml.Append("<label>" + RemtName + " [" + remitterMobNo + "]</label>");
                sbHtml.Append("<input type='hidden' name='RemitterMobNo' id='RemitterMobNo' value='" + remitterMobNo + "' />");
                sbHtml.Append("</div>");
                sbHtml.Append("<div class='col-md-3'>");
                sbHtml.Append("<h3>Ledger Debit Amount</h3>");
                sbHtml.Append("<label>₹ " + ledDebitAmt + "</label>");
                sbHtml.Append("</div>");
                //sbHtml.Append("<div class='col-md-3'>");
                //sbHtml.Append("<h3>InstantPay Debit Amount</h3>");
                //sbHtml.Append("<label> " + (!string.IsNullOrEmpty(apiDebitAmt) ? "₹ " + apiDebitAmt : "- - -") + "</label>");
                //sbHtml.Append("</div></div>");
                sbHtml.Append("</div>");

                List<string> strTrackId = new List<string>();
                if (dsDetail.Rows.Count > 0)
                {
                    for (int i = 0; i < dsDetail.Rows.Count; i++)
                    {
                        string transMode = "- - -";

                        if (dsDetail.Rows[i]["Mode"].ToString() == "DPN")
                        {
                            transMode = "IMPS";
                        }
                        else if (dsDetail.Rows[i]["Mode"].ToString() == "BPN")
                        {
                            transMode = "NEFT";
                        }
                        else if (dsDetail.Rows[i]["Mode"].ToString() == "CPN")
                        {
                            transMode = "RTGS";
                        }
                        else
                        {
                            transMode = dsDetail.Rows[i]["Mode"].ToString();
                        }

                        strTrackId.Add(!string.IsNullOrEmpty(dsDetail.Rows[i]["InstPayTrackId"].ToString()) ? dsDetail.Rows[i]["InstPayTrackId"].ToString() : dsDetail.Rows[i]["TrackId"].ToString());
                        if (i == 0)
                        {
                            sbHtml.Append("<div class='row form-group'>");
                            sbHtml.Append("<div class='col-md-3'><h3>Ledger Track Id</h3><label>" + dsDetail.Rows[i]["TrackId"].ToString() + " </label></div>");
                            sbHtml.Append("<div class='col-md-2'><h3>InstantPay Track Id</h3><label>" + dsDetail.Rows[i]["InstPayTrackId"].ToString() + "</label></div>");
                            sbHtml.Append("<div class='col-md-2'><h3>InstantPay Debit Amount</h3><label>" + dsDetail.Rows[i]["InstDebitAmt"].ToString() + "</label></div>");
                            sbHtml.Append("<div class='col-md-2'><h3>Mode Of Payment</h3><label>" + transMode + "</label></div>");
                            sbHtml.Append("<div class='col-md-3'><h3>Status</h3><label>" + dsDetail.Rows[i]["Status"].ToString() + "</label></div>");
                            sbHtml.Append("</div>");
                        }
                        else
                        {
                            sbHtml.Append("<div class='row form-group'>");
                            sbHtml.Append("<div class='col-md-3'><label>" + dsDetail.Rows[i]["TrackId"].ToString() + " </label></div>");
                            sbHtml.Append("<div class='col-md-2'><label>" + dsDetail.Rows[i]["InstPayTrackId"].ToString() + "</label></div>");
                            sbHtml.Append("<div class='col-md-2'><label>" + dsDetail.Rows[i]["InstDebitAmt"].ToString() + "</label></div>");
                            sbHtml.Append("<div class='col-md-2'><label>" + transMode + "</label></div>");
                            sbHtml.Append("<div class='col-md-3'>" + dsDetail.Rows[i]["Status"].ToString() + "</label></div>");
                            sbHtml.Append("</div>");
                        }
                    }
                }

                sbHtml.Append("<div class='row form-group' style='border-top: 1px solid #ccc;'>");
                sbHtml.Append("<div class='col-md-3'><h3>Benificiery Name</h3><label>" + BenName + "</label></div>");
                sbHtml.Append("<div class='col-md-3'><h3>Account Number</h3><label>" + AccountNo + "</label></div>");
                sbHtml.Append("<div class='col-md-3'><h3>Bank Name</h3><label>" + BankName + "</label></div>");
                sbHtml.Append("<div class='col-md-3'><h3>IFSC Code</h3><label>" + IFSCCode + "</label></div>");
                sbHtml.Append("</div>");

                //sbHtml.Append("<div class='row form-group'>");
                ////sbHtml.Append("<div class='col-md-3'><h3>Transaction Status</h3><label>"+ transactionStatusCode + "</label></div>");
                //sbHtml.Append("<div class='col-md-3'><h3>Transaction Message</h3><label>" + transactionStatus + "</label></div>");
                ////sbHtml.Append("<div class='col-md-3'><h3>Transaction Amount</h3><label>₹ 3000.00</label></div>");
                //sbHtml.Append("<div class='col-md-3'><h3>Mode Of Payment</h3><label>" + mode + "</label></div>");
                //sbHtml.Append("</div>");

                //sbHtml.Append("<div class='row form-group'>");
                //sbHtml.Append("<div class='col-md-3'><h3>Account Number</h3><label>" + dyorder.account + "</label></div>");
                //sbHtml.Append("<div class='col-md-3'><h3>IFSC Code</h3><label>" + dyorder.optional1 + "</label></div>");
                //sbHtml.Append("<div class='col-md-3'><h3>Name</h3><label>" + dyorder.optional2 + "</label></div>");
                //sbHtml.Append("<div class='col-md-3'><h3>Mode of Payment</h3><label>" + dyorder.optional4 + "</label></div>");
                //sbHtml.Append("</div>");
                sbHtml.Append("</div>");

                sbHtml.Append("<div class='col-md-12' style='border-bottom: 1px solid #ccc;'>");
                int loopCount = 1;
                bool IsTransactionFailed = false;
                foreach (var item in strTrackId)
                {
                    if (DirectPayoutParameters.Rows.Count > 0)
                    {
                        string IsApply = DirectPayoutParameters.Rows[0]["IsApply"].ToString();
                        if (Convert.ToBoolean(IsApply) == true)
                        {
                            string AuthCode = DirectPayoutParameters.Rows[0]["AuthCode"].ToString();
                            string ClientId = DirectPayoutParameters.Rows[0]["ClientId"].ToString();
                            string ClientSecret = DirectPayoutParameters.Rows[0]["ClientSecret"].ToString();
                            string EndpointIp = DirectPayoutParameters.Rows[0]["EndpointIp"].ToString();
                            string PostUrl = DirectPayoutParameters.Rows[0]["PostUrl"].ToString();
                            bool UpdateRecord = Convert.ToBoolean(DirectPayoutParameters.Rows[0]["UpdateRecord"].ToString());

                            string reqJson = "{\"transactionDate\" : \"" + transactionDate + "\",\"externalRef\" : \"" + item + "\"}";
                            if (!string.IsNullOrEmpty(reqJson))
                            {
                                string clientRefId = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 7).ToUpper();
                                string response = Post("POST", PostUrl, reqJson, "checkstatus", agentId, clientRefId, AuthCode, ClientId, ClientSecret, EndpointIp);
                                //string response = "{\"statuscode\":\"TXN\",\"actcode\":null,\"status\":\"-\",\"data\":{\"transactionStatusCode\":\"ERR\",\"transactionStatus\":\"Transaction not found in given transaction date\",\"transactionAmount\":null,\"transactionReferenceId\":null,\"order\":{\"refereceId\":null,\"externalRef\":null,\"spKey\":null,\"sspKey\":null,\"account\":null,\"optional1\":null,\"optional2\":null,\"optional3\":null,\"optional4\":null,\"optional5\":null,\"optional6\":null,\"optional7\":null,\"optional8\":null,\"optional9\":null},\"pool\":{\"transaction\":{\"refereceId\":null,\"openingBalance\":null,\"paymentAmount\":null,\"mode\":null,\"closingBalance\":null},\"reversal\":{\"refereceId\":null,\"openingBalance\":null,\"paymentAmount\":null,\"mode\":null,\"closingBalance\":null}},\"tag\":{\"1\":{\"desc\":null,\"value\":null},\"2\":{\"desc\":null,\"value\":null},\"3\":{\"desc\":null,\"value\":null},\"4\":{\"desc\":null,\"value\":null},\"5\":{\"desc\":null,\"value\":null}}},\"timestamp\":\"2022-04-23 15:55:05\",\"ipay_uuid\":\"h0689621935f-577a-4286-b21e-8160268c509f\",\"orderid\":null,\"environment\":\"LIVE\"}";

                                if (!string.IsNullOrEmpty(response))
                                {
                                    dynamic dyResult = JObject.Parse(response);
                                    dynamic dyData = dyResult.data;
                                    string transactionStatusCode = dyData.transactionStatusCode;
                                    string transactionStatus = dyData.transactionStatus;

                                    dynamic dyorder = dyData.order;
                                    dynamic dypool = dyData.pool;

                                    dynamic dypool_transaction = dypool.transaction;
                                    dynamic dypool_reversal = dypool.reversal;

                                    string reversal_refereceId = dypool_reversal.refereceId;
                                    string reversal_openingBalance = dypool_reversal.openingBalance;
                                    string reversal_paymentAmount = dypool_reversal.paymentAmount;
                                    string reversal_mode = dypool_reversal.mode;
                                    string reversal_closingBalance = dypool_reversal.closingBalance;

                                    if (transactionStatusCode != "ERR")
                                    {
                                        sbHtml.Append("<div class='row form-group'>");
                                        sbHtml.Append("<div class='col-md-3'><h3>Transaction RefereceId</h3><label>" + dypool_transaction.refereceId + "</label></div>");
                                        sbHtml.Append("<div class='col-md-3'><h3>Transaction Opening Balance</h3><label>₹ " + dypool_transaction.openingBalance + "</label></div>");
                                        sbHtml.Append("<div class='col-md-3'><h3>Transaction Payment Amount</h3><label>₹ " + dypool_transaction.paymentAmount + " [ " + dypool_transaction.mode + " ]</label></div>");
                                        sbHtml.Append("<div class='col-md-3'><h3>Transaction Closing Balance</h3><label>₹ " + dypool_transaction.closingBalance + "</label></div>");
                                        sbHtml.Append("</div>");

                                        if (!string.IsNullOrEmpty(reversal_refereceId) && reversal_mode == "CR")
                                        {
                                            IsTransactionFailed = true;
                                            sbHtml.Append("<div class='row form-group'>");
                                            sbHtml.Append("<div class='col-md-2'><h3>Reversal RefereceId</h3><label>" + dypool_reversal.refereceId + "</label></div>");
                                            sbHtml.Append("<div class='col-md-2'><h3>Reversal Opening Balance</h3><label>₹ " + dypool_reversal.openingBalance + "</label></div>");
                                            sbHtml.Append("<div class='col-md-2'><h3>Reversal Payment Amount</h3><label style='color: red; font-weight: bold;'>₹ " + dypool_reversal.paymentAmount + " [ " + dypool_reversal.mode + " ]</label></div>");
                                            sbHtml.Append("<div class='col-md-3'><h3>Reversal Closing Balance</h3><label>₹ " + dypool_reversal.closingBalance + "</label></div>");
                                            sbHtml.Append("<div class='col-md-3'><h3>Status</h3><label>" + transactionStatus + "</label></div>");
                                            sbHtml.Append("</div>");
                                        }
                                        else
                                        {
                                            sbHtml.Append("<div class='col-md-12' style='border-bottom: 1px solid #ccc;'>");
                                            sbHtml.Append("<div class='row form-group'>");
                                            sbHtml.Append("<div class='col-md-4'></div>");
                                            sbHtml.Append("<div class='col-md-4'><p style='font-size: 25px;margin-top: 35px;color: green;'>"+ transactionStatus + "</p></div>");
                                            sbHtml.Append("<div class='col-md-4'></div>");
                                            sbHtml.Append("</div>");
                                            sbHtml.Append("</div>");
                                        }
                                    }
                                    else if (transactionStatusCode == "ERR")
                                    {
                                        IsTransactionFailed = true;
                                        //    sbHtml.Append("<div class='row form-group'>");
                                        //    sbHtml.Append("<div class='col-md-3'><h3>Enter Refund Amount</h3><input id='txtRefundAmount' type='number' class='form-control' /></div>");
                                        //    sbHtml.Append("<div class='col-md-3'><span id='btnSubmit' class='button buttonBlue' style='margin-top: 33px;'>Refund</span></div>");
                                        //    sbHtml.Append("</div>");
                                    }
                                }
                            }
                        }
                    }
                    loopCount = loopCount + 1;
                }
                sbHtml.Append("</div>");

                if (IsTransactionFailed)
                {
                    if (InstantPay_DataBase.Get_DMTAmountRefundedOrNot(agentId, trackId) == 0)
                    {
                        sbHtml.Append("<div class='col-md-12' style='border-bottom: 1px solid #ccc;'>");
                        sbHtml.Append("<div class='row form-group'>");
                        sbHtml.Append("<div class='col-md-3'><h3>Enter Refund Amount</h3><input id='txtRefundAmount' type='number' class='form-control' /></div>");
                        sbHtml.Append("<div class='col-md-3'><span id='btnSubmit' class='button buttonBlue' style='margin-top: 33px;'>Refund</span></div>");
                        sbHtml.Append("</div>");
                        sbHtml.Append("</div>");
                    }
                    else
                    {
                        sbHtml.Append("<div class='col-md-12'>");
                        sbHtml.Append("<div class='row form-group'><br><h3 style='color: red;'>Amount already refunded for this track id : " + trackId + "</h3>");
                        sbHtml.Append("</div>");
                        sbHtml.Append("</div>");
                    }
                }
                //}
            }
            return sbHtml.ToString();
        }

        public static int InsertInitiatePayout(InitiatePayout payout)
        {
            try
            {
                return InstantPay_DataBase.InsertInitiatePayout(payout);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return 0;
        }

        public static string Post(string postType, string Url, string ReqJson, string actionType, string agentId, string trackid, string AuthCode, string ClientId, string ClientSecret, string EndpointIp)
        {
            string ReturnValue = string.Empty;
            InstantPay_DataBase.Information(Url, "Request captured in post Method : " + Url + "", ReqJson, "", actionType, agentId, trackid);

            StringBuilder sbResult = new StringBuilder();

            //ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(Url);

            try
            {
                Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                if (!string.IsNullOrEmpty(AuthCode) && !string.IsNullOrEmpty(ClientId) && !string.IsNullOrEmpty(ClientSecret))
                {
                    Http.Headers.Add("X-Ipay-Auth-Code", AuthCode);
                    Http.Headers.Add("X-Ipay-Client-Id", ClientId);
                    Http.Headers.Add("X-Ipay-Client-Secret", ClientSecret);
                    Http.Headers.Add("X-Ipay-Endpoint-Ip", EndpointIp);
                }

                Http.Method = postType;
                byte[] lbPostBuffer = Encoding.UTF8.GetBytes(ReqJson);
                Http.ContentLength = lbPostBuffer.Length;
                Http.Timeout = 130000; //5000 milliseconds == 5 seconds// 900000 milliseconds == 900 seconds- 15 mints
                Http.ContentType = "application/json";
                Http.Accept = "application/json";

                using (Stream PostStream = Http.GetRequestStream())
                {
                    PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        ReturnValue = sbResult.ToString();
                        responseStream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                InstantPay_DataBase.Error("Error : Request in Post Method : " + Url + "", Url, ReqJson, "Response in Post Method : " + ReturnValue + "", "Exception in Post Method:" + ex.Message.Replace("'", "''") + "", actionType, agentId, trackid);
                ReturnValue = null;
            }
            finally
            {
                Http.Abort();
                Http = null;
            }
            InstantPay_DataBase.Information(Url, "Request captured in post Method : " + Url + "", ReqJson, ReturnValue, actionType, agentId, trackid);
            return ReturnValue;
        }

        public static DataTable GetFilterTransactionHistory(string remitterId, string fromDate, string toDate, string trackId, string filstatus)
        {
            return InstantPay_PayoutDatabase.GetFilterTransactionHistory(remitterId, fromDate, toDate, trackId, filstatus);
        }

        public static bool DeleteBeneficiaryDetailById(string benid)
        {
            try
            {
                return InstantPay_PayoutDatabase.DeleteBeneficiaryDetailById(benid);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static DataTable GetCommonFundTransHistory(string trackid, string agentId, ref DataTable RemitterDel, ref DataTable BenDetail)
        {
            return InstantPay_PayoutDatabase.GetCommonFundTransHistory(trackid, agentId, ref RemitterDel, ref BenDetail);
        }

        #region [Ledger Section Credit and Debit]
        public static List<string> PayoutLedgerDebitCredit(double Amount, string AgentId, string AgencyName, string AccountID, string IPAddress, double Debit, double Credit, string BookingType, string Remark, string Uploadtype, string shortRemark, string Narration, string TransType, string user_Id, string mobile, string remitterId, string benid)
        {
            return LedgerService.PayoutLedgerDebitCredit(Amount, AgentId, AgencyName, AccountID, IPAddress, Debit, Credit, BookingType, Remark, Uploadtype, shortRemark, Narration, TransType, user_Id, mobile, remitterId, benid);
        }
        #endregion

        //=====================================================================

        public static DataTable GetPayoutRemitterWithBeniCheck(string transId, string trackId, string regId, string agentid, string mobile, string remitterid, ref DataTable dtTrans)
        {
            return InstantPay_PayoutDatabase.GetPayoutRemitterWithBeniCheck(transId, trackId, regId, agentid, mobile, remitterid, ref dtTrans);
        }
    }
}
