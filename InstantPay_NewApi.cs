﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace InstantPayServiceLib
{
    public static class InstantPay_NewApi
    {
        public static string Post(string postType, string Url, string ReqJson, string actionType, string agentId, string trackid)
        {
            string ReturnValue = string.Empty;
            InstantPay_DataBase.Information(Url, "Request captured in post Method : " + Url + "", ReqJson, "", actionType, agentId, trackid);

            StringBuilder sbResult = new StringBuilder();

            //ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(Url);

            try
            {
                Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");

                DataTable DirectPayoutParameters = InstantPay_DataBase.Get_InstantPayDirectPayoutParameters("direct");
                if (DirectPayoutParameters != null && DirectPayoutParameters.Rows.Count > 0)
                {
                    string IsApply = DirectPayoutParameters.Rows[0]["IsApply"].ToString();
                    if (Convert.ToBoolean(IsApply) == true)
                    {
                        string AuthCode = DirectPayoutParameters.Rows[0]["AuthCode"].ToString();
                        string ClientId = DirectPayoutParameters.Rows[0]["ClientId"].ToString();
                        string ClientSecret = DirectPayoutParameters.Rows[0]["ClientSecret"].ToString();
                        string EndpointIp = DirectPayoutParameters.Rows[0]["EndpointIp"].ToString();
                        string OutletId = DirectPayoutParameters.Rows[0]["OutletId"].ToString();

                        Http.Headers.Add("X-Ipay-Auth-Code", AuthCode);
                        Http.Headers.Add("X-Ipay-Client-Id", ClientId);
                        Http.Headers.Add("X-Ipay-Client-Secret", ClientSecret);
                        Http.Headers.Add("X-Ipay-Outlet-Id", OutletId);
                        Http.Headers.Add("X-Ipay-Endpoint-Ip", EndpointIp);                        
                    }
                }

                Http.Method = postType;
                byte[] lbPostBuffer = Encoding.UTF8.GetBytes(ReqJson);
                Http.ContentLength = lbPostBuffer.Length;
                Http.Timeout = 130000; //5000 milliseconds == 5 seconds// 900000 milliseconds == 900 seconds- 15 mints
                Http.ContentType = "application/json";
                Http.Accept = "application/json";

                using (Stream PostStream = Http.GetRequestStream())
                {
                    PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        ReturnValue = sbResult.ToString();
                        responseStream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                InstantPay_DataBase.Error("Error : Request in Post Method : " + Url + "", Url, ReqJson, "Response in Post Method : " + ReturnValue + "", "Exception in Post Method:" + ex.Message.Replace("'", "''") + "", actionType, agentId, trackid);
                ReturnValue = null;
            }
            finally
            {
                Http.Abort();
                Http = null;
            }
            InstantPay_DataBase.Information(Url, "Request captured in post Method : " + Url + "", ReqJson, ReturnValue, actionType, agentId, trackid);
            return ReturnValue;
        }    
    }
}
