﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstantPayServiceLib
{
    public static class InstantPay_PayoutDatabase
    {
        #region [Connection Strings]
        public static SqlConnection MyAmdDBConnection = new SqlConnection(InstantPayConfig.MyAmdDBConnectionString);

        private static SqlCommand sqlCommand { get; set; }
        private static SqlDataAdapter sqlDataAdapter { get; set; }
        private static DataSet dataSet { get; set; }
        private static DataTable dataTable { get; set; }

        public static void MyAmdOpenConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Closed)
            {
                MyAmdDBConnection.Open();
            }
        }
        public static void MyAmdCloseConnection()
        {
            if (MyAmdDBConnection.State == ConnectionState.Open)
            {
                MyAmdDBConnection.Close();
            }
        }
        #endregion

        public static bool CheckPayoutKycComplete(string agentid, string mobile, string remitterid, ref string regId, ref bool showkyc, ref string kycStatus)
        {
            try
            {
                string query = "select * from T_InstantPayDMTDirectRemitter where RemitterId='" + remitterid + "' and Mobile='" + mobile + "' and AgentId='" + agentid + "'";
                DataTable dtPayoutRemitter = GetRecordFromTable(query);
                if (dtPayoutRemitter != null && dtPayoutRemitter.Rows.Count > 0)
                {
                    string iskyc = dtPayoutRemitter.Rows[0]["IsKYC"].ToString();
                    string isShowkyc = dtPayoutRemitter.Rows[0]["ShowKYCForm"].ToString();
                    kycStatus = dtPayoutRemitter.Rows[0]["KYCStatus"].ToString();
                    regId = dtPayoutRemitter.Rows[0]["Id"].ToString();
                    showkyc = isShowkyc.ToLower() == "true" ? true : false;

                    return (iskyc.ToLower() == "true" ? true : false);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static DataTable GetPayoutRemitterDetails(string regId, string agentid, string mobile, string remitterid, ref DataTable BenDetail, string benid = null)
        {
            DataTable dtPayoutRemitter = new DataTable();

            try
            {
                string query = "select * from T_InstantPayDMTDirectRemitter where RemitterId='" + remitterid + "' and Mobile='" + mobile + "' and AgentId='" + agentid + "'";

                if (!string.IsNullOrEmpty(regId))
                {
                    query = query + " and Id=" + regId;
                }

                dtPayoutRemitter = GetRecordFromTable(query);
                if (dtPayoutRemitter != null && dtPayoutRemitter.Rows.Count > 0)
                {
                    string iskyc = dtPayoutRemitter.Rows[0]["IsKYC"].ToString();
                    bool booliskyc = iskyc.ToLower() == "true" ? true : false;

                    if (booliskyc)
                    {
                        //Ben Datatable pending

                        BenDetail = GetPayoutRemitterBenDetails(regId, agentid, mobile, remitterid, benid);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dtPayoutRemitter;
        }

        public static DataTable GetPayoutRemitterBenDetails(string regId, string agentid, string mobile, string remitterid, string benid = null)
        {
            string query = "select * from T_InstantPayDMTDirectBeneficiary where IsActive=1 and RemitterId='" + remitterid + "' and Mobile='" + mobile + "' and AgentId='" + agentid + "'";

            if (!string.IsNullOrEmpty(regId))
            {
                query = query + " and RegRemittId=" + regId;
            }

            if (!string.IsNullOrEmpty(benid))
            {
                query = query + " and BenId='" + benid + "'";
            }

            return GetRecordFromTable(query);
        }

        public static bool UpdateRemitterKYCDetails(string regId, string agentId, string mobile, string remitterId, string fn, string ln, string add, string gen, string dob, string proof, string pno, string frontImg, string backImg)
        {
            try
            {
                string query = "update T_InstantPayDMTDirectRemitter set "
                    + "FirstName='" + fn + "',LastName='" + ln + "',Address='" + add + "',KYCStatus='WAITING FOR APPROVAL',ShowKYCForm=0,Gender='" + gen + "',DOB='" + dob + "',"
                    + "IDProof='" + proof + "',IDProofNumber='" + pno + "',FrontImg='" + frontImg + "',BackImg='" + backImg + "', UpdatedDate=GETDATE()"
                    + " where RemitterId='" + remitterId + "'";
                if (!string.IsNullOrEmpty(regId))
                {
                    query = query + " and Id=" + regId;
                }
                else
                {
                    query = query + " and Mobile='" + mobile + "' and AgentId='" + agentId + "'";
                }

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool PayoutUpdateLocalAddress(string regid, string remitterId, string agentId, string mobile, string address)
        {
            try
            {
                string query = "update T_InstantPayDMTDirectRemitter set Address='" + address + "', UpdatedDate=getdate() where RemitterId='" + remitterId + "' and Mobile='" + mobile + "' and AgentId='" + agentId + "'";

                if (!string.IsNullOrEmpty(regid))
                {
                    query = query + " and Id=" + regid;
                }

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool PayoutBeneficiaryRegistration(string regid, string remitterid, string name, string mobile, string account, string bankname, string ifsc, string agentId)
        {
            try
            {
                string benId = "BEN" + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 7).ToUpper();

                string query = "insert into T_InstantPayDMTDirectBeneficiary"
                    + " (RegRemittId,RemitterId,AgentId,Mobile,AccountNo,BankName,IFSCCode,BenName,BenId) values "
                    + "(" + regid + ",'" + remitterid + "','" + agentId + "','" + mobile + "','" + account + "','" + bankname + "','" + ifsc + "','" + name + "','" + benId + "')";

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        #region [Common Section]       
        private static DataTable GetRecordFromTable(string query)
        {
            try
            {
                if (!string.IsNullOrEmpty(query))
                {
                    sqlCommand = new SqlCommand(query, MyAmdDBConnection);

                    sqlDataAdapter = new SqlDataAdapter();
                    sqlDataAdapter.SelectCommand = sqlCommand;
                    dataSet = new DataSet();
                    sqlDataAdapter.Fill(dataSet, "T_Table");
                    dataTable = dataSet.Tables["T_Table"];
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        private static bool InsertUpdateDataBase(string query)
        {
            try
            {
                if (!string.IsNullOrEmpty(query))
                {
                    sqlCommand = new SqlCommand(query, MyAmdDBConnection);

                    MyAmdOpenConnection();
                    int isSuccess = sqlCommand.ExecuteNonQuery();
                    MyAmdCloseConnection();

                    if (isSuccess > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        #endregion

        #region [DMT DIRECT]
        //public static DataTable GetDMTDirectRemitter(string mobile, string agentid, string remitterid = null)
        //{
        //    string query = "select * from T_InstantPayDMTDirectRemitter where Mobile='" + mobile + "' and AgentId='" + agentid + "'";

        //    if (!string.IsNullOrEmpty(remitterid))
        //    {
        //        query = query + " and RemitterId='" + remitterid + "'";
        //    }

        //    return GetRecordFromTable(query);
        //}

        public static DataTable GetDMTDirectRemitter(string mobile, string agentid, string remitterid = null)
        {
            try
            {
                sqlCommand = new SqlCommand("sp_GetT_InstantPayDMTDirectRemitter", MyAmdDBConnection);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add(new SqlParameter("@Mobile", mobile));
                sqlCommand.Parameters.Add(new SqlParameter("@AgentId", agentid));
                sqlCommand.Parameters.Add(new SqlParameter("@Remitterid", (!string.IsNullOrEmpty(remitterid) ? remitterid : "")));
                sqlDataAdapter = new SqlDataAdapter();
                sqlDataAdapter.SelectCommand = sqlCommand;
                dataSet = new DataSet();
                sqlDataAdapter.Fill(dataSet, "table");
                dataTable = dataSet.Tables["table"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dataTable;
        }

        public static bool InsertDMTDirectRemitter(string mobile, string fistname, string lastname, string pincode, string address, string agentid, string dynremitterid, ref string otp)
        {
            try
            {
                Random rnd = new Random();
                int myRandomNo = rnd.Next(100000, 999999);
                otp = "123456";//myRandomNo.ToString();

                DateTime dtNow = DateTime.Now;
                string expOtp = dtNow.AddMinutes(3).ToString();

                string query = "insert into T_InstantPayDMTDirectRemitter (RemitterId,AgentId,FirstName,LastName,Mobile,Address,Pincode,OTP,OTPExpTime)"
                    + " values('" + dynremitterid + "','" + agentid + "','" + fistname + "','" + lastname + "','" + mobile + "'" + ",'" + address + "','" + pincode + "','" + otp + "','" + expOtp + "')";

                if (InsertUpdateDataBase(query))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool ResendDMTDOtp(string agentid, string mobile, string remitterid)
        {
            try
            {
                Random rnd = new Random();
                int myRandomNo = rnd.Next(100000, 999999);
                string otp = "123456";//myRandomNo.ToString();

                DateTime dtNow = DateTime.Now;
                string expOtp = dtNow.AddMinutes(3).ToString();

                string query = "update T_InstantPayDMTDirectRemitter set OTP='" + otp + "', OTPExpTime='" + expOtp + "' where RemitterId='" + remitterid + "' and Mobile='" + mobile + "' and AgentId='" + agentid + "'";

                if (InsertUpdateDataBase(query))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool VerifyingDMTDRemitter(string agentid, string mobile, string remitterid)
        {
            try
            {
                Random rnd = new Random();
                int myRandomNo = rnd.Next(100000, 999999);
                string otp = myRandomNo.ToString();

                DateTime dtNow = DateTime.Now;
                string expOtp = dtNow.AddMinutes(3).ToString();

                string query = "update T_InstantPayDMTDirectRemitter set OTP='', OTPExpTime='', IsVerified=1 where RemitterId='" + remitterid + "' and Mobile='" + mobile + "' and AgentId='" + agentid + "'";

                if (InsertUpdateDataBase(query))
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        #endregion

        public static bool GenrateOTPForTransfer(string regId, string agentid, string mobile, string remitterid, ref string otp)
        {
            try
            {
                Random rnd = new Random();
                int myRandomNo = rnd.Next(100000, 999999);
                otp = myRandomNo.ToString();

                DateTime dtNow = DateTime.Now;
                string expOtp = dtNow.AddMinutes(3).ToString();

                string query = "update T_InstantPayDMTDirectRemitter set TransferOTP='" + otp + "', TransferOTPExpDate='" + expOtp + "', UpdatedDate=GETDATE()"
                    + " where RemitterId='" + remitterid + "' and Mobile='" + mobile + "' and AgentId='" + agentid + "'";
                if (!string.IsNullOrEmpty(regId))
                {
                    query = query + " and Id=" + regId;
                }

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool RemoveOTPForTransfer(string regId, string agentid, string mobile, string remitterid)
        {
            try
            {
                string query = "update T_InstantPayDMTDirectRemitter set TransferOTP='', TransferOTPExpDate='', UpdatedDate=GETDATE()"
                    + " where RemitterId='" + remitterid + "' and Mobile='" + mobile + "' and AgentId='" + agentid + "'";
                if (!string.IsNullOrEmpty(regId))
                {
                    query = query + " and Id=" + regId;
                }

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool GenrateOTPForBenDelete(string agentid, string mobile, string remitterid, string benid, ref string otp)
        {
            try
            {
                Random rnd = new Random();
                int myRandomNo = rnd.Next(100000, 999999);
                otp = "123456";//myRandomNo.ToString();

                DateTime dtNow = DateTime.Now;
                string expOtp = dtNow.AddMinutes(3).ToString();

                string query = "update T_InstantPayDMTDirectBeneficiary set OTP='" + otp + "', EXPOTP='" + expOtp + "', UpdatedDate=GETDATE()"
                    + " where RemitterId='" + remitterid + "' and Mobile='" + mobile + "' and AgentId='" + agentid + "' and BenId='" + benid + "'";

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        private static string GetDateFormate(string date)
        {
            string result = string.Empty;

            if (!string.IsNullOrEmpty(date))
            {
                string[] strDate = date.Split('/');

                result = strDate[2] + "-" + strDate[1] + "-" + strDate[0];
            }

            return result;
        }

        public static DataTable GetFilterTransactionHistory(string remitterId, string fromDate, string toDate, string trackId, string filstatus)
        {
            DataTable mytable = new DataTable();

            try
            {
                mytable.Columns.Add("UpdatedDate", typeof(string));
                mytable.Columns.Add("ipay_id", typeof(string));
                mytable.Columns.Add("ref_no", typeof(string));
                mytable.Columns.Add("TrackId", typeof(string));
                mytable.Columns.Add("TxnMode", typeof(string));
                mytable.Columns.Add("Amount", typeof(string));
                mytable.Columns.Add("charged_amt", typeof(string));
                mytable.Columns.Add("BenificieryId", typeof(string));
                mytable.Columns.Add("BenName", typeof(string));
                mytable.Columns.Add("Refund", typeof(string));
                mytable.Columns.Add("RefundId", typeof(string));
                mytable.Columns.Add("FundTransId", typeof(string));
                mytable.Columns.Add("Status", typeof(string));
                mytable.Columns.Add("Type", typeof(string));

                string query2 = "select * from T_InstantPayInitiatePayout";
                string whrcondition2 = " where RemitterId='" + remitterId + "' and (PaymentType='payout_direct' or PaymentType='paygetname')";
                if (!string.IsNullOrEmpty(fromDate))
                {
                    whrcondition2 = whrcondition2 + " and CreatedDate>=CONVERT(datetime,'" + GetDateFormate(fromDate) + "')";
                }

                if (!string.IsNullOrEmpty(toDate))
                {
                    whrcondition2 = whrcondition2 + " and CreatedDate<=CONVERT(datetime,'" + GetDateFormate(toDate) + " 23:59:59')";
                }

                if (!string.IsNullOrEmpty(trackId))
                {
                    whrcondition2 = whrcondition2 + " and TrackId='" + trackId + "'";
                }

                if (!string.IsNullOrEmpty(filstatus))
                {
                    whrcondition2 = whrcondition2 + " and Status like '%" + filstatus + "%'";
                }

                query2 = query2 + whrcondition2 + " order by CreatedDate desc";

                DataTable dtPayTrans = GetRecordFromTable(query2);
                if (dtPayTrans != null && dtPayTrans.Rows.Count > 0)
                {
                    for (int i = 0; i < dtPayTrans.Rows.Count; i++)
                    {
                        string mode = string.Empty;
                        if (dtPayTrans.Rows[i]["sp_key"].ToString() == "DPN")
                        {
                            mode = "IMPS";
                        }
                        else if (dtPayTrans.Rows[i]["sp_key"].ToString() == "BPN")
                        {
                            mode = "NEFT";
                        }
                        else if (dtPayTrans.Rows[i]["sp_key"].ToString() == "CPN")
                        {
                            mode = "RTGS";
                        }

                        DataRow dr2 = mytable.NewRow();
                        dr2 = mytable.NewRow();
                        dr2["UpdatedDate"] = dtPayTrans.Rows[i]["CreatedDate"].ToString();
                        dr2["ipay_id"] = dtPayTrans.Rows[i]["ipay_id"].ToString();
                        dr2["ref_no"] = dtPayTrans.Rows[i]["external_ref"].ToString();
                        dr2["TrackId"] = dtPayTrans.Rows[i]["TrackId"].ToString();
                        dr2["TxnMode"] = mode;
                        dr2["Amount"] = dtPayTrans.Rows[i]["LedgerAmount"].ToString();
                        //dr2["charged_amt"] = dtPayTrans.Rows[i]["LedgerAmount"].ToString();
                        dr2["BenificieryId"] = dtPayTrans.Rows[i]["BenificieryId"].ToString();
                        dr2["BenName"] = !string.IsNullOrEmpty(dtPayTrans.Rows[i]["bene_name"].ToString()) && dtPayTrans.Rows[i]["bene_name"].ToString() != "none" ? dtPayTrans.Rows[i]["bene_name"].ToString() : dtPayTrans.Rows[i]["payout_name"].ToString();
                        dr2["Refund"] = dtPayTrans.Rows[i]["Refund"].ToString();
                        dr2["RefundId"] = dtPayTrans.Rows[i]["RefundId"].ToString();
                        dr2["FundTransId"] = dtPayTrans.Rows[i]["InitiatePayoutId"].ToString();
                        dr2["Status"] = dtPayTrans.Rows[i]["Status"].ToString();
                        dr2["Type"] = dtPayTrans.Rows[i]["PaymentType"].ToString();
                        mytable.Rows.Add(dr2);
                    }
                }

                //if (mytable != null && mytable.Rows.Count > 0)
                //{
                //    DataView view = mytable.DefaultView;
                //    view.Sort = "UpdatedDate DESC";
                //    mytable = view.ToTable();
                //}
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return mytable;
        }

        public static bool DeleteBeneficiaryDetailById(string benid)
        {
            try
            {
                string query = "update T_InstantPayDMTDirectBeneficiary set IsActive=0 where BenId='" + benid + "'";

                return InsertUpdateDataBase(query);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static DataTable GetCommonFundTransHistory(string trackid, string agentId, ref DataTable RemitterDel, ref DataTable BenDetail)
        {
            DataTable dtCommonFund = new DataTable();

            try
            {
                string fund = "select * from T_InstantPayInitiatePayout where TrackId='" + RemoveSpecialCharacters(trackid.Trim()) + "'";
                dtCommonFund = GetRecordFromTable(fund);
                if (dtCommonFund != null && dtCommonFund.Rows.Count > 0)
                {
                    string mobile = dtCommonFund.Rows[0]["RemitterMobile"].ToString();
                    string remitterid = dtCommonFund.Rows[0]["RemitterId"].ToString();
                    string beneficaryId = dtCommonFund.Rows[0]["BenificieryId"].ToString();

                    RemitterDel = GetDMTDirectRemitter(mobile, agentId, remitterid);
                    if (RemitterDel.Rows.Count == 0)
                    {
                        RemitterDel = InstantPay_DataBase.GetReitterDetails(remitterid, mobile, agentId);
                    }
                    BenDetail = GetPayoutRemitterBenDetails(string.Empty, agentId, mobile, remitterid, beneficaryId);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dtCommonFund;
        }

        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public static DataTable GetPayoutCommision(string groupType)
        {
            string query = "select * from T_PayoutMarkupCharges where Group_Type='" + groupType + "'";

            return GetRecordFromTable(query);
        }

        //========================================================
        public static DataTable GetPayoutRemitterWithBeniCheck(string transId, string trackId, string regId, string agentid, string mobile, string remitterid, ref DataTable dtTrans)
        {
            DataTable dtPayoutRemitter = new DataTable();

            try
            {
                string query = "select * from T_InstantPayDMTDirectRemitter where RemitterId='" + remitterid + "' and Mobile='" + mobile + "' and AgentId='" + agentid + "'";

                if (!string.IsNullOrEmpty(regId))
                {
                    query = query + " and Id=" + regId;
                }

                dtPayoutRemitter = GetRecordFromTable(query);
                if (dtPayoutRemitter != null && dtPayoutRemitter.Rows.Count > 0)
                {
                    string iskyc = dtPayoutRemitter.Rows[0]["IsKYC"].ToString();
                    bool booliskyc = iskyc.ToLower() == "true" ? true : false;

                    if (booliskyc)
                    {
                        dtTrans = GetRecordFromTable("select * from T_InstantPayInitiatePayout where InitiatePayoutId=" + transId + " and AgentId='" + agentid + "' and TrackId='" + trackId + "'");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return dtPayoutRemitter;
        }
    }
}
